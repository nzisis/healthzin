package com.ngng.healthzin.Adapters.Menu;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ngng.healthzin.Fragments.Menu.Interfaces.MenuCallback;
import com.ngng.healthzin.R;

/**
 * Created by nzisis on 11/24/16.
 */
public class MenuViewHolder extends RecyclerView.ViewHolder {
    public TextView tvTitle;
    public ImageView ivIcon;

    public MenuViewHolder(View itemView, final MenuCallback menuCallback,Context context) {
        super(itemView);

        Typeface typefaceCR = Typeface.createFromAsset(context.getAssets(), "fonts/Comfortaa-Regular.ttf");

        tvTitle = (TextView) itemView.findViewById(R.id.tvMenuTitle);
        ivIcon = (ImageView) itemView.findViewById(R.id.ivMenuIcon);

        tvTitle.setTypeface(typefaceCR);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuCallback.onItemClicked(getAdapterPosition());
            }
        });
    }
}
