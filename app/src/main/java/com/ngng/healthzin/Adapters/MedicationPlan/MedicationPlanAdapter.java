package com.ngng.healthzin.Adapters.MedicationPlan;

//import android.support.v4.app.FragmentManager;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngng.healthzin.Fragments.Medication.Interfaces.MedicationPlanListener;
import com.ngng.healthzin.Fragments.Medication.Interfaces.MedicationPlanUpdater;
import com.ngng.healthzin.Model.MedicationPlan.MedicationPlanItem;
import com.ngng.healthzin.R;
import java.util.ArrayList;


/**
 * Created by nzisis on 28/11/16.
 */
public class MedicationPlanAdapter extends RecyclerView.Adapter<MedicationPlanViewHolder> {

    private ArrayList<MedicationPlanItem> items;
    private MedicationPlanListener medicationPlanListener;
    private Context context;

    public MedicationPlanAdapter(ArrayList<MedicationPlanItem> items,MedicationPlanListener medicationPlanListener,Context context){
        this.items = items;
        this.medicationPlanListener = medicationPlanListener;
        this.context = context;
    }


    @Override
    public MedicationPlanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater =  LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.medication_plan_item,parent,false);

        return new MedicationPlanViewHolder(view,medicationPlanListener,medicationPlanUpdater,context);

    }

    @Override
    public void onBindViewHolder(MedicationPlanViewHolder holder, int position) {

        holder.tvHeader.setText(items.get(position).getHeader());
        holder.tvTime.setText(items.get(position).getTime());


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void notify(ArrayList<MedicationPlanItem> items){
        this.items = items;
        notifyDataSetChanged();

    }


    public ArrayList<MedicationPlanItem> getItems(){
        return items;
    }

   private MedicationPlanUpdater medicationPlanUpdater = new MedicationPlanUpdater() {
       @Override
       public void updateItem(int position, String time) {
          items.get(position).setTime(time);
       }
   };
}
