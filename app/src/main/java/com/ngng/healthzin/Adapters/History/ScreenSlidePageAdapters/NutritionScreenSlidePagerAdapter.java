package com.ngng.healthzin.Adapters.History.ScreenSlidePageAdapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

//import com.ngng.healthzin.Model.Database.Nutrition;
import com.ngng.healthzin.Model.Realm.Nutrition;
import com.ngng.healthzin.R;


import java.util.ArrayList;

/**
 * Created by nzisis on 14/12/16.
 */
public class NutritionScreenSlidePagerAdapter extends PagerAdapter {
    private ArrayList<Nutrition> items;
    private TextView tvMealQuantity, tvMealNumber, tvTime, tvMeal;
    private final LayoutInflater mLayoutInflater;
    private Context context;

    public NutritionScreenSlidePagerAdapter(Context context, ArrayList<Nutrition> items) {
        this.items = items;
        // Inflater which will be used for creating all the necessary pages
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.history_nutrition_item, container, false);


        tvMealQuantity = (TextView) itemView.findViewById(R.id.tvMealQuantity);
        tvMealNumber = (TextView) itemView.findViewById(R.id.tvMealNumber);
        tvTime = (TextView) itemView.findViewById(R.id.tvTime);
        tvMeal = (TextView) itemView.findViewById(R.id.tvMeal);

        display(items.get(position));

        container.addView(itemView);

        return itemView;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Removes the page from the container for the given position. We simply removed object using removeView()
        // but could’ve also used removeViewAt() by passing it the position.
        try {
            // Remove the view from the container
            container.removeView((View) object);

            object = null;
        } catch (Exception e) {
            Log.w("Mhstos", "destroyItem: failed to destroy item and clear it's used resources", e);
        }
    }


    private void display(Nutrition nutrition) {

        Typeface typefaceCR = Typeface.createFromAsset(context.getAssets(), "fonts/Comfortaa-Regular.ttf");

        tvMeal.setTypeface(typefaceCR);
        tvMealNumber.setTypeface(typefaceCR);
        tvMealQuantity.setTypeface(typefaceCR);
        tvTime.setTypeface(typefaceCR);

        String time,food;
        int current;
        double quantity;

        time = nutrition.getTime();
        food = nutrition.getFood();
        current = nutrition.getCurrent_meal();
        quantity = nutrition.getQuantity();


        String formatedMealNumber;
        String mealNumbers[] = context.getResources().getStringArray(R.array.meal_numbers);
        if (current == 1) {
            formatedMealNumber = mealNumbers[0];
        } else if (current == 2) {
            formatedMealNumber = mealNumbers[1];
        } else if (current == 3) {
            formatedMealNumber = mealNumbers[2];
        } else {
            formatedMealNumber = current + mealNumbers[3];
        }

        tvMealNumber.setText(formatedMealNumber);
        tvTime.setText(time);
        tvMeal.setText(food);
        tvMealQuantity.setText(quantity+"gr");
    }
}
