package com.ngng.healthzin.Adapters.History.ScreenSlidePageAdapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ngng.healthzin.Model.Realm.Medication;
import com.ngng.healthzin.R;

import java.util.ArrayList;

/**
 * Created by nzisis on 22/12/16.
 */
public class MedicationScreenSlidePagerAdapter extends PagerAdapter {
    private ArrayList<Medication> items;
    private TextView tvMedicine, tvReason, tvTime, tvDose;
    private final LayoutInflater mLayoutInflater;
    private Context context;


    public MedicationScreenSlidePagerAdapter(Context context, ArrayList<Medication> items) {
        this.items = items;
        // Inflater which will be used for creating all the necessary pages
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.history_medication_item, container, false);


        tvMedicine = (TextView) itemView.findViewById(R.id.tvMedicine);
        tvReason = (TextView) itemView.findViewById(R.id.tvMedicationReason);
        tvTime = (TextView) itemView.findViewById(R.id.tvTime);
        tvDose = (TextView) itemView.findViewById(R.id.tvMedicationDose);

        display(items.get(position));

        container.addView(itemView);

        return itemView;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Removes the page from the container for the given position. We simply removed object using removeView()
        // but could’ve also used removeViewAt() by passing it the position.
        try {
            // Remove the view from the container
            container.removeView((View) object);

            object = null;
        } catch (Exception e) {
            Log.w("Mhstos", "destroyItem: failed to destroy item and clear it's used resources", e);
        }
    }


    private void display(Medication medication) {
        Typeface typefaceCR = Typeface.createFromAsset(context.getAssets(), "fonts/Comfortaa-Regular.ttf");

        tvTime.setTypeface(typefaceCR);
        tvMedicine.setTypeface(typefaceCR);
        tvDose.setTypeface(typefaceCR);
        tvReason.setTypeface(typefaceCR);


        String medicine,reason,time;
        double dose;

        time = medication.getTime();
        medicine = medication.getMedicine();
        reason = medication.getCause();
        dose = medication.getDose();



        tvMedicine.setText(medicine);
        tvTime.setText(time);
        tvDose.setText(dose+medication.getDose_meter());
        tvReason.setText(reason);
    }

}
