package com.ngng.healthzin.Adapters.Menu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngng.healthzin.Fragments.Menu.Interfaces.MenuCallback;
import com.ngng.healthzin.Model.Menu.MenuItem;
import com.ngng.healthzin.R;

import java.util.ArrayList;

/**
 * Created by nzisis on 11/24/16.
 */
public class MenuAdapter extends RecyclerView.Adapter<MenuViewHolder> {
    private ArrayList<MenuItem> items;
    private MenuCallback menuCallback;
    private Context context;

    public MenuAdapter(ArrayList<MenuItem> menuItems,MenuCallback menuCallback,Context context){
        this.items = menuItems;
        this.menuCallback = menuCallback;
        this.context = context;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater =  LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.menu_item,parent,false);

        return new MenuViewHolder(view,menuCallback,context);
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, int position) {

        if(items.get(position).getSelected()){
            holder.tvTitle.setTextColor(context.getResources().getColor(R.color.app_blue));
            holder.ivIcon.setImageResource(items.get(position).getColor_resource());
        }else{
            holder.tvTitle.setTextColor(context.getResources().getColor(R.color.white));
            holder.ivIcon.setImageResource(items.get(position).getResource());
        }

        holder.tvTitle.setText(items.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void notify(ArrayList<MenuItem> menuItems){
        this.items = menuItems;
        notifyDataSetChanged();
    }
    
}
