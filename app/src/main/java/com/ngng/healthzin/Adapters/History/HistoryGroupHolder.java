package com.ngng.healthzin.Adapters.History;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.ngng.healthzin.R;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;
import static android.view.animation.Animation.RELATIVE_TO_SELF;
/**
 * Created by nzisis on 13/12/16.
 */
public class HistoryGroupHolder extends GroupViewHolder {

    public TextView tvGroupName;
    public ImageView ivArrow;

    public HistoryGroupHolder(View itemView,Context context) {
        super(itemView);

        Typeface typefaceCR = Typeface.createFromAsset(context.getAssets(), "fonts/Comfortaa-Regular.ttf");


        tvGroupName = (TextView) itemView.findViewById(R.id.tvHeader);
        ivArrow = (ImageView) itemView.findViewById(R.id.ivArrow);

        tvGroupName.setTypeface(typefaceCR);
    }

    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        ivArrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        ivArrow.setAnimation(rotate);
    }
}
