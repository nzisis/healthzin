package com.ngng.healthzin.Adapters.Overview;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ngng.healthzin.Fragments.Overview.Interfaces.OverviewMedicationListener;
import com.ngng.healthzin.Model.Overview.OverviewMedicationItem;
import com.ngng.healthzin.R;

import java.util.ArrayList;

import me.grantland.widget.AutofitTextView;

/**
 * Created by nzisis on 24/01/17.
 */
public class OverviewMedicationAdapter extends FragmentStatePagerAdapter {
    private ArrayList<OverviewMedicationItem> items;
    private OverviewMedicationListener overviewMedicationListener;
    private boolean next;


    public OverviewMedicationAdapter(FragmentManager fragmentManager,Context context, ArrayList<OverviewMedicationItem> items,OverviewMedicationListener overviewMedicationListener) {
        super(fragmentManager);
        this.items = items;
        this.overviewMedicationListener = overviewMedicationListener;

        if(items.size() == 1) next = false;
        else next = true;

    }

    @Override
    public Fragment getItem(int position) {
       OverviewMedicationFragment overviewMedicationFragment = OverviewMedicationFragment.newInstance(items.get(position),next);
       overviewMedicationFragment.setOverviewMedicationListener(overviewMedicationListener);

       return overviewMedicationFragment;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        //super.restoreState(state, loader);
    }


    public static class OverviewMedicationFragment extends Fragment{

        private TextView tvMedication, tvTimeAgo, tvTime;
        private AutofitTextView tvHeader;
        private ImageView ivNavigation;
        private OverviewMedicationItem medicationItem;
        private OverviewMedicationListener overviewMedicationListener;
        private boolean next;


        static OverviewMedicationFragment newInstance(OverviewMedicationItem medicationItem,boolean next){
            OverviewMedicationFragment overviewMedicationFragment = new OverviewMedicationFragment();

            Bundle bundle = new Bundle();
            bundle.putSerializable("item", medicationItem);
            bundle.putBoolean("next",next);
            overviewMedicationFragment.setArguments(bundle);


            return overviewMedicationFragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            init();
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View itemView = inflater.inflate(R.layout.overview_medication_item, container, false);


            tvMedication = (TextView) itemView.findViewById(R.id.tvMedication);
            tvTimeAgo = (TextView) itemView.findViewById(R.id.tvMedicationHoursAgo);
            tvTime = (TextView) itemView.findViewById(R.id.tvMedicationHour);
            tvHeader = (AutofitTextView) itemView.findViewById(R.id.tvMedicationHeader);

            ivNavigation = (ImageView) itemView.findViewById(R.id.ivArrow);



            return itemView;        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            initUI();
            initListeners();

        }

        public void setOverviewMedicationListener(OverviewMedicationListener overviewMedicationListener){
            this.overviewMedicationListener = overviewMedicationListener;
        }

        private void init(){
            if(getArguments()!=null){
                medicationItem =(OverviewMedicationItem) getArguments().getSerializable("item");
                next = getArguments().getBoolean("next");
            }
        }

        private void initUI() {
            Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
            tvTime.setTypeface(typefaceCR);
            tvMedication.setTypeface(typefaceCR);
            tvHeader.setTypeface(typefaceCR);
            tvTimeAgo.setTypeface(typefaceCR);


            if(medicationItem!=null){
                tvMedication.setText(medicationItem.getMedication());
                tvTime.setText(medicationItem.getTime());
                tvTimeAgo.setText(medicationItem.getTime_ago());
                tvHeader.setText(medicationItem.getHeader());

                if(!next){
                    ivNavigation.setVisibility(View.GONE);
                }else{
                    ivNavigation.setVisibility(View.VISIBLE);
                    if(medicationItem.getNext()){
                        //Right arrow
                        ivNavigation.setImageResource(R.drawable.back_arrow_right);

                    }else{
                        //Left arrow
                        ivNavigation.setImageResource(R.drawable.back_arrow_left);
                    }
                }
            }

        }


        private void initListeners(){
            ivNavigation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    overviewMedicationListener.moveToNext();
                }
            });
        }

    }


}
