package com.ngng.healthzin.Adapters.History.ScreenSlidePageAdapters;

import android.content.Context;

import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ngng.healthzin.Model.Realm.Exercise;
import com.ngng.healthzin.R;

import java.util.ArrayList;

/**
 * Created by nzisis on 20/12/16.
 */
public class ExerciseScreenSlidePagerAdapter extends PagerAdapter {
    private ArrayList<Exercise> items;
    private TextView tvExercise, tvPeriod, tvTime, tvWorkTime;
    private final LayoutInflater mLayoutInflater;
    private Context context;


    public ExerciseScreenSlidePagerAdapter(Context context, ArrayList<Exercise> items) {
        this.items = items;
        // Inflater which will be used for creating all the necessary pages
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.history_exercise_item, container, false);


        tvExercise = (TextView) itemView.findViewById(R.id.tvExercise);
        tvPeriod = (TextView) itemView.findViewById(R.id.tvExercisePeriod);
        tvTime = (TextView) itemView.findViewById(R.id.tvTime);
        tvWorkTime = (TextView) itemView.findViewById(R.id.tvExerciseWorkTime);

        display(items.get(position));

        container.addView(itemView);

        return itemView;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Removes the page from the container for the given position. We simply removed object using removeView()
        // but could’ve also used removeViewAt() by passing it the position.
        try {
            // Remove the view from the container
            container.removeView((View) object);

            object = null;
        } catch (Exception e) {
            Log.w("Mhstos", "destroyItem: failed to destroy item and clear it's used resources", e);
        }
    }


    private void display(Exercise exerciseItem) {
        Typeface typefaceCR = Typeface.createFromAsset(context.getAssets(), "fonts/Comfortaa-Regular.ttf");

        tvPeriod.setTypeface(typefaceCR);
        tvWorkTime.setTypeface(typefaceCR);
        tvExercise.setTypeface(typefaceCR);
        tvTime.setTypeface(typefaceCR);

        String exercise,period,time;
        double workTime;

        time = exerciseItem.getTime();
        exercise = exerciseItem.getExercise();
        period = exerciseItem.getPeriodOfDay();
        workTime = exerciseItem.getWorkTime();



        tvExercise.setText(exercise);
        tvTime.setText(time);
        tvWorkTime.setText(workTime+"");
        tvPeriod.setText(period);
    }

}
