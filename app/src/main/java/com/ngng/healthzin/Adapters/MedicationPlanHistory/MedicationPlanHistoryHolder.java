package com.ngng.healthzin.Adapters.MedicationPlanHistory;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces.MedicationPlanHistoryUpdaterListener;
import com.ngng.healthzin.R;

/**
 * Created by nzisis on 02/02/17.
 */
public class MedicationPlanHistoryHolder extends RecyclerView.ViewHolder {

    public TextView tvDateStarted,tvDaysLeft,tvNext,tvDose,tvMedicine;
    public CheckBox cbActive;
    public RelativeLayout rlMainContent;

    public MedicationPlanHistoryHolder(View itemView, final MedicationPlanHistoryUpdaterListener medicationPlanHistoryUpdaterListener,Context context) {
        super(itemView);
        Typeface typefaceCR = Typeface.createFromAsset(context.getAssets(), "fonts/Comfortaa-Regular.ttf");

        tvDateStarted = (TextView) itemView.findViewById(R.id.tvDateStarted);
        tvNext = (TextView) itemView.findViewById(R.id.tvNext);
        tvDaysLeft = (TextView) itemView.findViewById(R.id.tvDaysLeft);
        tvDose = (TextView) itemView.findViewById(R.id.tvDose);
        tvMedicine = (TextView) itemView.findViewById(R.id.tvMedicine);
        cbActive = (CheckBox) itemView.findViewById(R.id.cbActive);
        rlMainContent = (RelativeLayout) itemView.findViewById(R.id.rlMainContent);


        tvDateStarted.setTypeface(typefaceCR);
        tvNext.setTypeface(typefaceCR);
        tvDaysLeft.setTypeface(typefaceCR);
        tvMedicine.setTypeface(typefaceCR);
        tvDose.setTypeface(typefaceCR);

        cbActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = cbActive.isChecked();
                medicationPlanHistoryUpdaterListener.updateMedicationPlan(getAdapterPosition(),isChecked);
            }
        });
    }
}
