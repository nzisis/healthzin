package com.ngng.healthzin.Adapters.History;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
//import com.ngng.healthzin.Model.Database.Nutrition;
import com.ngng.healthzin.Adapters.History.ScreenSlidePageAdapters.ExerciseScreenSlidePagerAdapter;
import com.ngng.healthzin.Adapters.History.ScreenSlidePageAdapters.MedicationScreenSlidePagerAdapter;
import com.ngng.healthzin.Adapters.History.ScreenSlidePageAdapters.NutritionScreenSlidePagerAdapter;
import com.ngng.healthzin.Model.History.ExerciseHistory;
import com.ngng.healthzin.Model.History.MedicationHistory;
import com.ngng.healthzin.Model.History.NutritionHistory;
import com.ngng.healthzin.Model.Realm.Exercise;
import com.ngng.healthzin.Model.Realm.Medication;
import com.ngng.healthzin.Model.Realm.Nutrition;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nzisis on 13/12/16.
 */
public class HistoryTabAdapter extends ExpandableRecyclerViewAdapter<HistoryGroupHolder, HistoryItemHolder> {


    private Context context;
    private int indicator;

    public HistoryTabAdapter(List<? extends ExpandableGroup> groups, Context context, int indicator) {
        super(groups);
        this.context = context;
        this.indicator = indicator;
    }

    @Override
    public HistoryGroupHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_group_item, parent, false);
        return new HistoryGroupHolder(view,context);
    }

    @Override
    public HistoryItemHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_child, parent, false);
        return new HistoryItemHolder(view,context);
    }

    @Override
    public void onBindChildViewHolder(HistoryItemHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {

        switch (indicator){
            case 1:
                ArrayList<Nutrition> items = ((NutritionHistory) group).getInnerItems().get(childIndex);
                if(!items.isEmpty()) {
                    String formatedDate = TimeManager.getMonthAndDayFromDate(items.get(0).getDate());
                    holder.tvDate.setText(formatedDate);
                }

                NutritionScreenSlidePagerAdapter screenSlidePagerAdapter = new NutritionScreenSlidePagerAdapter(context,items);
                holder.mPager.setAdapter(screenSlidePagerAdapter);
                holder.setup();
                break;
            case 2:
                ArrayList<Exercise> exerciseItems = ((ExerciseHistory) group).getInnerItems().get(childIndex);
                if(!exerciseItems.isEmpty()) {
                    String formatedDate = TimeManager.getMonthAndDayFromDate(exerciseItems.get(0).getDate());
                    holder.tvDate.setText(formatedDate);
                }

                ExerciseScreenSlidePagerAdapter exerciseScreenSlidePagerAdapter = new ExerciseScreenSlidePagerAdapter(context,exerciseItems);
                holder.mPager.setAdapter(exerciseScreenSlidePagerAdapter);
                holder.setup();
                break;
            case 3:
                ArrayList<Medication> medicationItems = ((MedicationHistory) group).getInnerItems().get(childIndex);
                if(!medicationItems.isEmpty()) {
                    String formatedDate = TimeManager.getMonthAndDayFromDate(medicationItems.get(0).getDate());
                    holder.tvDate.setText(formatedDate);
                }

                MedicationScreenSlidePagerAdapter medicationScreenSlidePagerAdapter = new MedicationScreenSlidePagerAdapter(context,medicationItems);
                holder.mPager.setAdapter(medicationScreenSlidePagerAdapter);
                holder.setup();
                break;


        }


    }

    @Override
    public void onBindGroupViewHolder(HistoryGroupHolder holder, int flatPosition, ExpandableGroup group) {
       holder.tvGroupName.setText(group.getTitle());
    }


    public void notifyList(List<? extends ExpandableGroup> groups){
        this.expandableList = new ExpandableList(groups);

        notifyDataSetChanged();
    }
}
