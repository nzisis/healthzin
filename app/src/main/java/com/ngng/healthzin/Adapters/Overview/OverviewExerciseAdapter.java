package com.ngng.healthzin.Adapters.Overview;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ngng.healthzin.Fragments.Overview.Interfaces.OverviewExerciseListener;
import com.ngng.healthzin.Model.Overview.OverviewExerciseItem;
import com.ngng.healthzin.R;

import java.util.ArrayList;

import me.grantland.widget.AutofitTextView;

/**
 * Created by nzisis on 21/01/17.
 */
public class OverviewExerciseAdapter extends FragmentStatePagerAdapter {
    private ArrayList<OverviewExerciseItem> items;
    private OverviewExerciseListener overviewExerciseListener;
    private boolean next;


    public OverviewExerciseAdapter(FragmentManager fragmentManager, Context context, ArrayList<OverviewExerciseItem> items, OverviewExerciseListener overviewExerciseListener) {
        super(fragmentManager);
        this.items = items;
        this.overviewExerciseListener = overviewExerciseListener;

        if (items.size() == 1) next = false;
        else next = true;
    }

    @Override
    public Fragment getItem(int position) {
        OverviewExerciseFragment overviewExerciseFragment = OverviewExerciseFragment.newInstance(items.get(position), next);
        overviewExerciseFragment.setOverviewExerciseListener(overviewExerciseListener);

        return overviewExerciseFragment;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        //super.restoreState(state, loader);
    }



    public static class OverviewExerciseFragment extends Fragment {

        private TextView tvExercise, tvTimeAgo, tvTime;
        private AutofitTextView tvHeader;
        private ImageView ivNavigation;
        private OverviewExerciseItem exerciseItem;
        private OverviewExerciseListener overviewExerciseListener;
        private boolean next;


        static OverviewExerciseFragment newInstance(OverviewExerciseItem exerciseItem, boolean next) {
            OverviewExerciseFragment overviewExerciseFragment = new OverviewExerciseFragment();

            Bundle bundle = new Bundle();
            bundle.putSerializable("item", exerciseItem);
            bundle.putBoolean("next", next);
            overviewExerciseFragment.setArguments(bundle);


            return overviewExerciseFragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            init();
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View itemView = inflater.inflate(R.layout.overview_exercise_item, container, false);


            tvExercise = (TextView) itemView.findViewById(R.id.tvExercise);
            tvTimeAgo = (TextView) itemView.findViewById(R.id.tvExerciseHoursAgo);
            tvTime = (TextView) itemView.findViewById(R.id.tvExerciseHour);
            tvHeader = (AutofitTextView) itemView.findViewById(R.id.tvExerciseHeader);

            ivNavigation = (ImageView) itemView.findViewById(R.id.ivArrow);

            return itemView;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            initUI();
            initListeners();

        }

        public void setOverviewExerciseListener(OverviewExerciseListener overviewExerciseListener) {
            this.overviewExerciseListener = overviewExerciseListener;
        }

        private void init() {

            if (getArguments() != null) {
                exerciseItem = (OverviewExerciseItem) getArguments().getSerializable("item");
                next = getArguments().getBoolean("next");
            }
        }

        private void initUI() {
            Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
            tvExercise.setTypeface(typefaceCR);
            tvTimeAgo.setTypeface(typefaceCR);
            tvTime.setTypeface(typefaceCR);
            tvHeader.setTypeface(typefaceCR);


            if (exerciseItem != null) {
                tvExercise.setText(exerciseItem.getExercise());
                tvTime.setText(exerciseItem.getTime());
                tvTimeAgo.setText(exerciseItem.getTime_ago());
                tvHeader.setText(exerciseItem.getHeader());

                if (!next) {
                    ivNavigation.setVisibility(View.GONE);
                } else {
                    ivNavigation.setVisibility(View.VISIBLE);
                    if (exerciseItem.getNext()) {
                        //Rightς arrow
                        ivNavigation.setImageResource(R.drawable.back_arrow_right);

                    } else {
                        //Left arrow
                        ivNavigation.setImageResource(R.drawable.back_arrow_left);
                    }
                }
            }

        }


        private void initListeners() {
            ivNavigation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    overviewExerciseListener.moveToNext();
                }
            });
        }

    }


}
