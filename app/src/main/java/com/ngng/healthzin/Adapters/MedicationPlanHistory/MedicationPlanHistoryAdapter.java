package com.ngng.healthzin.Adapters.MedicationPlanHistory;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces.MedicationPlanHistoryInnerListener;
import com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces.MedicationPlanHistoryTabInnerListener;
import com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces.MedicationPlanHistoryUpdaterListener;
import com.ngng.healthzin.Model.Realm.MedicationPlanEvent;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.AlarmManagerSetter;
import com.ngng.healthzin.Utils.TimeManager;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by nzisis on 02/02/17.
 */
public class MedicationPlanHistoryAdapter extends RecyclerView.Adapter<MedicationPlanHistoryHolder> {

    private LinkedHashMap<MedicationPlanEvent,Long> items;
    private ArrayList<MedicationPlanEvent> keys;
    private ArrayList<Long> values;
    private int indicator;
    private Context context;
    private MedicationPlanHistoryTabInnerListener medicationPlanHistoryTabInnerListener;
    private MedicationPlanHistoryInnerListener medicationPlanHistoryInnerListener;

    public MedicationPlanHistoryAdapter(LinkedHashMap<MedicationPlanEvent,Long> items,int indicator,Context context,MedicationPlanHistoryTabInnerListener medicationPlanHistoryTabInnerListener,MedicationPlanHistoryInnerListener medicationPlanHistoryInnerListener){
        this.items = items;
        this.indicator = indicator;
        this.context = context;
        this.medicationPlanHistoryTabInnerListener = medicationPlanHistoryTabInnerListener;
        this.medicationPlanHistoryInnerListener = medicationPlanHistoryInnerListener;

        keys = new ArrayList<>(items.keySet());
        values = new ArrayList<>(items.values());

    }

    @Override
    public MedicationPlanHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater =  LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.medication_plan_history_item,parent,false);

        return new MedicationPlanHistoryHolder(view,medicationPlanHistoryUpdaterListener,context);
    }

    @Override
    public void onBindViewHolder(MedicationPlanHistoryHolder holder, int position) {
        MedicationPlanEvent medicationPlanEvent = keys.get(position);
        String formatedDate = TimeManager.getMonthAndDayFromDate(medicationPlanEvent.getDate());

        holder.tvDateStarted.setText(formatedDate);
        holder.tvMedicine.setText(medicationPlanEvent.getMedicine());
        holder.tvDose.setText(medicationPlanEvent.getDose()+medicationPlanEvent.getDose_meter());


      if(indicator == 1){
        holder.cbActive.setChecked(true);
        holder.tvDaysLeft.setText(medicationPlanEvent.getDays_left() + " "+context.getString(R.string.days_left) );
        holder.tvNext.setText(context.getString(R.string.next)+" "+TimeManager.futureTimeDifferenceString(values.get(position),context).toLowerCase());
      }else{
          holder.tvDose.setTextColor(ContextCompat.getColor(context,R.color.white_trans50));
          holder.tvDaysLeft.setTextColor(ContextCompat.getColor(context,R.color.white_trans50));
          holder.tvMedicine.setTextColor(ContextCompat.getColor(context,R.color.white_trans50));
          holder.tvNext.setTextColor(ContextCompat.getColor(context,R.color.app_blue_trans50));


          holder.cbActive.setChecked(false);
          holder.tvNext.setText(context.getString(R.string.at)+" "+medicationPlanEvent.getTime());
          holder.tvDaysLeft.setText(context.getString(R.string.total_days)+" " +medicationPlanEvent.getTotal_days() );
      }


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void notifyData( LinkedHashMap<MedicationPlanEvent,Long> items){
        this.items = items;
        keys = new ArrayList<>(items.keySet());
        values = new ArrayList<>(items.values());

        medicationPlanHistoryTabInnerListener.updateMedicationPlanView(items.isEmpty(),indicator);
        notifyDataSetChanged();
    }

    private MedicationPlanHistoryUpdaterListener medicationPlanHistoryUpdaterListener = new MedicationPlanHistoryUpdaterListener() {
        @Override
        public void updateMedicationPlan(int position,boolean isChecked) {
            MedicationPlanEvent medicationPlanEvent = keys.get(position);

            medicationPlanHistoryTabInnerListener.updateMedicationPlanDetails(isChecked, medicationPlanEvent);
            medicationPlanHistoryInnerListener.updateMedicationPlan();

        }
    };
}
