package com.ngng.healthzin.Adapters.MedicationPlan;

//import android.support.v4.app.FragmentManager;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.ngng.healthzin.Fragments.Medication.Interfaces.MedicationPlanListener;
import com.ngng.healthzin.Fragments.Medication.Interfaces.MedicationPlanUpdater;
import com.ngng.healthzin.R;

import java.util.Calendar;

/**
 * Created by nzisis on 28/11/16.
 */
public class MedicationPlanViewHolder extends RecyclerView.ViewHolder {

    public TextView tvHeader,tvTime;
    public ImageView ivTime;

    private String time;
    private Context context;
    private RadialTimePickerDialogFragment rtime;
    private MedicationPlanUpdater medicationPlanUpdater;


    public MedicationPlanViewHolder(View itemView, final MedicationPlanListener medicationPlanListener,MedicationPlanUpdater medicationPlanUpdater,Context context) {
        super(itemView);


        this.medicationPlanUpdater = medicationPlanUpdater;
        this.context = context;

        tvHeader = (TextView) itemView.findViewById(R.id.tvHeader);
        tvTime = (TextView) itemView.findViewById(R.id.tvTime);
        ivTime = (ImageView) itemView.findViewById(R.id.ivTime);

        init();

        ivTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicationPlanListener.showTimeDialog(rtime);
            }
        });

    }


    private void init(){
        Typeface typefaceCR = Typeface.createFromAsset(context.getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvTime.setTypeface(typefaceCR);
        tvHeader.setTypeface(typefaceCR);


        Calendar calendar = Calendar.getInstance();

        rtime = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(timeSetListener)
                .setDoneText("Done")
                .setCancelText("Cancel")
                .setStartTime(calendar.getTime().getHours(), calendar.getTime().getMinutes());

    }

    private RadialTimePickerDialogFragment.OnTimeSetListener timeSetListener = new RadialTimePickerDialogFragment.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
            String hours, mins;
            if (hourOfDay < 10)
                hours = "0" + hourOfDay;
            else
                hours = String.valueOf(hourOfDay);
            if (minute < 10)
                mins = "0" + minute;
            else
                mins = String.valueOf(minute);

            time = hours + ":" + mins;

            tvTime.setText(time);
            medicationPlanUpdater.updateItem(getAdapterPosition(),time);

        }
    };


}
