package com.ngng.healthzin.Adapters.History;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ngng.healthzin.Fragments.History.ScreenSlideNutritionFragment;

import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;
import com.ngng.healthzin.Utils.WrapContentHeightViewPager;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.util.ArrayList;

import me.grantland.widget.AutofitTextView;

/**
 * Created by nzisis on 13/12/16.
 */
public class HistoryItemHolder extends ChildViewHolder {

    public AutofitTextView tvDate;
    public ViewPager mPager;
    public ImageView ivRightArrow,ivLeftArrow;
    private int currentPos;


    public HistoryItemHolder(View itemView,Context context) {
        super(itemView);

        Typeface typefaceCR = Typeface.createFromAsset(context.getAssets(), "fonts/Comfortaa-Regular.ttf");

        tvDate = (AutofitTextView) itemView.findViewById(R.id.tvDate);
        mPager = (ViewPager) itemView.findViewById(R.id.pager);
        ivRightArrow = (ImageView) itemView.findViewById(R.id.ivRightArrow);
        ivLeftArrow = (ImageView) itemView.findViewById(R.id.ivLeftArrow);


        tvDate.setTypeface(typefaceCR);

        ivRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPos = mPager.getCurrentItem();

                int nextPos = (currentPos + 1)%mPager.getAdapter().getCount();
                if(nextPos == 0) ivLeftArrow.setVisibility(View.GONE);
                else ivLeftArrow.setVisibility(View.VISIBLE);

                mPager.setCurrentItem(nextPos);
            }
        });


        ivLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPos = mPager.getCurrentItem();

                int nextPos = currentPos-1;
                if(nextPos>=0){
                    if(nextPos == 0) ivLeftArrow.setVisibility(View.GONE);
                    else ivLeftArrow.setVisibility(View.VISIBLE);

                    mPager.setCurrentItem(nextPos);
                }

            }
        });

    }

    public void setup(){
        if(mPager.getAdapter().getCount() == 1){
            ivLeftArrow.setVisibility(View.GONE);
            ivRightArrow.setVisibility(View.GONE);
        }else{
            ivRightArrow.setVisibility(View.VISIBLE);

            currentPos = mPager.getCurrentItem();
            if(currentPos == 0) ivLeftArrow.setVisibility(View.GONE);
            else ivLeftArrow.setVisibility(View.VISIBLE);
        }


    }


}
