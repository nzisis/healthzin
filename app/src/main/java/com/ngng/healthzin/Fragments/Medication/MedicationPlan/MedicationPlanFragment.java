package com.ngng.healthzin.Fragments.Medication.MedicationPlan;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.ngng.healthzin.Activities.OverviewActivity;
import com.ngng.healthzin.Activities.SettingsActivity;
import com.ngng.healthzin.Adapters.AutoCompleteTextview.AutoCompleteTextViewAdapter;
import com.ngng.healthzin.Adapters.MedicationPlan.MedicationPlanAdapter;
import com.ngng.healthzin.Fragments.Medication.Interfaces.MedicationPlanListener;
import com.ngng.healthzin.Fragments.Settings.Interfaces.MedicationPlanSettingsListener;
import com.ngng.healthzin.Model.MedicationPlan.MedicationPlanItem;
import com.ngng.healthzin.Model.Realm.MedicationPlanEvent;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.AlarmManagerSetter;
import com.ngng.healthzin.Utils.DividerItemDecoration;
import com.ngng.healthzin.Utils.TimeManager;

import java.util.ArrayList;

/**
 * Created by nzisis on 28/11/16.
 */
public class MedicationPlanFragment extends Fragment {

    private TextView tvMedicineHeader, tvDaysHeader, tvTimesPerDaysHeader, tvDoseHeader, tvMedicationPlanHeader, tvDifferentDaysHeader, tvTimesPerDay;
    private EditText etDays, etDose;
    private AutoCompleteTextView etMedicine;
    private TextView fabAdd, fabSubstract;
    private Spinner spDose;
    private Toolbar toolbar;
    private RecyclerView rvTimes;
    private LinearLayout llTimesPerDay;

    private ArrayList<MedicationPlanItem> items;
    private RealmDatabase db;

    private MedicationPlanAdapter adapter;
    private MedicationPlanListener medicationPlanListener;
    private MedicationPlanSettingsListener medicationPlanSettingsListener;

    private boolean flow, layout_active;
    private String dose_meter;
    private int times_per_day;

    private View doneView;
    private ShowcaseView svMedication, svSecondMedication;
    private int showcase;
    private SharedPreferences sharedPreferences;


    private AutoCompleteTextViewAdapter autoCompleteTextViewAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (getArguments() != null) {
            flow = getArguments().getBoolean("flow");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_medication_plan, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.navigation_back_arrow);

        if (flow) ((OverviewActivity) getActivity()).setSupportActionBar(toolbar);
        else ((SettingsActivity) getActivity()).setSupportActionBar(toolbar);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!flow && medicationPlanSettingsListener != null)
                    medicationPlanSettingsListener.setup();
                getActivity().onBackPressed();

            }
        });

        toolbar.setTitle(getString(R.string.medication_plan));

        tvMedicineHeader = (TextView) view.findViewById(R.id.tvMedicineHeader);
        tvDaysHeader = (TextView) view.findViewById(R.id.tvDaysHeader);
        tvTimesPerDaysHeader = (TextView) view.findViewById(R.id.tvTimesPerDaysHeader);
        tvDoseHeader = (TextView) view.findViewById(R.id.tvDoseHeader);
        tvMedicationPlanHeader = (TextView) view.findViewById(R.id.tvMedicationPlanHeader);
        tvDifferentDaysHeader = (TextView) view.findViewById(R.id.tvDifferentTimesHeader);
        tvTimesPerDay = (TextView) view.findViewById(R.id.tvTimesPerDay);


        etDose = (EditText) view.findViewById(R.id.etDose);
        etMedicine = (AutoCompleteTextView) view.findViewById(R.id.etMedicine);
        etDays = (EditText) view.findViewById(R.id.etDays);

        spDose = (Spinner) view.findViewById(R.id.spDose);

        fabAdd = (TextView) view.findViewById(R.id.fabAdd);
        fabSubstract = (TextView) view.findViewById(R.id.fabSubtract);


        rvTimes = (RecyclerView) view.findViewById(R.id.rvTimes);
        rvTimes.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTimes.addItemDecoration(new DividerItemDecoration(getActivity().getResources().getDrawable(R.drawable.divider), false, false));

        llTimesPerDay = (LinearLayout) view.findViewById(R.id.llTimesPerDay);

        fabAdd.setVisibility(View.GONE);
        fabSubstract.setVisibility(View.GONE);

        setHasOptionsMenu(true);

        return view;

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();
    }


    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_medication_plan, menu);
        new Handler().post(new Runnable() {
            @Override
            public void run() {

                MenuItem done = menu.findItem(R.id.action_Done);

                Activity activity = getActivity();
                if (activity != null) {
                    if (flow)
                        doneView = ((OverviewActivity) getActivity()).findViewById(done.getItemId());
                    else
                        doneView = ((SettingsActivity) getActivity()).findViewById(done.getItemId());
                }


            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_Done:
                Log.e("Done", "Pressed");
                addMedicationPlan();
                break;

        }

        return false;
    }

    private void addMedicationPlan() {
        if (etMedicine.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getString(R.string.enter_medicine), Toast.LENGTH_SHORT).show();
            return;
        }

        if (etDose.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getString(R.string.enter_dose), Toast.LENGTH_SHORT).show();
            return;
        }

        if (etDays.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getString(R.string.enter_days), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!checkDifferentTimes()) {
            Toast.makeText(getActivity(), getString(R.string.enter_different_times), Toast.LENGTH_SHORT).show();
            return;
        }

        ArrayList<MedicationPlanItem> items = adapter.getItems();
        ArrayList<MedicationPlanEvent> events = new ArrayList<>();


        for (int i = 0; i < items.size(); i++) {
            String time = items.get(i).getTime();
            MedicationPlanEvent medicationPlanEvent = new MedicationPlanEvent(etMedicine.getText().toString(), TimeManager.getCurrentDate(), Integer.parseInt(etDays.getText().toString()), time, Integer.parseInt(etDays.getText().toString()), Double.parseDouble(etDose.getText().toString()), dose_meter);
            medicationPlanEvent.setId(db.getMedicationPlanEventNextKey());

            db.insertOrUpdateMedicationPlan(medicationPlanEvent);
            events.add(medicationPlanEvent);
        }


        AlarmManagerSetter.setAlarmManager(events, getActivity());


        if (!flow && medicationPlanSettingsListener != null) medicationPlanSettingsListener.setup();
        medicationPlanListener.medicationPlanAdded();
    }


    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");

        tvMedicationPlanHeader.setTypeface(typefaceCR);
        tvMedicineHeader.setTypeface(typefaceCR);
        tvDoseHeader.setTypeface(typefaceCR);
        tvDaysHeader.setTypeface(typefaceCR);
        tvTimesPerDaysHeader.setTypeface(typefaceCR);
        tvDifferentDaysHeader.setTypeface(typefaceCR);
        tvTimesPerDay.setTypeface(typefaceCR);
        etMedicine.setTypeface(typefaceCR);
        etDose.setTypeface(typefaceCR);
        etDays.setTypeface(typefaceCR);


        autoCompleteTextViewAdapter = new AutoCompleteTextViewAdapter(getActivity(), 3);
        etMedicine.setAdapter(autoCompleteTextViewAdapter);


        dose_meter = "ml";
        times_per_day = 1;
        layout_active = false;


        showcase = 0;

        if (!sharedPreferences.getBoolean("medicationPlanShowcase", false)) {

            if (flow) ((OverviewActivity) getActivity()).setDisable_back_button(true);
            else ((SettingsActivity) getActivity()).setDisable_back_button(true);

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);


            svMedication = new ShowcaseView.Builder(getActivity())
                    .withMaterialShowcase()
                    .setTarget(Target.NONE)
                    .setContentTitle(getString(R.string.medication_plan) + " (1/4)")
                    .setContentText(getString(R.string.instructions_medication_plan_1))
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setOnClickListener(showCaseListener)
                    .build();

            svMedication.setButtonText(getString(R.string.instructions_got_it));
            svMedication.setButtonPosition(lps);
        }


        String categories[] = getResources().getStringArray(R.array.dose_meter);
        ArrayList<String> finalCategories = new ArrayList<>();
        for (int i = 0; i < categories.length; i++) {
            finalCategories.add(categories[i]);
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item_blue, R.id.tvSpinnerCategories, finalCategories);
        spDose.setAdapter(arrayAdapter);


        db = new RealmDatabase(getActivity());

        String headers_array[] = getResources().getStringArray(R.array.medication_plan_headers);
        items = new ArrayList<>();
        for (int i = 0; i < headers_array.length; i++) {
            items.add(new MedicationPlanItem(headers_array[i], ""));
        }


        ArrayList<MedicationPlanItem> medicationPlanItems = new ArrayList<>();
        medicationPlanItems.add(items.get(0));

        adapter = new MedicationPlanAdapter(medicationPlanItems, medicationPlanListener, getActivity());
        rvTimes.setAdapter(adapter);


    }


    private void initListeners() {

        spDose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dose_meter = spDose.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (times_per_day != 6) {
                    times_per_day++;
                    tvTimesPerDay.setText(times_per_day + "");
                    notifyData(times_per_day);
                }


            }
        });

        fabSubstract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (times_per_day != 1) {
                    times_per_day--;
                    tvTimesPerDay.setText(times_per_day + "");
                    notifyData(times_per_day);
                }
            }
        });

        tvTimesPerDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!layout_active) {
                    fabAdd.setVisibility(View.VISIBLE);
                    fabSubstract.setVisibility(View.VISIBLE);
                } else {
                    fabAdd.setVisibility(View.GONE);
                    fabSubstract.setVisibility(View.GONE);
                }
                layout_active = !layout_active;
            }


        });


    }

    public void setMedicationPlanListener(MedicationPlanListener medicationPlanListener) {
        this.medicationPlanListener = medicationPlanListener;
    }

    public void setMedicationPlanSettingsListener(MedicationPlanSettingsListener medicationPlanSettingsListener) {
        this.medicationPlanSettingsListener = medicationPlanSettingsListener;
    }

    private boolean checkDifferentTimes() {
        ArrayList<MedicationPlanItem> items = adapter.getItems();

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getTime().equals("")) {
                return false;
            }
        }

        return true;
    }

    private void notifyData(int times) {
        ArrayList<MedicationPlanItem> current_items = new ArrayList<>();

        for (int i = 0; i < times; i++) {
            current_items.add(items.get(i));
        }

        adapter.notify(current_items);
    }

    @Override
    public void onDetach() {
        if (!flow && medicationPlanSettingsListener != null) medicationPlanSettingsListener.setup();
        super.onDetach();

    }

    private View.OnClickListener showCaseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            svMedication.hide();

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);

            Target viewTarget = new ViewTarget(R.id.tvTimesPerDay, getActivity());
            svSecondMedication = new ShowcaseView.Builder(getActivity())
                    .withMaterialShowcase()
                    .blockAllTouches()
                    .setTarget(viewTarget)
                    .setContentTitle(getString(R.string.medication_plan) + " (2/4)")
                    .setContentText(getString(R.string.instructions_medication_plan_2))
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setOnClickListener(showCaseSecondListener)
                    .build();

            svSecondMedication.setButtonText(getString(R.string.instructions_got_it));
            svSecondMedication.setButtonPosition(lps);


            showcase++;
        }
    };

    private View.OnClickListener showCaseSecondListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (showcase) {
                case 1:
                    View view = rvTimes.findViewHolderForAdapterPosition(0).itemView.findViewById(R.id.ivTime);
                    svSecondMedication.setContentTitle("");
                    svSecondMedication.setContentText("");
                    svSecondMedication.setShowcase(new ViewTarget(view), true);
                    svSecondMedication.setContentTitle(getString(R.string.medication_plan) + " (3/4)");
                    svSecondMedication.setContentText(getString(R.string.instructions_medication_plan_3));
                    svSecondMedication.setButtonText(getString(R.string.instructions_got_it));
                    break;
                case 2:
                    svSecondMedication.setContentTitle("");
                    svSecondMedication.setContentText("");
                    svSecondMedication.setShowcase(new ViewTarget(doneView), true);
                    svSecondMedication.setContentTitle(getString(R.string.medication_plan) + " (4/4)");
                    svSecondMedication.setContentText(getString(R.string.instructions_medication_plan_4));
                    svSecondMedication.setButtonText(getString(R.string.instructions_got_it));
                    break;

                case 3:
                    svSecondMedication.hide();
                    if (flow) ((OverviewActivity) getActivity()).setDisable_back_button(false);
                    else ((SettingsActivity) getActivity()).setDisable_back_button(false);
                    sharedPreferences.edit().putBoolean("medicationPlanShowcase", true).apply();
                    break;
            }
            showcase++;
        }
    };
}
