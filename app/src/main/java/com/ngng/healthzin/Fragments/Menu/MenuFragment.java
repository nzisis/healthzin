package com.ngng.healthzin.Fragments.Menu;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.ngng.healthzin.Adapters.Menu.MenuAdapter;
import com.ngng.healthzin.Fragments.Menu.Interfaces.MenuCallback;
import com.ngng.healthzin.Model.Menu.MenuItem;
import com.ngng.healthzin.Model.User.UserInstance;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.ImageHandler;
import java.util.ArrayList;

/**
 * Created by nzisis on 11/5/16.
 */
public class MenuFragment extends Fragment {


    private TextView tvName,tvSettings,tvProfile;
    private LinearLayout llSettings,llProfile;
    private ImageView ivProfile,ivSettings;
    private RecyclerView rvMenu;
    private RoundedImageView rivProfile;
    private ArrayList<MenuItem> menuItems;
    private MenuAdapter menuAdapter;
    private MenuCallback menuCallback;
    private int currentPosition;
    private SharedPreferences sharedPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu,container,false);

        tvName = (TextView) view.findViewById(R.id.tvName);
        tvSettings = (TextView) view.findViewById(R.id.tvSettings);
        tvProfile = (TextView) view.findViewById(R.id.tvProfile);

        rivProfile = (RoundedImageView) view.findViewById(R.id.ivProfile);


        ivProfile = (ImageView) view.findViewById(R.id.ivMenuProfile);
        ivSettings = (ImageView) view.findViewById(R.id.ivSettings);

        llSettings = (LinearLayout) view.findViewById(R.id.llSettings);
        llProfile = (LinearLayout) view.findViewById(R.id.llProfile);

        rvMenu = (RecyclerView) view.findViewById(R.id.rvMenu);
        rvMenu.setLayoutManager(new LinearLayoutManager(getActivity()));



        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initMenuItems();
        initListeners();
    }

    private void initListeners() {
        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuCallback.profileClicked();
            }
        });
        llSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuCallback.settingClicked();
            }
        });
    }


    private void initMenuItems(){
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvProfile.setTypeface(typefaceCR);
        tvName.setTypeface(typefaceCR);
        tvSettings.setTypeface(typefaceCR);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        currentPosition = getArguments().getInt("menu_position");

        menuItems = new ArrayList<>();
        menuItems.add(new MenuItem(getString(R.string.overview),R.drawable.overview_icon,R.drawable.overview_icon_selected,false));
        menuItems.add(new MenuItem(getString(R.string.nutrition),R.drawable.nutricion_white,R.drawable.nutrition_menu_selected,false));
        menuItems.add(new MenuItem(getString(R.string.medication),R.drawable.medication_white,R.drawable.medication_menu_selected,false));
        menuItems.add(new MenuItem(getString(R.string.exercise),R.drawable.exercise_white,R.drawable.exercise_menu_selected,false));
        menuItems.add(new MenuItem(getString(R.string.history), R.drawable.history_data_icon, R.drawable.history_data_icon_selected, false));

        menuItems.get(currentPosition).setSelected(true);

        menuAdapter = new MenuAdapter(menuItems,menuCallback,getActivity());
        rvMenu.setAdapter(menuAdapter);

        tvName.setText(UserInstance.getInstance().getFirst_name() + " " + UserInstance.getInstance().getLast_name());


        String path = sharedPreferences.getString("profile_path", "");
        if (!path.equals("")) {
            Bitmap bitmap = ImageHandler.decodeSampledBitmapFromPath(path, 100, 100);
             if (bitmap != null) rivProfile.setImageBitmap(ImageHandler.rotatePicture(bitmap, path, getActivity()));
             else rivProfile.setImageResource(R.drawable.no_profile_icon);

        } else {
            rivProfile.setImageResource(R.drawable.no_profile_icon);
        }
    }

    public void notify(int position){
        if(menuItems!=null){
            for(int i=0; i<menuItems.size(); i++){
                menuItems.get(i).setSelected(false);
            }

            if(position<=4){
                menuItems.get(position).setSelected(true);

                tvSettings.setTextColor(getResources().getColor(R.color.white));
                tvProfile.setTextColor(getResources().getColor(R.color.white));

                ivProfile.setImageResource(R.drawable.profile_icon_data);
                ivSettings.setImageResource(R.drawable.settings_data_icon);
            }else if(position == 5){
                tvSettings.setTextColor(getResources().getColor(R.color.white));
                ivSettings.setImageResource(R.drawable.settings_data_icon);

                tvProfile.setTextColor(getResources().getColor(R.color.app_blue));
                ivProfile.setImageResource(R.drawable.profile_icon_data_selected);
            }else{
                tvProfile.setTextColor(getResources().getColor(R.color.white));
                ivProfile.setImageResource(R.drawable.profile_icon_data);

            }

            menuAdapter.notify(menuItems);
        }
    }

    public void updateName(){
        tvName.setText(UserInstance.getInstance().getFirst_name() + " " + UserInstance.getInstance().getLast_name());

        if(sharedPreferences ==null)  sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String path = sharedPreferences.getString("profile_path", "");
        if (!path.equals("")) {
            Bitmap bitmap = ImageHandler.decodeSampledBitmapFromPath(path, 100, 100);
            if (bitmap != null) rivProfile.setImageBitmap(ImageHandler.rotatePicture(bitmap, path, getActivity()));
            else rivProfile.setImageResource(R.drawable.no_profile_icon);

        } else {
            rivProfile.setImageResource(R.drawable.no_profile_icon);
        }
    }


    public void setMenuCallback(MenuCallback menuCallback){
        this.menuCallback = menuCallback;
    }
}
