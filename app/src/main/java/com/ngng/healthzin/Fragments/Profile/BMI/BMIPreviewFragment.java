package com.ngng.healthzin.Fragments.Profile.BMI;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ngng.healthzin.Model.User.UserInstance;
import com.ngng.healthzin.R;

import java.text.DecimalFormat;

import me.grantland.widget.AutofitTextView;

/**
 * Created by nzisis on 18/01/17.
 */
public class BMIPreviewFragment extends Fragment {

    private AutofitTextView tvBMI;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bmi_preview,container,false);

        tvBMI = (AutofitTextView) view.findViewById(R.id.tvBMI);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvBMI.setTypeface(typefaceCR);


        if(UserInstance.getInstance().getWeight() == -1 || UserInstance.getInstance().getHeight() == -1){
            tvBMI.setVisibility(View.GONE);
        }else{
            tvBMI.setVisibility(View.VISIBLE);
            float bmi = (UserInstance.getInstance().getWeight() / (UserInstance.getInstance().getHeight()*UserInstance.getInstance().getHeight()));
            DecimalFormat decimalFormat = new DecimalFormat("#.#");
            String category = calculateBMICategory(bmi);
            if(!category.equals(""))  tvBMI.setText(decimalFormat.format(bmi) + " ("+category+")");
            else tvBMI.setText(decimalFormat.format(bmi) + "");

        }
    }

    private String calculateBMICategory(float bmi){

        String[] categories = getActivity().getResources().getStringArray(R.array.bmi_categories);

        if(bmi>0 && bmi<=15) return categories[0];
        else if(bmi>15 && bmi<=16) return categories[1];
        else if(bmi>16 && bmi<=18.5) return categories[2];
        else if(bmi>18.5 && bmi<=25) return categories[3];
        else if(bmi>25 && bmi<=30) return categories[4];
        else if(bmi>30 && bmi<=35) return categories[5];
        else if(bmi>35 && bmi<=40) return categories[6];
        else if(bmi>40) return categories[7];

        return "";
    }

}
