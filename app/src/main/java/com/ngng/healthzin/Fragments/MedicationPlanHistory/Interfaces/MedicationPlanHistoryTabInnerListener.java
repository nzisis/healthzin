package com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces;

import com.ngng.healthzin.Model.Realm.MedicationPlanEvent;

/**
 * Created by nzisis on 10/02/17.
 */
public interface MedicationPlanHistoryTabInnerListener {
    void updateMedicationPlanView(boolean empty,int indicator);
    void updateMedicationPlanDetails(boolean active, MedicationPlanEvent medicationPlanEvent);
}
