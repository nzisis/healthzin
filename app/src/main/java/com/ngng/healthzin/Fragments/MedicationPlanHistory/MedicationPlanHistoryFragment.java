package com.ngng.healthzin.Fragments.MedicationPlanHistory;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentPagerAdapter;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngng.healthzin.Activities.SettingsActivity;
import com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces.MedicationPlanHistoryInnerListener;
import com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces.MedicationPlanHistoryListener;
import com.ngng.healthzin.R;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by nzisis on 02/02/17.
 */
public class MedicationPlanHistoryFragment extends Fragment {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private Fragment firstPage, secondPage;
    private HashMap<String, Fragment> components;

    private ViewPagerAdapter adapter;
    private MedicationPlanHistoryListener medicationPlanHistoryListener;

    private static final Field sChildFragmentManagerField;

    static {
        Field f = null;
        try {
            f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.e("EEEE", "Error getting mChildFragmentManager field", e);
        }
        sChildFragmentManagerField = f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_medication_plan_history, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.navigation_back_arrow);


        ((SettingsActivity) getActivity()).getSupportActionBar().hide();
        toolbar.setTitle(getString(R.string.medication_plan_history));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (ViewPager) view.findViewById(R.id.pager);


        return view;

    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();

    }



    private void init() {
        components = new HashMap<>();

        MedicationPlanHistoryTabFragment activeMedicationPlanHistoryFragment = MedicationPlanHistoryTabFragment.newInstance(1);
        activeMedicationPlanHistoryFragment.setMedicationPlanHistoryListener(medicationPlanHistoryListener);
        activeMedicationPlanHistoryFragment.setMedicationPlanHistoryInnerListener(medicationPlanHistoryInnerListener);
        components.put("ActiveMedicationPlanHistoryTab",activeMedicationPlanHistoryFragment);

        MedicationPlanHistoryTabFragment noactiveMedicationPlanHistoryFragment = MedicationPlanHistoryTabFragment.newInstance(2);
        noactiveMedicationPlanHistoryFragment.setMedicationPlanHistoryInnerListener(medicationPlanHistoryInnerListener);
        components.put("NoActiveMedicationPlanHistoryTab",noactiveMedicationPlanHistoryFragment);

        adapter = new ViewPagerAdapter(getChildFragmentManager());


        adapter.addFragment(getString(R.string.active));
        adapter.addFragment(getString(R.string.not_active));

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);  // clear all scroll flags

    }



    public void setMedicationPlanHistoryListener(MedicationPlanHistoryListener medicationPlanHistoryListener){
        this.medicationPlanHistoryListener = medicationPlanHistoryListener;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                     firstPage = components.get("ActiveMedicationPlanHistoryTab");
                    return firstPage;

                case 1:
                     secondPage = components.get("NoActiveMedicationPlanHistoryTab");
                    return secondPage;

            }

            return null;
        }

        @Override
        public int getCount() {
            return mFragmentTitleList.size();
        }

        public void addFragment(String title) {
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private MedicationPlanHistoryInnerListener medicationPlanHistoryInnerListener = new MedicationPlanHistoryInnerListener() {
        @Override
        public void updateMedicationPlan() {
            if(components!=null && components.size()>1){
                Iterator it = components.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();
                    ((MedicationPlanHistoryTabFragment)pair.getValue()).notifyData();
                }
            }
        }
    };

    @Override
    public void onDetach() {
        super.onDetach();

        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e) {
                Log.e("EEEE", "Error setting mChildFragmentManager field", e);
            }
        }
    }


}
