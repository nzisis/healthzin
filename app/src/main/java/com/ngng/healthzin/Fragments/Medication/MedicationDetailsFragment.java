package com.ngng.healthzin.Fragments.Medication;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ngng.healthzin.R;

import java.util.HashMap;

/**
 * Created by nzisis on 1/11/17.
 */
public class MedicationDetailsFragment extends Fragment {
    private FragmentManager manager;
    private HashMap<String,Fragment> components;

    private String tag;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_medication_details,container,false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {

        manager = getChildFragmentManager();
        components = new HashMap<>();

        tag = "PreviousMedicationFragment";
        PreviousMedicationDetailsFragment previousMedicationDetailsFragment = new PreviousMedicationDetailsFragment();
        components.put(tag, previousMedicationDetailsFragment);

        manager.beginTransaction().replace(R.id.flPreviousMedication, components.get(tag)).commit();

        tag = "NextMedicationFragment";
        NextMedicationDetailsFragment nextMedicationDetailsFragment = new NextMedicationDetailsFragment();
        components.put(tag, nextMedicationDetailsFragment);

        manager.beginTransaction().replace(R.id.flNextMedication, components.get(tag)).commit();

    }
}
