package com.ngng.healthzin.Fragments.MedicationPlanHistory;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ngng.healthzin.Adapters.MedicationPlanHistory.MedicationPlanHistoryAdapter;
import com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces.MedicationPlanHistoryInnerListener;
import com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces.MedicationPlanHistoryTabInnerListener;
import com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces.MedicationPlanHistoryListener;
import com.ngng.healthzin.Model.Realm.MedicationPlanEvent;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.AlarmManagerSetter;
import com.ngng.healthzin.Utils.DividerItemDecoration;
import com.ngng.healthzin.Utils.TimeManager;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by nzisis on 02/02/17.
 */
public class MedicationPlanHistoryTabFragment extends Fragment {
    private RecyclerView rvHistory;
    private LinearLayout llNoData;
    private TextView tvNoDataHeader, tvRecord;
    private FloatingActionButton fabAdd;

    private RealmDatabase db;
    private MedicationPlanHistoryAdapter adapter;
    private LinkedHashMap<MedicationPlanEvent, Long> items;

    private SwipeRefreshLayout refreshLayout;

    private int indicator;

    private MedicationPlanHistoryListener medicationPlanHistoryListener;
    private MedicationPlanHistoryInnerListener medicationPlanHistoryInnerListener;


    public static MedicationPlanHistoryTabFragment newInstance(int indicator) {

        MedicationPlanHistoryTabFragment myFragment = new MedicationPlanHistoryTabFragment();

        Bundle args = new Bundle();
        args.putInt("indicator", indicator);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_medication_plan_history_tab, container, false);

        llNoData = (LinearLayout) view.findViewById(R.id.llNoData);
        tvNoDataHeader = (TextView) view.findViewById(R.id.tvNoDataHeader);
        tvRecord = (TextView) view.findViewById(R.id.tvRecord);
        fabAdd = (FloatingActionButton) view.findViewById(R.id.fabRecord);

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.srLayout);

        rvHistory = (RecyclerView) view.findViewById(R.id.rvHistory);
        rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvHistory.addItemDecoration(new DividerItemDecoration(getActivity().getResources().getDrawable(R.drawable.divider), false, false));


        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();
    }


    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvNoDataHeader.setTypeface(typefaceCR);
        tvRecord.setTypeface(typefaceCR);

        db = new RealmDatabase(getActivity());

        this.indicator = getArguments().getInt("indicator");
        switch (indicator) {
            case 1:
                items = db.getAllActiveMedicationPlanEventsOrdered();
                fabAdd.setVisibility(View.VISIBLE);
                break;
            case 2:
                items = db.getAllNoActiveMedicationPlanEventsOrdered();
                fabAdd.setVisibility(View.GONE);
                break;


        }


        refreshLayout.setRefreshing(false);
        initNoData(items.isEmpty(), indicator);

        if (adapter != null) adapter = null;
        adapter = new MedicationPlanHistoryAdapter(items, indicator, getActivity(), medicationPlanHistoryTabInnerListener,medicationPlanHistoryInnerListener);
        rvHistory.setAdapter(adapter);

    }

    public void setMedicationPlanHistoryListener(MedicationPlanHistoryListener medicationPlanHistoryListener) {
        this.medicationPlanHistoryListener = medicationPlanHistoryListener;
    }

    public void setMedicationPlanHistoryInnerListener(MedicationPlanHistoryInnerListener medicationPlanHistoryInnerListener) {
        this.medicationPlanHistoryInnerListener = medicationPlanHistoryInnerListener;
    }

    private void initListeners() {
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicationPlanHistoryListener.newMedicationPlan();
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                notifyData();
                refreshLayout.setRefreshing(false);
            }
        });
    }

    private void initNoData(boolean flag, int indicator) {

        if (!flag) {
            llNoData.setVisibility(View.GONE);
        } else {
            llNoData.setVisibility(View.VISIBLE);
            if (indicator == 1) {
                tvNoDataHeader.setText(getString(R.string.no_medication_plan));
                tvRecord.setText(getString(R.string.add_medication_plan));
            } else if (indicator == 2) {
                if(db == null) db = new RealmDatabase(getActivity());
                boolean active = db.getAllActiveMedicationPlanEventsOrdered().isEmpty();
                tvRecord.setVisibility(View.GONE);

                if(!active) tvNoDataHeader.setText(getString(R.string.medication_plan_active));
                else tvNoDataHeader.setText(getString(R.string.no_medication_plan));

            }

        }

    }

    public void notifyData() {
        switch (indicator) {
            case 1:
                items = db.getAllActiveMedicationPlanEventsOrdered();
                fabAdd.setVisibility(View.VISIBLE);
                break;
            case 2:
                items = db.getAllNoActiveMedicationPlanEventsOrdered();
                fabAdd.setVisibility(View.GONE);
                break;
        }

        if (adapter != null) adapter.notifyData(items);
    }


    private MedicationPlanHistoryTabInnerListener medicationPlanHistoryTabInnerListener = new MedicationPlanHistoryTabInnerListener() {
        @Override
        public void updateMedicationPlanView(boolean empty, int indicator) {
            initNoData(empty, indicator);
        }

        @Override
        public void updateMedicationPlanDetails(boolean active, MedicationPlanEvent medicationPlanEvent) {
            if(db ==null)  db = new RealmDatabase(getActivity());

            db.updateAMedicationPlanActive(active,medicationPlanEvent.getId());
            if(!active) AlarmManagerSetter.stopAlarmManager(medicationPlanEvent, getActivity());
            else{
              db.updateMedicationPlanDate(TimeManager.getCurrentDate(),medicationPlanEvent.getId());
                ArrayList<MedicationPlanEvent> items = new ArrayList<>();
                items.add(db.getMedicationPlanEventBasedOnID(medicationPlanEvent.getId()));
                AlarmManagerSetter.setAlarmManager(items,getActivity());
            }
        }
    };

}



