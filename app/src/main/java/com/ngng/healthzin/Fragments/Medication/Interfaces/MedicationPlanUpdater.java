package com.ngng.healthzin.Fragments.Medication.Interfaces;

/**
 * Created by nzisis on 29/11/16.
 */
public interface MedicationPlanUpdater {
    void updateItem(int position,String time);
}
