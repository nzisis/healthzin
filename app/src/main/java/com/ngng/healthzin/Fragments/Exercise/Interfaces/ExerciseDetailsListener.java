package com.ngng.healthzin.Fragments.Exercise.Interfaces;

/**
 * Created by vromia on 1/5/17.
 */
public interface ExerciseDetailsListener {
    void showDetails();
}
