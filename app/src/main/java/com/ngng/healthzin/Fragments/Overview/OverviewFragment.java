package com.ngng.healthzin.Fragments.Overview;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;


import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.ngng.healthzin.Activities.OverviewActivity;
import com.ngng.healthzin.Adapters.Overview.OverviewExerciseAdapter;
import com.ngng.healthzin.Adapters.Overview.OverviewMedicationAdapter;
import com.ngng.healthzin.Fragments.History.Interfaces.HistoryListener;
import com.ngng.healthzin.Fragments.Menu.Interfaces.NavigationListener;
import com.ngng.healthzin.Fragments.Overview.Interfaces.OverviewExerciseListener;
import com.ngng.healthzin.Fragments.Overview.Interfaces.OverviewMedicationListener;
import com.ngng.healthzin.Model.Overview.OverviewExerciseItem;
import com.ngng.healthzin.Model.Overview.OverviewMedicationItem;
import com.ngng.healthzin.Model.Realm.Exercise;
import com.ngng.healthzin.Model.Realm.ExercisePlanEvent;
import com.ngng.healthzin.Model.Realm.Medication;
import com.ngng.healthzin.Model.Realm.Nutrition;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;

import java.lang.reflect.Field;
import java.util.ArrayList;

import me.grantland.widget.AutofitTextView;

/**
 * Created by nzisis on 11/24/16.
 */
public class OverviewFragment extends Fragment {

    private Toolbar toolbar;
    private TextView tvNutritionHeader, tvExerciseHeader, tvMedicationHeader;
    private RelativeLayout rlNutritionData, rlExerciseData, rlMedicationData;
    private ImageView  ivNutrition,ivExercise, ivMedication;
    private ViewPager exercisePager, medicationPager;
    private TextView  tvNutritionHour, tvNutritionHourAgo, tvNutritionMeal;
    private AutofitTextView tvLastMealHeader;
    private LinearLayout fabNutriton, fabExercise, fabMedication;

    private NavigationListener navigationListener;
    private HistoryListener historyListener;
    private RealmDatabase db;

    private OverviewExerciseAdapter overviewExerciseAdapter;
    private OverviewMedicationAdapter overviewMedicationAdapter;

    private ShowcaseView svOverview, svHealthzin;
    private int showcase;
    private SharedPreferences sharedPreferences;
    private Animation in;

    private static final Field sChildFragmentManagerField;

    static {
        Field f = null;
        try {
            f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.e("EEEE", "Error getting mChildFragmentManager field", e);
        }
        sChildFragmentManagerField = f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overview, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.menu_drawer);

        ((OverviewActivity) getActivity()).setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showcase == 1) {
                    svOverview.hide();
                    sharedPreferences.edit().putBoolean("overviewShowcase", true).apply();
                    showcase = -1;
                }
                navigationListener.onNavigationClicked();
            }
        });

        toolbar.setTitle(getString(R.string.app_name));

        tvNutritionHeader = (TextView) view.findViewById(R.id.tvNutritionHeader);
        tvExerciseHeader = (TextView) view.findViewById(R.id.tvExerciseHeader);
        tvMedicationHeader = (TextView) view.findViewById(R.id.tvMedicationHeader);

        fabNutriton = (LinearLayout) view.findViewById(R.id.fabNutrition);
        fabExercise = (LinearLayout) view.findViewById(R.id.fabExercise);
        fabMedication = (LinearLayout) view.findViewById(R.id.fabMedication);

        rlNutritionData = (RelativeLayout) view.findViewById(R.id.rlNutritionData);
        rlExerciseData = (RelativeLayout) view.findViewById(R.id.rlExerciseData);
        rlMedicationData = (RelativeLayout) view.findViewById(R.id.rlMedicationData);

        ivExercise = (ImageView) view.findViewById(R.id.ivExercise);
        ivMedication = (ImageView) view.findViewById(R.id.ivMedication);
        ivNutrition = (ImageView) view.findViewById(R.id.ivNutrition);

        exercisePager = (ViewPager) view.findViewById(R.id.exercise_pager);
        medicationPager = (ViewPager) view.findViewById(R.id.medication_pager);

        tvLastMealHeader = (AutofitTextView) view.findViewById(R.id.tvNutritionLastMealHeader);
        tvNutritionHourAgo = (TextView) view.findViewById(R.id.tvNutritionHoursAgo);
        tvNutritionHour = (TextView) view.findViewById(R.id.tvNutritionHour);
        tvNutritionMeal = (TextView) view.findViewById(R.id.tvNutritionMeal);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();
    }


    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        Typeface typefaceCRB = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Bold.ttf");

        tvNutritionHeader.setTypeface(typefaceCRB);
        tvMedicationHeader.setTypeface(typefaceCRB);
        tvExerciseHeader.setTypeface(typefaceCRB);

        tvLastMealHeader.setTypeface(typefaceCR);
        tvNutritionHourAgo.setTypeface(typefaceCR);
        tvNutritionHour.setTypeface(typefaceCR);
        tvNutritionMeal.setTypeface(typefaceCR);

         in = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);

        showcase = 0;

        if (!sharedPreferences.getBoolean("overviewShowcase", false)) {

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);


            svHealthzin = new ShowcaseView.Builder(getActivity())
                    .withMaterialShowcase()
                    .blockAllTouches()
                    .setTarget(Target.NONE)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.instructions_general_description))
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setOnClickListener(generalShowcaseListener)
                    .build();

            svHealthzin.setButtonText(getString(R.string.instructions_got_it));
            svHealthzin.setButtonPosition(lps);
        }


        db = new RealmDatabase(getActivity());
        Nutrition nutrition = db.getTheMostRecentNutrition();

        if (nutrition == null) {
            rlNutritionData.setVisibility(View.GONE);
            ivNutrition.setVisibility(View.VISIBLE);
        } else {
            rlNutritionData.setVisibility(View.VISIBLE);
            ivNutrition.setVisibility(View.GONE);

            tvNutritionHour.setText(nutrition.getTime());

            tvNutritionHourAgo.setText(TimeManager.getTimeDifferencePast(nutrition.getDate(), nutrition.getTime(),getActivity()));
            tvNutritionMeal.setText(nutrition.getFood());
        }

        ExercisePlanEvent nextExercisePlan = db.getNextExercisePlanEvent(TimeManager.getCurrentDate(), TimeManager.getCurrentTime());
        Exercise latestExercise = db.getTheMostRecentExercise();


        if (nextExercisePlan == null && latestExercise == null) {
            rlExerciseData.setVisibility(View.GONE);
            ivExercise.setVisibility(View.VISIBLE);
        } else {
            rlExerciseData.setVisibility(View.VISIBLE);
            ivExercise.setVisibility(View.GONE);

            ArrayList<OverviewExerciseItem> items = new ArrayList<>();

            if (nextExercisePlan != null) {
                String header = getString(R.string.next_exercise);
                String time = nextExercisePlan.getTime();
                String exercise = nextExercisePlan.getExercise();
                boolean next = true;

                //Calculation of time
                String time_difference = TimeManager.getTimeDifferenceFuture(nextExercisePlan.getDate(), nextExercisePlan.getTime(),getActivity());

                OverviewExerciseItem nextExercise = new OverviewExerciseItem(header, time, exercise, next);
                nextExercise.setTime_ago(time_difference);

                items.add(nextExercise);
            }

            if (latestExercise != null) {
                String header = getString(R.string.latest_exercise);
                String time = latestExercise.getTime();
                String exercise = latestExercise.getExercise();
                boolean next = false;

                String time_difference = TimeManager.getTimeDifferencePast(latestExercise.getDate(), latestExercise.getTime(),getActivity());

                OverviewExerciseItem latest = new OverviewExerciseItem(header, time, exercise, next);
                latest.setTime_ago(time_difference);

                items.add(latest);

            }


            overviewExerciseAdapter = new OverviewExerciseAdapter(getChildFragmentManager(), getActivity(), items, overviewExerciseListener);
            exercisePager.setAdapter(overviewExerciseAdapter);

        }


        Medication nextMedicationEvent = db.getNextMedicationPlanEvent();
        Medication latestMedication = db.getTheMostRecentMedication();

        if (nextMedicationEvent == null && latestMedication == null) {
            rlMedicationData.setVisibility(View.GONE);
            ivMedication.setVisibility(View.VISIBLE);
        } else {
            rlMedicationData.setVisibility(View.VISIBLE);
            ivMedication.setVisibility(View.GONE);

            ArrayList<OverviewMedicationItem> items = new ArrayList<>();

            if (nextMedicationEvent != null) {
                String header = getString(R.string.next_medication);
                String time = nextMedicationEvent.getTime();
                String medicine = nextMedicationEvent.getMedicine();
                boolean next = true;

                //Calculation of time
                String time_difference = TimeManager.getTimeDifferenceFuture(nextMedicationEvent.getDate(), nextMedicationEvent.getTime(),getActivity());

                OverviewMedicationItem nextMedication = new OverviewMedicationItem(header, time, medicine, next);
                nextMedication.setTime_ago(time_difference);

                items.add(nextMedication);
            }

            if (latestMedication != null) {
                String header = getString(R.string.latest_medicication);
                String time = latestMedication.getTime();
                String medicine = latestMedication.getMedicine();
                boolean next = false;

                String time_difference = TimeManager.getTimeDifferencePast(latestMedication.getDate(), latestMedication.getTime(),getActivity());

                OverviewMedicationItem latest = new OverviewMedicationItem(header, time, medicine, next);
                latest.setTime_ago(time_difference);

                items.add(latest);

            }

            overviewMedicationAdapter = new OverviewMedicationAdapter(getChildFragmentManager(), getActivity(), items, overviewMedicationListener);
            medicationPager.setAdapter(overviewMedicationAdapter);

        }


    }

    private void initListeners() {
        fabNutriton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historyListener.addNutrition();
            }
        });

        fabMedication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historyListener.addMedication();
            }
        });

        fabExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historyListener.addExercise();
            }
        });


        ivNutrition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivNutrition.startAnimation(in);
                historyListener.addNutrition();
            }
        });

        ivMedication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivMedication.startAnimation(in);
                historyListener.addMedication();
            }
        });

        ivExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivExercise.startAnimation(in);
                historyListener.addExercise();
            }
        });

    }


    public void setNavigationListener(NavigationListener navigationListener) {
        this.navigationListener = navigationListener;
    }

    public void setHistoryListener(HistoryListener historyListener) {
        this.historyListener = historyListener;
    }

    private OverviewExerciseListener overviewExerciseListener = new OverviewExerciseListener() {
        @Override
        public void moveToNext() {
            if (exercisePager != null) {
                int current_pos = exercisePager.getCurrentItem();
                int next_pos = (current_pos + 1) % exercisePager.getAdapter().getCount();
                exercisePager.setCurrentItem(next_pos);
            }
        }
    };

    private OverviewMedicationListener overviewMedicationListener = new OverviewMedicationListener() {
        @Override
        public void moveToNext() {
            if (medicationPager != null) {
                int current_pos = medicationPager.getCurrentItem();
                int next_pos = (current_pos + 1) % medicationPager.getAdapter().getCount();
                medicationPager.setCurrentItem(next_pos);
            }
        }
    };


    @Override
    public void onDetach() {
        super.onDetach();

        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e) {
                Log.e("EEEE", "Error setting mChildFragmentManager field", e);
            }
        }
    }


    private View.OnClickListener generalShowcaseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            svHealthzin.hide();

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);

            Target viewTarget = new ViewTarget(R.id.ivNutrition, getActivity());
            svOverview = new ShowcaseView.Builder(getActivity())
                    .withMaterialShowcase()
                    .blockAllTouches()
                    .setTarget(viewTarget)
                    .setContentTitle(getString(R.string.nutrition))
                    .setContentText(getString(R.string.instructions_overview_nutrition))
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setOnClickListener(showCaseListener)
                    .build();

            svOverview.setButtonText(getString(R.string.instructions_got_it));
            svOverview.setButtonPosition(lps);
            showcase++;
        }
    };


    private View.OnClickListener showCaseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (showcase) {
                case 1:
                    svOverview.setContentTitle("");
                    svOverview.setContentText("");
                    svOverview.setShowcase(new ViewTarget(getToolbarNavigationButton()), true);
                    svOverview.setContentTitle(getString(R.string.menu));
                    svOverview.setContentText(getString(R.string.instructions_overview_menu));
                    svOverview.setButtonText(getString(R.string.instructions_got_it));

                    break;
                case 2:
                    svOverview.hide();
                    sharedPreferences.edit().putBoolean("overviewShowcase", true).apply();
                    break;
            }
            showcase++;
        }
    };


    private ImageButton getToolbarNavigationButton() {
        int size = toolbar.getChildCount();
        for (int i = 0; i < size; i++) {
            View child = toolbar.getChildAt(i);
            if (child instanceof ImageButton) {
                ImageButton btn = (ImageButton) child;
                if (btn.getDrawable() == toolbar.getNavigationIcon()) {
                    return btn;
                }
            }
        }
        return null;
    }

}
