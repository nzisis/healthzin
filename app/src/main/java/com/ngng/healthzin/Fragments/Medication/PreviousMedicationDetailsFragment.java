package com.ngng.healthzin.Fragments.Medication;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ngng.healthzin.Model.Realm.Medication;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;

import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by nzisis on 1/11/17.
 */
public class PreviousMedicationDetailsFragment extends Fragment {

    private RelativeLayout rlPreviousMedication;
    private LinearLayout llNoPreviousMedication;
    private TextView tvPreviousHeader, tvPreviousDate, tvPreviousTime, tvPreviousMedication;
    private TextView tvNoPreviousHeader, tvPreviousRecord;

    private RealmDatabase db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_previous_medication, container, false);

        rlPreviousMedication = (RelativeLayout) view.findViewById(R.id.rlPreviousMedication);
        llNoPreviousMedication = (LinearLayout) view.findViewById(R.id.llNoPreviousMedication);

        tvPreviousHeader = (TextView) view.findViewById(R.id.tvPreviousMedicationHeader);
        tvPreviousDate = (TextView) view.findViewById(R.id.tvDate);
        tvPreviousTime = (TextView) view.findViewById(R.id.tvPreviousTime);
        tvPreviousMedication = (TextView) view.findViewById(R.id.tvPreviousMedication);


        tvNoPreviousHeader = (TextView) view.findViewById(R.id.tvNoMedicationHeader);
        tvPreviousRecord = (TextView) view.findViewById(R.id.tvMedicationRecord);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvNoPreviousHeader.setTypeface(typefaceCR);
        tvPreviousRecord.setTypeface(typefaceCR);
        tvPreviousDate.setTypeface(typefaceCR);
        tvPreviousHeader.setTypeface(typefaceCR);
        tvPreviousTime.setTypeface(typefaceCR);
        tvPreviousMedication.setTypeface(typefaceCR);


        db = new RealmDatabase(getActivity());

        RealmResults<Medication> todayMedication = db.getMedicationByDate(TimeManager.getCurrentDate());
        if (!todayMedication.isEmpty()) {
            Medication lastMedication;
            if (todayMedication.size() > 1) {
                String[] fields = new String[1];
                Sort[] sorted = new Sort[1];
                fields[0] = "time";

                sorted[0] = Sort.DESCENDING;

                RealmResults<Medication> sortedItems = todayMedication.sort(fields, sorted);
                lastMedication = sortedItems.get(0);
            } else {
                lastMedication = todayMedication.get(0);
            }

            llNoPreviousMedication.setVisibility(View.GONE);
            rlPreviousMedication.setVisibility(View.VISIBLE);

            tvPreviousDate.setText(getString(R.string.today));
            tvPreviousTime.setText(lastMedication.getTime());
            tvPreviousMedication.setText(lastMedication.getMedicine());


        } else {
            llNoPreviousMedication.setVisibility(View.VISIBLE);
            rlPreviousMedication.setVisibility(View.GONE);

        }


    }
}
