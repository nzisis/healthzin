package com.ngng.healthzin.Fragments.Exercise;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ngng.healthzin.R;
import java.util.HashMap;


/**
 * Created by nzisis on 1/5/17.
 */
public class ExerciseDetailsFragment extends Fragment {

    private FragmentManager manager;
    private HashMap<String,Fragment> components;

    private String tag;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercise_details,container,false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {

        manager = getChildFragmentManager();
        components = new HashMap<>();

        tag = "PreviousExerciseFragment";
        PreviousExerciseDetailsFragment previousExerciseFragment = new PreviousExerciseDetailsFragment();
        components.put(tag, previousExerciseFragment);

        manager.beginTransaction().replace(R.id.flPreviousExercise, components.get(tag)).commit();

        tag = "NextExerciseFragment";
        NextExerciseDetailsFragment nextExerciseDetailsFragment = new NextExerciseDetailsFragment();
        components.put(tag, nextExerciseDetailsFragment);

        manager.beginTransaction().replace(R.id.flNextExercise, components.get(tag)).commit();

    }


}
