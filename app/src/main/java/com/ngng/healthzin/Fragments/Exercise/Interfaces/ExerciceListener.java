package com.ngng.healthzin.Fragments.Exercise.Interfaces;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

/**
 * Created by nzisis on 11/25/16.
 */
public interface ExerciceListener {
    void showWeeklyExercise();
    void addedExercise();
    void showTimeDialog(RadialTimePickerDialogFragment fragment);
    void showDateDialog(CalendarDatePickerDialogFragment fragment);
}
