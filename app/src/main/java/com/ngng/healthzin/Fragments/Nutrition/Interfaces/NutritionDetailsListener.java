package com.ngng.healthzin.Fragments.Nutrition.Interfaces;

/**
 * Created by nzisis on 11/24/16.
 */
public interface NutritionDetailsListener {
    void showCurrentMeals();
}
