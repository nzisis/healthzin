package com.ngng.healthzin.Fragments.Nutrition;


import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ngng.healthzin.Adapters.History.ScreenSlidePageAdapters.NutritionScreenSlidePagerAdapter;
import com.ngng.healthzin.Model.Realm.Nutrition;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;

import java.util.ArrayList;

import io.realm.RealmResults;

/**
 * Created by nzisis on 11/11/16.
 */
public class NutritionDetailsFragment extends Fragment {

    private LinearLayout llNoMeals;
    private TextView tvNoMealHeader, tvRecord;
    private RelativeLayout rlContent;
    private TextView tvDate;
    private ViewPager mPager;
    private ImageView ivRightArrow, ivLeftArrow;

    private RealmDatabase db;
    private int currentPos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nutrition_details, container, false);

        llNoMeals = (LinearLayout) view.findViewById(R.id.llNoNutrition);
        rlContent = (RelativeLayout) view.findViewById(R.id.rlContent);

        tvNoMealHeader = (TextView) view.findViewById(R.id.tvNoMealHeader);
        tvRecord = (TextView) view.findViewById(R.id.tvRecord);

        tvDate = (TextView) view.findViewById(R.id.tvDate);
        mPager = (ViewPager) view.findViewById(R.id.pager);
        ivRightArrow = (ImageView) view.findViewById(R.id.ivRightArrow);
        ivLeftArrow = (ImageView) view.findViewById(R.id.ivLeftArrow);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();

    }


    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");

        tvNoMealHeader.setTypeface(typefaceCR);
        tvRecord.setTypeface(typefaceCR);
        tvDate.setTypeface(typefaceCR);


        db = new RealmDatabase(getActivity());

        RealmResults<Nutrition> results = db.getNutritionByDate(TimeManager.getCurrentDate());
        if (!results.isEmpty()) {
            ArrayList<Nutrition> items = new ArrayList<>();
            for (int i = 0; i < results.size(); i++) {
                items.add(results.get(i));
            }

            tvDate.setText(getString(R.string.today));

            NutritionScreenSlidePagerAdapter screenSlidePagerAdapter = new NutritionScreenSlidePagerAdapter(getActivity(), items);
            mPager.setAdapter(screenSlidePagerAdapter);

            if (screenSlidePagerAdapter.getCount() == 1) {
                ivLeftArrow.setVisibility(View.GONE);
                ivRightArrow.setVisibility(View.GONE);
            } else {
                ivRightArrow.setVisibility(View.VISIBLE);

                currentPos = mPager.getCurrentItem();
                if (currentPos == 0) ivLeftArrow.setVisibility(View.GONE);
                else ivLeftArrow.setVisibility(View.VISIBLE);
            }

            llNoMeals.setVisibility(View.GONE);
            rlContent.setVisibility(View.VISIBLE);

        } else {

            llNoMeals.setVisibility(View.VISIBLE);
            rlContent.setVisibility(View.GONE);
        }


    }

    private void initListeners() {

        ivRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPos = mPager.getCurrentItem();

                int nextPos = (currentPos + 1) % mPager.getAdapter().getCount();
                if (nextPos == 0) ivLeftArrow.setVisibility(View.GONE);
                else ivLeftArrow.setVisibility(View.VISIBLE);

                mPager.setCurrentItem(nextPos);
            }
        });


        ivLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPos = mPager.getCurrentItem();

                int nextPos = currentPos - 1;
                if (nextPos >= 0) {
                    if (nextPos == 0) ivLeftArrow.setVisibility(View.GONE);
                    else ivLeftArrow.setVisibility(View.VISIBLE);

                    mPager.setCurrentItem(nextPos);
                }

            }
        });
    }
}
