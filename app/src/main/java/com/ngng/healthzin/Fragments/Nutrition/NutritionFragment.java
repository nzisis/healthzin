package com.ngng.healthzin.Fragments.Nutrition;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.makeramen.roundedimageview.RoundedImageView;
import com.michael.easydialog.EasyDialog;
import com.ngng.healthzin.Activities.OverviewActivity;
import com.ngng.healthzin.Adapters.AutoCompleteTextview.AutoCompleteTextViewAdapter;
import com.ngng.healthzin.Fragments.Menu.Interfaces.NavigationListener;
import com.ngng.healthzin.Fragments.Nutrition.Interfaces.NutritionDetailsListener;
import com.ngng.healthzin.Fragments.Nutrition.Interfaces.NutritionListener;
import com.ngng.healthzin.Model.Realm.AutoCompleteNutrition;
import com.ngng.healthzin.Model.Realm.Nutrition;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;


import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Stack;

import me.grantland.widget.AutofitTextView;

/**
 * Created by nzisis on 11/10/16.
 */
public class NutritionFragment extends Fragment {

    private Toolbar toolbar;
    private TextView tvFood, tvQuantity, tvHeader;
    private AutofitTextView tvDate, tvTime;
    private AutoCompleteTextView actvFood;
    private EditText etQuantity;
    private RoundedImageView ivNutrition;
    private ImageButton ibDate, ibTime;
    private ImageView ivTip;
    private CardView cvQuantity, cvFood;
    private RelativeLayout rlFood, rlQuantity;


    private CalendarDatePickerDialogFragment cdate;
    private RadialTimePickerDialogFragment rtime;

    private String date, time;

    private FragmentManager manager;
    private HashMap<String, Fragment> components;
    private Stack<String> fragmentsStack;

    private String tag;
    private boolean show;

    private NutritionListener nutritionListener;
    private NavigationListener navigationListener;
    private LayoutInflater inflater;

    private AutoCompleteTextViewAdapter autoCompleteTextViewAdapter;

    private View doneView;
    private ShowcaseView svNutrition;
    private int showcase;
    private SharedPreferences sharedPreferences;

    private InputMethodManager inputMethodManagerFood, inputMethodManagerQuantity;

    private static final Field sChildFragmentManagerField;

    static {
        Field f = null;
        try {
            f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.e("EEEE", "Error getting mChildFragmentManager field", e);
        }
        sChildFragmentManagerField = f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.inflater = inflater;

        View view = inflater.inflate(R.layout.fragment_nutrition, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.menu_drawer);

        ((OverviewActivity) getActivity()).setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();
                navigationListener.onNavigationClicked();
            }
        });

        toolbar.setTitle(getString(R.string.nutrition));


        tvFood = (TextView) view.findViewById(R.id.tvFood);
        tvQuantity = (TextView) view.findViewById(R.id.tvQuantity);
        tvDate = (AutofitTextView) view.findViewById(R.id.tvDate);
        tvTime = (AutofitTextView) view.findViewById(R.id.tvTime);
        tvHeader = (TextView) view.findViewById(R.id.tvHeader);


        actvFood = (AutoCompleteTextView) view.findViewById(R.id.etFood);
        etQuantity = (EditText) view.findViewById(R.id.etQuantity);

        ivNutrition = (RoundedImageView) view.findViewById(R.id.rivNutrition);
        ivTip = (ImageView) view.findViewById(R.id.ivTip);


        ibDate = (ImageButton) view.findViewById(R.id.ibDate);
        ibTime = (ImageButton) view.findViewById(R.id.ibTime);

        rlFood = (RelativeLayout) view.findViewById(R.id.rlFood);
        rlQuantity = (RelativeLayout) view.findViewById(R.id.rlQuantity);


        cvQuantity = (CardView) view.findViewById(R.id.cvQuantity);
        cvFood = (CardView) view.findViewById(R.id.cvFood);

        ivTip.setVisibility(View.GONE);
        actvFood.setVisibility(View.GONE);
        etQuantity.setVisibility(View.GONE);


        setHasOptionsMenu(true);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();
    }


    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_nutrition, menu);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                MenuItem done = menu.findItem(R.id.action_Done);

                doneView = getActivity().findViewById(done.getItemId());
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_Done:
                Log.e("Done", "Pressed");
                addNutrition();
                break;

        }

        return false;
    }

    private void addNutrition() {

        if (actvFood.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getString(R.string.enter_food), Toast.LENGTH_SHORT).show();
            return;
        }

        if (etQuantity.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getString(R.string.enter_quantity), Toast.LENGTH_SHORT).show();
            return;
        }


        RealmDatabase db = new RealmDatabase(getActivity());
        Nutrition nutrition = new Nutrition(actvFood.getText().toString(), Double.valueOf(etQuantity.getText().toString()), time, date, db.getCurrentMealNumber(date));
        nutrition.setNutritionID(db.getNutritionNextKey());

        AutoCompleteNutrition autoCompleteNutrition = new AutoCompleteNutrition(actvFood.getText().toString().toLowerCase());

        db.insertNutrition(nutrition);
        db.insertAutoCompleteNutrition(autoCompleteNutrition);
        db.getAllNutritionFromNewestToOldest();

        closeKeyboard();
        closeInnerFragment();

        nutritionListener.addedNutrition();

    }


    private void initListeners() {

        rlFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tvFood.setVisibility(View.GONE);
                actvFood.setVisibility(View.VISIBLE);
                actvFood.requestFocus();
                inputMethodManagerFood.showSoftInput(actvFood, InputMethodManager.SHOW_IMPLICIT);


            }
        });

        rlQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvQuantity.setVisibility(View.GONE);
                etQuantity.setVisibility(View.VISIBLE);
                ivTip.setVisibility(View.VISIBLE);
                etQuantity.requestFocus();
                inputMethodManagerQuantity.showSoftInput(etQuantity, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        ibDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nutritionListener.showDateDialog(cdate);
            }
        });


        ibTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nutritionListener.showTimeDialog(rtime);
            }
        });

        ivNutrition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nutritionDetailsListener.showCurrentMeals();
            }
        });

        ivTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                View view = inflater.inflate(R.layout.tip_content, null);
                TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                tvTip.setText(getString(R.string.nutrition_tip));

                new EasyDialog(getActivity())
                        .setLayout(view)
                        .setBackgroundColor(getResources().getColor(R.color.app_blue))
                        .setLocationByAttachedView(cvQuantity)
                        .setGravity(EasyDialog.GRAVITY_TOP)
                        .setAnimationTranslationShow(EasyDialog.DIRECTION_X, 1000, -600, 100, -50, 50, 0)
                        .setAnimationAlphaShow(1000, 0.3f, 1.0f)
                        .setAnimationTranslationShow(EasyDialog.DIRECTION_Y, 1000, -800, 100, -50, 50, 0)
                        .setAnimationTranslationDismiss(EasyDialog.DIRECTION_Y, 500, 0, -800)
                        .setTouchOutsideDismiss(true)
                        .setMatchParent(false)
                        .setMarginLeftAndRight(24, 24)
                        .show();


            }
        });

    }

    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");

        tvFood.setTypeface(typefaceCR);
        tvQuantity.setTypeface(typefaceCR);
        tvDate.setTypeface(typefaceCR);
        tvTime.setTypeface(typefaceCR);
        tvHeader.setTypeface(typefaceCR);
        actvFood.setTypeface(typefaceCR);
        etQuantity.setTypeface(typefaceCR);

        inputMethodManagerFood = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManagerQuantity = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        autoCompleteTextViewAdapter = new AutoCompleteTextViewAdapter(getActivity(), 1);
        actvFood.setAdapter(autoCompleteTextViewAdapter);

        showcase = 0;

        if (!sharedPreferences.getBoolean("nutritionShowcase", false)) {

            ((OverviewActivity) getActivity()).setDisable_back_button(true);


            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);


            Target viewTarget = new ViewTarget(R.id.rivNutrition, getActivity());
            svNutrition = new ShowcaseView.Builder(getActivity())
                    .withMaterialShowcase()
                    .blockAllTouches()
                    .setTarget(viewTarget)
                    .setContentTitle(getString(R.string.nutrition) + " (1/3)")
                    .setContentText(getString(R.string.instructions_nutrition_1))
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setOnClickListener(showCaseListener)
                    .build();

            svNutrition.setButtonText("Ok got it!!");
            svNutrition.setButtonPosition(lps);
        }


        manager = getChildFragmentManager();
        fragmentsStack = new Stack<>();
        components = new HashMap<>();


        show = false;

        tag = "NutritionDetailsFragment";
        NutritionDetailsFragment fragment = new NutritionDetailsFragment();
        components.put(tag, fragment);


        Calendar calendar = Calendar.getInstance();

        cdate = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(dateSetListener)
                .setDoneText("OK")
                .setCancelText(getString(R.string.cancel))
                .setPreselectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));


        rtime = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(timeSetListener)
                .setDoneText("OK")
                .setCancelText(getString(R.string.cancel))
                .setStartTime(calendar.getTime().getHours(), calendar.getTime().getMinutes());


        time = TimeManager.getCurrentTime();
        date = TimeManager.getCurrentDate();


        tvDate.setText(getString(R.string.today));
        tvTime.setText(time);
    }


    public void setNutritionListener(NutritionListener nutritionListener) {
        this.nutritionListener = nutritionListener;
    }

    public void setNavigationListener(NavigationListener navigationListener) {
        this.navigationListener = navigationListener;
    }


    private CalendarDatePickerDialogFragment.OnDateSetListener dateSetListener = new CalendarDatePickerDialogFragment.OnDateSetListener() {
        @Override
        public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
            String month, day;
            monthOfYear++;
            if (monthOfYear < 10)
                month = "0" + monthOfYear;
            else
                month = String.valueOf(monthOfYear);
            if (dayOfMonth < 10)
                day = "0" + dayOfMonth;
            else
                day = String.valueOf(dayOfMonth);
            date = year + "-" + month + "-" + day;


            if (TimeManager.isToday(date)) {
                tvDate.setText(getString(R.string.today));
            } else if (TimeManager.isYesterday(date)) {
                tvDate.setText(getString(R.string.yesterday));
            } else {
                tvDate.setText(date);
            }


        }
    };

    private RadialTimePickerDialogFragment.OnTimeSetListener timeSetListener = new RadialTimePickerDialogFragment.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
            String hours, mins;
            if (hourOfDay < 10)
                hours = "0" + hourOfDay;
            else
                hours = String.valueOf(hourOfDay);
            if (minute < 10)
                mins = "0" + minute;
            else
                mins = String.valueOf(minute);
            time = hours + ":" + mins;

            tvTime.setText(time);

        }
    };

    private NutritionDetailsListener nutritionDetailsListener = new NutritionDetailsListener() {
        @Override
        public void showCurrentMeals() {
            if (!show) {
                fragmentsStack.push(tag);

                Fragment fragment = components.get(tag);
                FragmentTransaction fragmentTransaction = manager.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), fragmentTransaction, null, fragment, R.id.flNutrition);
                fragmentTransactionExtended.addTransition(FragmentTransactionExtended.SLIDE_HORIZONTAL);
                fragmentTransactionExtended.commit();


                show = true;
            } else {
                Fragment fragment = components.get(tag);
                fragmentsStack.pop();
                manager.beginTransaction().remove(fragment).commit();

                show = false;

            }
        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        closeInnerFragment();
    }

    @Override
    public void onDetach() {
        super.onDetach();


        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e) {
                Log.e("EEEE", "Error setting mChildFragmentManager field", e);
            }
        }
    }


    private View.OnClickListener showCaseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (showcase) {
                case 0:
                    svNutrition.setContentTitle("");
                    svNutrition.setContentText("");
                    svNutrition.setShowcase(new ViewTarget(cvFood), true);
                    svNutrition.setContentTitle(getString(R.string.nutrition) + " (2/3)");
                    svNutrition.setContentText(getString(R.string.instructions_nutrition_2));
                    svNutrition.setButtonText(getString(R.string.instructions_got_it));
                    break;
                case 1:
                    svNutrition.setContentTitle("");
                    svNutrition.setContentText("");
                    svNutrition.setShowcase(new ViewTarget(doneView), true);
                    svNutrition.setContentTitle(getString(R.string.nutrition) + " (3/3)");
                    svNutrition.setContentText(getString(R.string.instructions_nutrition_3));
                    svNutrition.setButtonText(getString(R.string.instructions_got_it));
                    break;
                case 2:
                    svNutrition.hide();
                    ((OverviewActivity) getActivity()).setDisable_back_button(false);
                    sharedPreferences.edit().putBoolean("nutritionShowcase", true).apply();
                    break;
            }
            showcase++;
        }
    };

    public void closeKeyboard() {
        if (inputMethodManagerFood.isActive())
            inputMethodManagerFood.hideSoftInputFromWindow(actvFood.getWindowToken(), 0);
        else if (inputMethodManagerQuantity.isActive())
            inputMethodManagerQuantity.hideSoftInputFromWindow(etQuantity.getWindowToken(), 0);
    }

    private void closeInnerFragment() {
        if (show) {
            Fragment fragment = components.get(tag);
            fragmentsStack.pop();
            manager.beginTransaction().remove(fragment).commit();

            show = false;
        }
    }


}
