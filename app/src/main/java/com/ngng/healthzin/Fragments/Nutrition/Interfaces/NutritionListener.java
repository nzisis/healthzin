package com.ngng.healthzin.Fragments.Nutrition.Interfaces;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

/**
 * Created by vromia on 12/28/16.
 */
public interface NutritionListener {
    void addedNutrition();
    void showTimeDialog(RadialTimePickerDialogFragment fragment);
    void showDateDialog(CalendarDatePickerDialogFragment fragment);
}
