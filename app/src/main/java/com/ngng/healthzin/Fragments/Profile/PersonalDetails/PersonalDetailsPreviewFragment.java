package com.ngng.healthzin.Fragments.Profile.PersonalDetails;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ngng.healthzin.Model.User.UserInstance;
import com.ngng.healthzin.R;

/**
 * Created by nzisis on 17/01/17.
 */
public class PersonalDetailsPreviewFragment extends Fragment {

    private TextView tvSexHeader,tvAgeHeader,tvHeightHeader,tvWeightHeader;
    private TextView tvSex,tvAge,tvHeight,tvWeight;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal_details_preview,container,false);

        tvSexHeader = (TextView) view.findViewById(R.id.tvSexHeader);
        tvAgeHeader = (TextView) view.findViewById(R.id.tvAgeHeader);
        tvWeightHeader = (TextView) view.findViewById(R.id.tvWeightHeader);
        tvHeightHeader = (TextView) view.findViewById(R.id.tvHeightHeader);

        tvSex = (TextView) view.findViewById(R.id.tvSex);
        tvAge = (TextView) view.findViewById(R.id.tvAge);
        tvHeight = (TextView) view.findViewById(R.id.tvHeight);
        tvWeight = (TextView) view.findViewById(R.id.tvWeight);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvSex.setTypeface(typefaceCR);
        tvAge.setTypeface(typefaceCR);
        tvWeight.setTypeface(typefaceCR);
        tvHeight.setTypeface(typefaceCR);
        tvAgeHeader.setTypeface(typefaceCR);
        tvWeightHeader.setTypeface(typefaceCR);
        tvHeightHeader.setTypeface(typefaceCR);
        tvSexHeader.setTypeface(typefaceCR);


        if(UserInstance.getInstance().getSex().equals("")) {
            tvSexHeader.setVisibility(View.GONE);
            tvSex.setVisibility(View.GONE);
        }else{
            tvSexHeader.setVisibility(View.VISIBLE);
            tvSex.setVisibility(View.VISIBLE);
            tvSex.setText(UserInstance.getInstance().getSex());
        }

        if(UserInstance.getInstance().getAge() == -1){
            tvAgeHeader.setVisibility(View.GONE);
            tvAge.setVisibility(View.GONE);
        }else{
            tvAgeHeader.setVisibility(View.VISIBLE);
            tvAge.setVisibility(View.VISIBLE);
            tvAge.setText(UserInstance.getInstance().getAge()+"");
        }


        if(UserInstance.getInstance().getHeight() == -1){
            tvHeightHeader.setVisibility(View.GONE);
            tvHeight.setVisibility(View.GONE);
        }else{
            tvHeightHeader.setVisibility(View.VISIBLE);
            tvHeight.setVisibility(View.VISIBLE);
            tvHeight.setText(UserInstance.getInstance().getHeight()+"");
        }

        if(UserInstance.getInstance().getWeight() == -1){
            tvWeightHeader.setVisibility(View.GONE);
            tvWeight.setVisibility(View.GONE);
        }else{
            tvWeightHeader.setVisibility(View.VISIBLE);
            tvWeight.setVisibility(View.VISIBLE);
            tvWeight.setText(UserInstance.getInstance().getWeight()+"");
        }


    }


    public void updateView(){
        if(UserInstance.getInstance().getHeight() == -1){
            tvHeightHeader.setVisibility(View.GONE);
            tvHeight.setVisibility(View.GONE);
        }else{
            tvHeightHeader.setVisibility(View.VISIBLE);
            tvHeight.setVisibility(View.VISIBLE);
            tvHeight.setText(UserInstance.getInstance().getHeight()+"");
        }

        if(UserInstance.getInstance().getWeight() == -1){
            tvWeightHeader.setVisibility(View.GONE);
            tvWeight.setVisibility(View.GONE);
        }else{
            tvWeightHeader.setVisibility(View.VISIBLE);
            tvWeight.setVisibility(View.VISIBLE);
            tvWeight.setText(UserInstance.getInstance().getWeight()+"");
        }
    }

}
