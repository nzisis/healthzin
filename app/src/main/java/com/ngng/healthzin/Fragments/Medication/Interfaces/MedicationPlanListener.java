package com.ngng.healthzin.Fragments.Medication.Interfaces;

import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

/**
 * Created by nzisis on 29/01/17.
 */
public interface MedicationPlanListener {
    void medicationPlanAdded();
    void showTimeDialog(RadialTimePickerDialogFragment fragment);

}
