package com.ngng.healthzin.Fragments.Medication.Interfaces;

/**
 * Created by nzisis on 22/01/17.
 */
public interface MedicationDetailsListener {
    void showDetails();
}
