package com.ngng.healthzin.Fragments.Medication;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ngng.healthzin.Model.Realm.Medication;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;

/**
 * Created by nzisis on 1/11/17.
 */
public class NextMedicationDetailsFragment extends Fragment {
    private RelativeLayout rlNextMedication;
    private LinearLayout llNoNextMedication;
    private TextView tvNextHeader, tvNextDate, tvNextTime, tvNextMedication;
    private TextView tvNoNextHeader, tvNextRecord;


    private RealmDatabase db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_next_medication, container, false);

        rlNextMedication = (RelativeLayout) view.findViewById(R.id.rlNextMedication);
        llNoNextMedication = (LinearLayout) view.findViewById(R.id.llNoNextMedication);


        tvNextHeader = (TextView) view.findViewById(R.id.tvNextMedicationHeader);
        tvNextDate = (TextView) view.findViewById(R.id.tvNextDate);
        tvNextTime = (TextView) view.findViewById(R.id.tvNextTime);
        tvNextMedication = (TextView) view.findViewById(R.id.tvNextMedication);


        tvNoNextHeader = (TextView) view.findViewById(R.id.tvNoMedicationPlanHeader);
        tvNextRecord = (TextView) view.findViewById(R.id.tvMedicationPlanRecord);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvNoNextHeader.setTypeface(typefaceCR);
        tvNextRecord.setTypeface(typefaceCR);
        tvNextDate.setTypeface(typefaceCR);
        tvNextTime.setTypeface(typefaceCR);
        tvNextMedication.setTypeface(typefaceCR);
        tvNextHeader.setTypeface(typefaceCR);


        db = new RealmDatabase(getActivity());

        Medication medication = db.getNextMedicationPlanEvent();
        if (medication != null) {
            rlNextMedication.setVisibility(View.VISIBLE);
            llNoNextMedication.setVisibility(View.GONE);

            if (medication.getDate().equals(TimeManager.getCurrentDate())) {
                tvNextDate.setText(getString(R.string.today));
            } else {
                String formatedDate = TimeManager.getMonthAndDayFromDate(medication.getDate());
                tvNextDate.setText(formatedDate);
            }


            tvNextTime.setText(medication.getTime());
            tvNextMedication.setText(medication.getMedicine());
        } else {
            rlNextMedication.setVisibility(View.GONE);
            llNoNextMedication.setVisibility(View.VISIBLE);
        }


    }
}
