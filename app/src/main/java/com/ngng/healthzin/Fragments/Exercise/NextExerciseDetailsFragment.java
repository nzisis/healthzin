package com.ngng.healthzin.Fragments.Exercise;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.ngng.healthzin.Model.Realm.ExercisePlanEvent;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;

import java.util.ArrayList;

import me.grantland.widget.AutofitTextView;

/**
 * Created by vromia on 1/5/17.
 */
public class NextExerciseDetailsFragment extends Fragment {
    private RelativeLayout rlNextExercise;
    private LinearLayout llNoNextExercise;
    private TextView tvNextHeader,tvNextDate,tvNextTime,tvNextExercise;
    private AutofitTextView tvNoNextHeader,tvNextRecord;


    private RealmDatabase db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_next_exercise,container,false);

        rlNextExercise = (RelativeLayout) view.findViewById(R.id.rlNextExercise);
        llNoNextExercise = (LinearLayout) view.findViewById(R.id.llNoNextExercise);


        tvNextHeader = (TextView) view.findViewById(R.id.tvNextExerciseHeader);
        tvNextDate = (TextView) view.findViewById(R.id.tvNextDate);
        tvNextTime = (TextView) view.findViewById(R.id.tvNextTime);
        tvNextExercise = (TextView) view.findViewById(R.id.tvNextExercise);


        tvNoNextHeader = (AutofitTextView) view.findViewById(R.id.tvNoExercisePlanHeader);
        tvNextRecord = (AutofitTextView) view.findViewById(R.id.tvExercisePlanRecord);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init(){
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvNoNextHeader.setTypeface(typefaceCR);
        tvNextRecord.setTypeface(typefaceCR);
        tvNextDate.setTypeface(typefaceCR);
        tvNextTime.setTypeface(typefaceCR);
        tvNextExercise.setTypeface(typefaceCR);
        tvNextHeader.setTypeface(typefaceCR);


        db = new RealmDatabase(getActivity());

        ArrayList<String> exercisePlanEvents = db.getAllExercisePlanEvents();
        if(exercisePlanEvents.isEmpty()){
            rlNextExercise.setVisibility(View.GONE);
            llNoNextExercise.setVisibility(View.VISIBLE);

            tvNoNextHeader.setText(getString(R.string.no_exercise_plan));
        }else{

          ExercisePlanEvent exercisePlanEvent = db.getNextExercisePlanEvent(TimeManager.getCurrentDate(),TimeManager.getCurrentTime());
            if(exercisePlanEvent!=null){
                rlNextExercise.setVisibility(View.VISIBLE);
                llNoNextExercise.setVisibility(View.GONE);

                String formatedDate = TimeManager.getMonthAndDayFromDate(exercisePlanEvent.getDate());

                tvNextDate.setText(formatedDate);
                tvNextTime.setText(exercisePlanEvent.getTime());
                tvNextExercise.setText(exercisePlanEvent.getExercise());

            }else{
                rlNextExercise.setVisibility(View.GONE);
                llNoNextExercise.setVisibility(View.VISIBLE);

                tvNoNextHeader.setText(getString(R.string.no_exercise_plan_after_today));

            }

        }


    }
}
