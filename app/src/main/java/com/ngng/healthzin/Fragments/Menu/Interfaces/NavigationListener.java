package com.ngng.healthzin.Fragments.Menu.Interfaces;

/**
 * Created by vromia on 12/28/16.
 */
public interface NavigationListener {
    void onNavigationClicked();
}
