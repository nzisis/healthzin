package com.ngng.healthzin.Fragments.Exercise;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ngng.healthzin.Model.Realm.Exercise;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;

import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by nzisis on 1/5/17.
 */
public class PreviousExerciseDetailsFragment extends Fragment {

    private RelativeLayout rlPreviousExercise;
    private LinearLayout llNoPreviousExercise;
    private TextView tvPreviousHeader, tvPreviousDate, tvPreviousTime, tvPreviousExercise;
    private TextView tvNoPreviousHeader, tvPreviousRecord;

    private RealmDatabase db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_previous_exercise, container, false);

        rlPreviousExercise = (RelativeLayout) view.findViewById(R.id.rlPreviousExercise);
        llNoPreviousExercise = (LinearLayout) view.findViewById(R.id.llNoPreviousExercise);

        tvPreviousHeader = (TextView) view.findViewById(R.id.tvPreviousExerciseHeader);
        tvPreviousDate = (TextView) view.findViewById(R.id.tvDate);
        tvPreviousTime = (TextView) view.findViewById(R.id.tvPreviousTime);
        tvPreviousExercise = (TextView) view.findViewById(R.id.tvPreviousExercise);


        tvNoPreviousHeader = (TextView) view.findViewById(R.id.tvNoExerciseHeader);
        tvPreviousRecord = (TextView) view.findViewById(R.id.tvExerciseRecord);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvNoPreviousHeader.setTypeface(typefaceCR);
        tvPreviousRecord.setTypeface(typefaceCR);
        tvPreviousHeader.setTypeface(typefaceCR);
        tvPreviousDate.setTypeface(typefaceCR);
        tvPreviousTime.setTypeface(typefaceCR);
        tvPreviousExercise.setTypeface(typefaceCR);


        db = new RealmDatabase(getActivity());

        RealmResults<Exercise> todayExercise = db.getExerciseByDate(TimeManager.getCurrentDate());
        if (!todayExercise.isEmpty()) {
            Exercise lastExercise;
            if (todayExercise.size() > 1) {
                String[] fields = new String[1];
                Sort[] sorted = new Sort[1];
                fields[0] = "time";

                sorted[0] = Sort.DESCENDING;

                RealmResults<Exercise> sortedItems = todayExercise.sort(fields, sorted);
                lastExercise = sortedItems.get(0);
            } else {
                lastExercise = todayExercise.get(0);
            }

            llNoPreviousExercise.setVisibility(View.GONE);
            rlPreviousExercise.setVisibility(View.VISIBLE);

            tvPreviousDate.setText(getString(R.string.today));
            tvPreviousTime.setText(lastExercise.getTime());
            tvPreviousExercise.setText(lastExercise.getExercise());


        } else {
            llNoPreviousExercise.setVisibility(View.VISIBLE);
            rlPreviousExercise.setVisibility(View.GONE);

        }


    }
}
