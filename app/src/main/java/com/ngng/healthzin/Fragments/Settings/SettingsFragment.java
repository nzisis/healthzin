package com.ngng.healthzin.Fragments.Settings;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;


import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.AlertDialog;

import com.ngng.healthzin.Fragments.Settings.Interfaces.SettingsListener;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.ChangeLogDialog;

/**
 * Created by nzisis on 30/01/17.
 */
public class SettingsFragment extends PreferenceFragment {

    private SettingsListener settingsListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        Preference medicationPlan = findPreference("pref_key_medication_plan");
        medicationPlan.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                settingsListener.showMedicationPlanHistory();
                return false;
            }
        });

        Preference exercisePlan = findPreference("pref_key_exercise_plan");
        exercisePlan.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                settingsListener.showExercisePlan();
                return false;
            }
        });

        Preference about = findPreference("pref_key_about_us");
        about.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setMessage(getString(R.string.about_us_message))
                        .setTitle(getString(R.string.about_us));
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

                return false;
            }
        });

        Preference version = findPreference("pref_key_version");
        version.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                ChangeLogDialog dialog = new ChangeLogDialog();
                dialog.show(getFragmentManager(), getString(R.string.version));

                return false;
            }
        });


        Preference rate = findPreference("pref_key_rate");
        rate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
                }
                return false;
            }
        });

        Preference instructions = findPreference("pref_key_instructions");
        instructions.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new InstructionsDialog().show(getFragmentManager(), "InstructionsDialog");
                return false;
            }
        });

    }

    public void setSettingsListener(SettingsListener settingsListener) {
        this.settingsListener = settingsListener;
    }
}
