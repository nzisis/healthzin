package com.ngng.healthzin.Fragments.History;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentPagerAdapter;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.ngng.healthzin.Activities.OverviewActivity;
import com.ngng.healthzin.Fragments.History.Interfaces.HistoryListener;
import com.ngng.healthzin.Fragments.Menu.Interfaces.NavigationListener;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.NonSwipeableViewPager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


/**
 * Created by nzisis on 12/12/16.
 */
public class HistoryFragment extends Fragment {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private NonSwipeableViewPager viewPager;
    private AppBarLayout appBarLayout;
    private Menu menu;

    private HistoryListener historyListener;
    private NavigationListener navigationListener;
    private AlertDialog dialog;


    private Fragment firstPage, secondPage, thirdPage;
    private HashMap<String, Fragment> components;

    private CalendarDatePickerDialogFragment cdate;
    private String date;

    private ViewPagerAdapter adapter;
    private CoordinatorLayout clMainContent;

    private static final Field sChildFragmentManagerField;

    static {
        Field f = null;
        try {
            f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.e("EEEE", "Error getting mChildFragmentManager field", e);
        }
        sChildFragmentManagerField = f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.menu_drawer);


        ((OverviewActivity) getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.history));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigationListener.onNavigationClicked();
            }
        });

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (NonSwipeableViewPager) view.findViewById(R.id.pager);

        appBarLayout = (AppBarLayout) view.findViewById(R.id.app_bar_layout);

        clMainContent = (CoordinatorLayout) view.findViewById(R.id.clMainContent);


        setHasOptionsMenu(true);

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_history_tab, menu);
        this.menu = menu;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        MenuItem menuItem = menu.findItem(R.id.FiltersBasic);

        int tabPosition = tabLayout.getSelectedTabPosition();
        switch (tabPosition) {
            case 0:
                menuItem.setTitle(getString(R.string.filter_food));
                break;
            case 1:
                menuItem.setTitle(getString(R.string.filter_exercise));
                break;
            case 2:
                menuItem.setTitle(getString(R.string.filter_medicine));
                break;
        }


        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        switch (id) {
            case R.id.FilterAll:
                switch (tabPosition) {
                    case 0:
                        if (components.containsKey("NutritionTab"))
                            ((HistoryNutritionTabFragment) firstPage).initNutrition();
                        break;
                    case 1:
                        if (components.containsKey("ExerciseTab"))
                            ((HistoryExerciseTabFragment) secondPage).initExercise();
                        break;
                    case 2:
                        if (components.containsKey("MedicationTab"))
                            ((HistoryMedicationTabFragment) thirdPage).initMedication();
                        break;
                }
                break;

            case R.id.FiltersBasic:
                final EditText edittext = new EditText(getActivity());
                alert.setView(edittext);

                switch (tabPosition) {
                    case 0:
                        alert.setTitle(getString(R.string.filter_food));
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                String food = edittext.getText().toString();
                                if (components.containsKey("NutritionTab"))
                                    ((HistoryNutritionTabFragment) firstPage).filterFood(food);

                                dialogInterface.dismiss();
                                dialog.dismiss();
                            }
                        });

                        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                dialogInterface.dismiss();
                                dialog.dismiss();
                            }
                        });

                        break;
                    case 1:
                        alert.setTitle(getString(R.string.filter_exercise));
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                String exercise = edittext.getText().toString();
                                if (components.containsKey("ExerciseTab"))
                                    ((HistoryExerciseTabFragment) secondPage).filterExercise(exercise);

                                dialogInterface.dismiss();
                                dialog.dismiss();
                            }
                        });

                        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                dialogInterface.dismiss();
                                dialog.dismiss();
                            }
                        });

                        break;
                    case 2:
                        alert.setTitle(getString(R.string.filter_medicine));
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                String medicine = edittext.getText().toString();
                                if (components.containsKey("MedicationTab"))
                                    ((HistoryMedicationTabFragment) thirdPage).filterMedicine(medicine);


                                dialogInterface.dismiss();
                                dialog.dismiss();
                            }
                        });

                        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                dialogInterface.dismiss();
                                dialog.dismiss();
                            }
                        });

                        break;
                }

                dialog = alert.create();
                dialog.show();
                break;

            case R.id.FiltersDate:
                historyListener.showDateDialog(cdate);
                break;


        }

        return false;
    }

    public void setHistoryListener(HistoryListener historyListener) {
        this.historyListener = historyListener;
    }

    public void setNavigationListener(NavigationListener navigationListener){
        this.navigationListener = navigationListener;
    }

    private void init() {

        Calendar calendar = Calendar.getInstance();

        cdate = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(dateSetListener)
                .setDoneText("OK")
                .setCancelText(getString(R.string.cancel))
                .setPreselectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        components = new HashMap<>();

        HistoryNutritionTabFragment historyNutritionTabFragment = new HistoryNutritionTabFragment();
        historyNutritionTabFragment.setHistoryListener(historyListener);
        components.put("NutritionTab", historyNutritionTabFragment);

        HistoryExerciseTabFragment historyExerciseTabFragment = new HistoryExerciseTabFragment();
        historyExerciseTabFragment.setHistoryListener(historyListener);
        components.put("ExerciseTab", historyExerciseTabFragment);

        HistoryMedicationTabFragment historyMedicationTabFragment = new HistoryMedicationTabFragment();
        historyMedicationTabFragment.setHistoryListener(historyListener);
        components.put("MedicationTab", historyMedicationTabFragment);

        adapter = new ViewPagerAdapter(getChildFragmentManager());


        adapter.addFragment(getString(R.string.nutrition));
        adapter.addFragment(getString(R.string.exercise));
        adapter.addFragment(getString(R.string.medication));


        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);  // clear all scroll flags

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                     firstPage = components.get("NutritionTab");
                    return firstPage;

                case 1:
                     secondPage = components.get("ExerciseTab");
                    return secondPage;

                case 2:
                     thirdPage = components.get("MedicationTab");
                    return thirdPage;


            }

            return null;
        }

        @Override
        public int getCount() {
            return mFragmentTitleList.size();
        }

        public void addFragment(String title) {
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
            //super.restoreState(state, loader);
        }
    }


    private CalendarDatePickerDialogFragment.OnDateSetListener dateSetListener = new CalendarDatePickerDialogFragment.OnDateSetListener() {
        @Override
        public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
            String month, day;
            monthOfYear++;
            if (monthOfYear < 10)
                month = "0" + monthOfYear;
            else
                month = String.valueOf(monthOfYear);
            if (dayOfMonth < 10)
                day = "0" + dayOfMonth;
            else
                day = String.valueOf(dayOfMonth);
            date = year + "-" + month + "-" + day;


            int tabPosition = tabLayout.getSelectedTabPosition();
            switch (tabPosition) {
                case 0:
                    if (components.containsKey("NutritionTab"))
                        ((HistoryNutritionTabFragment) firstPage).filterDate(date);
                    break;
                case 1:
                    if (components.containsKey("ExerciseTab"))
                        ((HistoryExerciseTabFragment) secondPage).filterDate(date);
                    break;
                case 2:
                    if (components.containsKey("MedicationTab"))
                        ((HistoryMedicationTabFragment) thirdPage).filterDate(date);
                    break;
            }


        }
    };



    @Override
    public void onDetach() {
        super.onDetach();

        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e) {
                Log.e("EEEE", "Error setting mChildFragmentManager field", e);
            }
        }
    }


}
