package com.ngng.healthzin.Fragments.History;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ngng.healthzin.Model.Realm.Nutrition;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;

import java.util.ArrayList;

/**
 * Created by nzisis on 13/12/16.
 */
public class ScreenSlideNutritionFragment extends Fragment {

    private TextView tvDate,tvMealNumber,tvTime,tvMeal;

    public static ScreenSlideNutritionFragment newInstance(Nutrition nutritionItem){

        ScreenSlideNutritionFragment myFragment = new ScreenSlideNutritionFragment();

        Bundle args = new Bundle();
        args.putString("date",nutritionItem.getDate());
        args.putString("time",nutritionItem.getTime());
        args.putString("food",nutritionItem.getFood());
        args.putInt("current", nutritionItem.getCurrent_meal());
        args.putDouble("quantity",nutritionItem.getQuantity());
        myFragment.setArguments(args);

        return myFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View itemView = inflater.inflate(R.layout.history_nutrition_item,container,false);


        tvDate = (TextView) itemView.findViewById(R.id.tvDate);
        tvMealNumber = (TextView) itemView.findViewById(R.id.tvMealNumber);
        tvTime = (TextView) itemView.findViewById(R.id.tvTime);
        tvMeal = (TextView) itemView.findViewById(R.id.tvMeal);

        return itemView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String date,time,food;
        int current;
        double quantity;

        date = getArguments().getString("date");
        time = getArguments().getString("time");
        food = getArguments().getString("food");
        current = getArguments().getInt("current");
        quantity = getArguments().getDouble("quantity");


        String formatedDate = TimeManager.getMonthAndDayFromDate(date);
        String formatedMealNumber = "";
        if(current==1){
            formatedMealNumber = "1st meal";
        }else if(current==2){
            formatedMealNumber = "2nd meal";
        }else if(current==3){
            formatedMealNumber = "3rd meal";
        }else{
            formatedMealNumber = current+"th meal";
        }

        tvDate.setText(formatedDate);
        tvMealNumber.setText(formatedMealNumber);
        tvTime.setText(time);
        tvMeal.setText(food);

    }
}
