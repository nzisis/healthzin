package com.ngng.healthzin.Fragments.Exercise.ExercisePlan;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.ngng.healthzin.Activities.OverviewActivity;
import com.ngng.healthzin.Activities.SettingsActivity;
import com.ngng.healthzin.Fragments.Exercise.Interfaces.ExerciseEventListener;
import com.ngng.healthzin.Fragments.Exercise.Interfaces.ExercisePlanListener;
import com.ngng.healthzin.Model.Realm.ExercisePlanEvent;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.realm.RealmResults;

/**
 * Created by nzisis on 11/25/16.
 */
public class ExercisePlanFragment extends Fragment {

    private CompactCalendarView compactCalendarView;
    private Toolbar toolbar;
    private TextView tvHeader,tvCalendarMonth;
    private RealmDatabase db;

    private ExercisePlanListener exercisePlanListener;

    private boolean flow;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            flow = getArguments().getBoolean("flow");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercise_plan,container,false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.navigation_back_arrow);

        if(flow) ((OverviewActivity) getActivity()).setSupportActionBar(toolbar);
        else ((SettingsActivity) getActivity()).getSupportActionBar().hide();




        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        toolbar.setTitle(getString(R.string.exercise_plan));

        compactCalendarView = (CompactCalendarView) view.findViewById(R.id.cv);
        tvHeader = (TextView) view.findViewById(R.id.tvExercisePlan);
        tvCalendarMonth = (TextView) view.findViewById(R.id.tvCalendarMonth);


        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }


    private CompactCalendarView.CompactCalendarViewListener compactCalendarViewListener = new CompactCalendarView.CompactCalendarViewListener() {
        @Override
        public void onDayClick(Date dateClicked) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = format.format(dateClicked);
            Log.d("Date", "Day was clicked: " + formattedDate);

            Date date = null;
            try {
                date = format.parse(formattedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Log.d("Date",date+"");

            RealmResults<ExercisePlanEvent> event = db.searchEvent(formattedDate);
            ExercisePlanEvent exercisePlanEvent;
            if(!event.isEmpty()) {
                exercisePlanEvent = event.get(0);
            }else{
                exercisePlanEvent = new ExercisePlanEvent();
                exercisePlanEvent.setId(-1);
                exercisePlanEvent.setDate(formattedDate);
            }

            EventDialogFragment eventDialogFragment = EventDialogFragment.newInstance(formattedDate, exercisePlanEvent.getTime(), exercisePlanEvent.getExercise(), exercisePlanEvent.getId());
            eventDialogFragment.setExerciseEventListener(exerciseEventListener);
            eventDialogFragment.setExercisePlanListener(exercisePlanListener);
            eventDialogFragment.show(getFragmentManager(),"EventDialog");


        }

        @Override
        public void onMonthScroll(Date firstDayOfNewMonth) {
            SimpleDateFormat month_format = new SimpleDateFormat("MMMM , yyyy");
            String month = month_format.format(firstDayOfNewMonth);
            tvCalendarMonth.setText(month);
        }
    };


    public void setExercisePlanListener(ExercisePlanListener exercisePlanListener){
        this.exercisePlanListener = exercisePlanListener;
    }

    private void init(){
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvCalendarMonth.setTypeface(typefaceCR);
        tvHeader.setTypeface(typefaceCR);

        compactCalendarView.setListener(compactCalendarViewListener);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");


        db = new RealmDatabase(getActivity());

        ArrayList<String> events = db.getAllExercisePlanEvents();

        for(int i=0; i<events.size(); i++){
            try {
                Date date = format.parse(events.get(i));
                Event event = new Event(Color.GREEN, date.getTime());
                compactCalendarView.addEvent(event);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        Log.i("First day", compactCalendarView.getFirstDayOfCurrentMonth()+"");

        SimpleDateFormat month_format = new SimpleDateFormat("MMMM , yyyy");
        String month = month_format.format(compactCalendarView.getFirstDayOfCurrentMonth());
        tvCalendarMonth.setText(month);

    }


    private ExerciseEventListener exerciseEventListener = new ExerciseEventListener() {
        @Override
        public void addEvent(String sdate) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = format.parse(sdate);
                Event event = new Event(Color.GREEN, date.getTime());

                compactCalendarView.addEvent(event);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };


}
