package com.ngng.healthzin.Fragments.Overview.Interfaces;

/**
 * Created by nzisis on 21/01/17.
 */
public interface OverviewExerciseListener {
    void moveToNext();
}
