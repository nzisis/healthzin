package com.ngng.healthzin.Fragments.Settings;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.ngng.healthzin.R;

import me.grantland.widget.AutofitTextView;

/**
 * Created by nzisis on 12/03/17.
 */
public class InstructionsDialog extends DialogFragment {
    private TextView tvInstructionsHeader, tvNutritionHeader, tvExerciseHeader, tvProfileHeader;
    private AutofitTextView tvMedicationHeader, tvMedicalPlanHeader;
    private CheckBox cbNutrition, cbMedication, cbExercise, cbProfile, cbMedicalPlan;
    private Button bDone;

    private Dialog dialog;
    private SharedPreferences sharedPreferences;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.instructions_dialog);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        initUI();
        init();
        initListeners();

        return dialog;
    }


    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvInstructionsHeader.setTypeface(typefaceCR);
        tvExerciseHeader.setTypeface(typefaceCR);
        tvNutritionHeader.setTypeface(typefaceCR);
        tvMedicalPlanHeader.setTypeface(typefaceCR);
        tvMedicationHeader.setTypeface(typefaceCR);
        tvProfileHeader.setTypeface(typefaceCR);

        cbNutrition.setChecked(!sharedPreferences.getBoolean("nutritionShowcase", false));
        cbExercise.setChecked(!sharedPreferences.getBoolean("exerciseShowcase", false));
        cbMedication.setChecked(!sharedPreferences.getBoolean("medicationShowcase", false));
        cbProfile.setChecked(!sharedPreferences.getBoolean("profileShowcase", false));
        cbMedicalPlan.setChecked(!sharedPreferences.getBoolean("medicationPlanShowcase", false));
    }

    private void initUI() {
        tvInstructionsHeader = (TextView) dialog.findViewById(R.id.tvInstructionsHeader);
        tvNutritionHeader = (TextView) dialog.findViewById(R.id.tvNutritionHeader);
        tvMedicationHeader = (AutofitTextView) dialog.findViewById(R.id.tvMedicationHeader);
        tvExerciseHeader = (TextView) dialog.findViewById(R.id.tvExerciseHeader);
        tvProfileHeader = (TextView) dialog.findViewById(R.id.tvProfileHeader);
        tvMedicalPlanHeader = (AutofitTextView) dialog.findViewById(R.id.tvMedicalHeader);

        cbNutrition = (CheckBox) dialog.findViewById(R.id.cbNutrition);
        cbExercise = (CheckBox) dialog.findViewById(R.id.cbExercise);
        cbMedicalPlan = (CheckBox) dialog.findViewById(R.id.cbMedicalPlan);
        cbMedication = (CheckBox) dialog.findViewById(R.id.cbMedication);
        cbProfile = (CheckBox) dialog.findViewById(R.id.cbProfile);

        bDone = (Button) dialog.findViewById(R.id.bDone);

    }

    private void initListeners() {
        cbNutrition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sharedPreferences.edit().putBoolean("nutritionShowcase", !isChecked).apply();
            }
        });

        cbExercise.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sharedPreferences.edit().putBoolean("exerciseShowcase", !isChecked).apply();
            }
        });

        cbMedication.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sharedPreferences.edit().putBoolean("medicationShowcase", !isChecked).apply();
            }
        });

        cbProfile.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sharedPreferences.edit().putBoolean("profileShowcase", !isChecked).apply();
            }
        });

        cbMedicalPlan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sharedPreferences.edit().putBoolean("medicationPlanShowcase", !isChecked).apply();
            }
        });

        bDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }


}
