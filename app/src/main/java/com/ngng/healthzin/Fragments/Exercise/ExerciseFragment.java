package com.ngng.healthzin.Fragments.Exercise;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.makeramen.roundedimageview.RoundedImageView;
import com.michael.easydialog.EasyDialog;
import com.ngng.healthzin.Activities.OverviewActivity;
import com.ngng.healthzin.Adapters.AutoCompleteTextview.AutoCompleteTextViewAdapter;
import com.ngng.healthzin.Fragments.Exercise.Interfaces.ExerciceListener;
import com.ngng.healthzin.Fragments.Exercise.Interfaces.ExerciseDetailsListener;
import com.ngng.healthzin.Fragments.Menu.Interfaces.NavigationListener;
import com.ngng.healthzin.Model.Realm.AutoCompleteExercise;
import com.ngng.healthzin.Model.Realm.Exercise;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Stack;

import me.grantland.widget.AutofitTextView;

/**
 * Created by nzisis on 11/24/16.
 */
public class ExerciseFragment extends Fragment {

    private Toolbar toolbar;
    private TextView tvExercise, tvWorkTime, tvHeader, tvPeriod;
    private AutofitTextView tvDate, tvTime;
    private AutoCompleteTextView actvExercise;
    private EditText etWorkTime;
    private RoundedImageView ivExercise;
    private ImageButton ibDate, ibTime;
    private ImageView ivTip;
    private CardView cvWorktime, cvExercise;

    private RadioButton bMorn;
    private RadioButton bNoon;
    private RadioButton bNight;


    private RelativeLayout rlExercise, rlWorkTime;


    private CalendarDatePickerDialogFragment cdate;
    private RadialTimePickerDialogFragment rtime;

    private String date, time, period;

    private FragmentManager manager;
    private HashMap<String, Fragment> components;
    private Stack<String> fragmentsStack;

    private String tag;
    private boolean show;

    private ExerciceListener exerciceListener;
    private NavigationListener navigationListener;

    private LayoutInflater inflater;

    private AutoCompleteTextViewAdapter adapter;


    private View setupView, doneView;
    private ShowcaseView svExercise;
    private int showcase;
    private SharedPreferences sharedPreferences;

    private InputMethodManager inputMethodManagerExercise, inputMethodManagerWorkoutTime;

    private static final Field sChildFragmentManagerField;


    static {
        Field f = null;
        try {
            f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.e("EEEE", "Error getting mChildFragmentManager field", e);
        }
        sChildFragmentManagerField = f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.inflater = inflater;

        View view = inflater.inflate(R.layout.fragment_exercise, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.menu_drawer);

        ((OverviewActivity) getActivity()).setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();
                navigationListener.onNavigationClicked();
            }
        });

        toolbar.setTitle(getString(R.string.exercise));


        bMorn = (RadioButton) view.findViewById(R.id.rbMorn);
        bNoon = (RadioButton) view.findViewById(R.id.rbNoon);
        bNight = (RadioButton) view.findViewById(R.id.rbNight);


        tvExercise = (TextView) view.findViewById(R.id.tvExercise);
        tvWorkTime = (TextView) view.findViewById(R.id.tvWorkTime);
        tvDate = (AutofitTextView) view.findViewById(R.id.tvDate);
        tvTime = (AutofitTextView) view.findViewById(R.id.tvTime);
        tvHeader = (TextView) view.findViewById(R.id.tvHeader);
        tvPeriod = (AutofitTextView) view.findViewById(R.id.tvPeriod);


        actvExercise = (AutoCompleteTextView) view.findViewById(R.id.etExercise);
        etWorkTime = (EditText) view.findViewById(R.id.etWorkTime);

        ivExercise = (RoundedImageView) view.findViewById(R.id.rivExercise);

        ibDate = (ImageButton) view.findViewById(R.id.ibDate);
        ibTime = (ImageButton) view.findViewById(R.id.ibTime);

        ivTip = (ImageView) view.findViewById(R.id.ivTip);

        rlExercise = (RelativeLayout) view.findViewById(R.id.rlExercise);
        rlWorkTime = (RelativeLayout) view.findViewById(R.id.rlWorkTime);

        cvWorktime = (CardView) view.findViewById(R.id.cvWorkTime);
        cvExercise = (CardView) view.findViewById(R.id.cvExercise);

        actvExercise.setVisibility(View.GONE);
        etWorkTime.setVisibility(View.GONE);
        ivTip.setVisibility(View.GONE);

        setHasOptionsMenu(true);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();
    }


    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_exercise, menu);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                MenuItem setup = menu.findItem(R.id.action_Setup);
                MenuItem done = menu.findItem(R.id.action_Done);

                setupView = getActivity().findViewById(setup.getItemId());
                doneView = getActivity().findViewById(done.getItemId());
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_Done:
                Log.e("Done", "Pressed");
                addExercise();
                break;

            case R.id.action_Setup:
                exerciceListener.showWeeklyExercise();
                break;
        }

        return false;
    }


    private void addExercise() {

        if (actvExercise.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getString(R.string.enter_exercise), Toast.LENGTH_SHORT).show();
            return;
        }

        if (etWorkTime.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getString(R.string.enter_exercise_time), Toast.LENGTH_SHORT).show();
            return;
        }

        RealmDatabase db = new RealmDatabase(getActivity());

        Exercise exercise = new Exercise(actvExercise.getText().toString(), date, time, Double.valueOf(etWorkTime.getText().toString()), period);
        exercise.setId(db.getExerciseNextKey());

        AutoCompleteExercise autoCompleteExercise = new AutoCompleteExercise(actvExercise.getText().toString().toLowerCase());

        db.insertExercise(exercise);
        db.insertAutoCompleteExercise(autoCompleteExercise);
        db.getAllExercise();

        closeKeyboard();
        closeInnerFragment();

        exerciceListener.addedExercise();

    }

    public void setExerciseListener(ExerciceListener exerciseListener) {
        this.exerciceListener = exerciseListener;
    }


    public void setNavigationListener(NavigationListener navigationListener) {
        this.navigationListener = navigationListener;
    }


    private void initListeners() {

        rlExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvExercise.setVisibility(View.GONE);
                actvExercise.setVisibility(View.VISIBLE);
                actvExercise.requestFocus();
                inputMethodManagerExercise.showSoftInput(actvExercise, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        rlWorkTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvWorkTime.setVisibility(View.GONE);
                etWorkTime.setVisibility(View.VISIBLE);
                ivTip.setVisibility(View.VISIBLE);
                etWorkTime.requestFocus();
                inputMethodManagerWorkoutTime.showSoftInput(etWorkTime, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        ibDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exerciceListener.showDateDialog(cdate);
            }
        });


        ibTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exerciceListener.showTimeDialog(rtime);
            }
        });

        ivExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exerciseDetailsListener.showDetails();

            }
        });

        bMorn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                period = getString(R.string.period_morning);
            }
        });

        bNoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                period = getString(R.string.period_noon);
            }
        });

        bNight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                period = getString(R.string.period_evening);
            }
        });

        ivTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View view = inflater.inflate(R.layout.tip_content, null);

                TextView tvTip = (TextView) view.findViewById(R.id.tvTip);
                tvTip.setText(getString(R.string.exercise_tip));

                new EasyDialog(getActivity())
                        .setLayout(view)
                        .setBackgroundColor(getResources().getColor(R.color.app_blue))
                        .setLocationByAttachedView(cvWorktime)
                        .setGravity(EasyDialog.GRAVITY_TOP)
                        .setAnimationTranslationShow(EasyDialog.DIRECTION_X, 1000, -600, 100, -50, 50, 0)
                        .setAnimationAlphaShow(1000, 0.3f, 1.0f)
                        .setAnimationTranslationShow(EasyDialog.DIRECTION_Y, 1000, -800, 100, -50, 50, 0)
                        .setAnimationTranslationDismiss(EasyDialog.DIRECTION_Y, 500, 0, -800)
                        .setTouchOutsideDismiss(true)
                        .setMatchParent(false)
                        .setMarginLeftAndRight(24, 24)
                        .show();


            }
        });
    }

    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");

        tvHeader.setTypeface(typefaceCR);
        tvExercise.setTypeface(typefaceCR);
        tvWorkTime.setTypeface(typefaceCR);
        tvDate.setTypeface(typefaceCR);
        tvPeriod.setTypeface(typefaceCR);
        tvTime.setTypeface(typefaceCR);
        actvExercise.setTypeface(typefaceCR);
        etWorkTime.setTypeface(typefaceCR);
        bMorn.setTypeface(typefaceCR);
        bNoon.setTypeface(typefaceCR);
        bNight.setTypeface(typefaceCR);

        inputMethodManagerExercise = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManagerWorkoutTime = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);


        adapter = new AutoCompleteTextViewAdapter(getActivity(), 2);
        actvExercise.setAdapter(adapter);

        showcase = 0;

        if (!sharedPreferences.getBoolean("exerciseShowcase", false)) {

            ((OverviewActivity) getActivity()).setDisable_back_button(true);

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);


            Target viewTarget = new ViewTarget(R.id.rivExercise, getActivity());
            svExercise = new ShowcaseView.Builder(getActivity())
                    .withMaterialShowcase()
                    .blockAllTouches()
                    .setTarget(viewTarget)
                    .setContentTitle(getString(R.string.exercise) + " (1/4)")
                    .setContentText(getString(R.string.instructions_exercise_1))
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setOnClickListener(showCaseListener)
                    .build();

            svExercise.setButtonText(getString(R.string.instructions_got_it));
            svExercise.setButtonPosition(lps);
        }

        manager = getChildFragmentManager();
        fragmentsStack = new Stack<>();
        components = new HashMap<>();

        show = false;

        tag = "ExerciseDetailsFragment";
        ExerciseDetailsFragment fragment = new ExerciseDetailsFragment();
        components.put(tag, fragment);


        Calendar calendar = Calendar.getInstance();

        cdate = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(dateSetListener)
                .setDoneText("OK")
                .setCancelText(getString(R.string.cancel))
                .setPreselectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));


        rtime = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(timeSetListener)
                .setDoneText("OK")
                .setCancelText(getString(R.string.cancel))
                .setStartTime(calendar.getTime().getHours(), calendar.getTime().getMinutes());


        time = TimeManager.getCurrentTime();
        date = TimeManager.getCurrentDate();
        period = "";


        tvDate.setText(getString(R.string.today));
        tvTime.setText(time);
    }


    private CalendarDatePickerDialogFragment.OnDateSetListener dateSetListener = new CalendarDatePickerDialogFragment.OnDateSetListener() {
        @Override
        public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
            String month, day;
            monthOfYear++;
            if (monthOfYear < 10)
                month = "0" + monthOfYear;
            else
                month = String.valueOf(monthOfYear);
            if (dayOfMonth < 10)
                day = "0" + dayOfMonth;
            else
                day = String.valueOf(dayOfMonth);
            date = year + "-" + month + "-" + day;


            if (TimeManager.isToday(date)) {
                tvDate.setText(getString(R.string.today));
            } else if (TimeManager.isYesterday(date)) {
                tvDate.setText(getString(R.string.yesterday));
            } else {
                tvDate.setText(date);
            }


        }
    };

    private RadialTimePickerDialogFragment.OnTimeSetListener timeSetListener = new RadialTimePickerDialogFragment.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
            String hours, mins;
            if (hourOfDay < 10)
                hours = "0" + hourOfDay;
            else
                hours = String.valueOf(hourOfDay);
            if (minute < 10)
                mins = "0" + minute;
            else
                mins = String.valueOf(minute);
            time = hours + ":" + mins;

            tvTime.setText(time);

        }
    };

    private ExerciseDetailsListener exerciseDetailsListener = new ExerciseDetailsListener() {
        @Override
        public void showDetails() {
            if (!show) {
                fragmentsStack.push(tag);

                Fragment fragment = components.get(tag);
                FragmentTransaction fragmentTransaction = manager.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), fragmentTransaction, null, fragment, R.id.flExercise);
                fragmentTransactionExtended.addTransition(FragmentTransactionExtended.SLIDE_HORIZONTAL);
                fragmentTransactionExtended.commit();

                show = true;
            } else {
                Fragment fragment = components.get(tag);
                fragmentsStack.pop();
                manager.beginTransaction().remove(fragment).commit();

                show = false;

            }
        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        closeInnerFragment();
    }


    @Override
    public void onDetach() {
        super.onDetach();

        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e) {
                Log.e("EEEE", "Error setting mChildFragmentManager field", e);
            }
        }
    }


    private View.OnClickListener showCaseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (showcase) {
                case 0:
                    svExercise.setContentTitle("");
                    svExercise.setContentText("");
                    svExercise.setShowcase(new ViewTarget(cvExercise), true);
                    svExercise.setContentTitle(getString(R.string.exercise) + " (2/4)");
                    svExercise.setContentText(getString(R.string.instructions_exercise_2));
                    svExercise.setButtonText(getString(R.string.instructions_got_it));
                    break;
                case 1:
                    svExercise.setContentTitle("");
                    svExercise.setContentText("");
                    svExercise.setShowcase(new ViewTarget(setupView), true);
                    svExercise.setContentTitle(getString(R.string.exercise) + " (3/4)");
                    svExercise.setContentText(getString(R.string.instructions_exercise_3));
                    svExercise.setButtonText(getString(R.string.instructions_got_it));
                    break;
                case 2:
                    svExercise.setContentTitle("");
                    svExercise.setContentText("");
                    svExercise.setShowcase(new ViewTarget(doneView), true);
                    svExercise.setContentTitle(getString(R.string.exercise) + " (4/4)");
                    svExercise.setContentText(getString(R.string.instructions_exercise_4));
                    svExercise.setButtonText(getString(R.string.instructions_got_it));

                    break;
                case 3:
                    svExercise.hide();
                    ((OverviewActivity) getActivity()).setDisable_back_button(false);
                    sharedPreferences.edit().putBoolean("exerciseShowcase", true).apply();
                    break;
            }
            showcase++;
        }
    };

    public void closeKeyboard() {
        if (inputMethodManagerExercise.isActive())
            inputMethodManagerExercise.hideSoftInputFromWindow(actvExercise.getWindowToken(), 0);
        else if (inputMethodManagerWorkoutTime.isActive())
            inputMethodManagerWorkoutTime.hideSoftInputFromWindow(etWorkTime.getWindowToken(), 0);
    }

    private void closeInnerFragment() {
        if (show) {
            Fragment fragment = components.get(tag);
            fragmentsStack.pop();
            manager.beginTransaction().remove(fragment).commit();

            show = false;
        }
    }

}
