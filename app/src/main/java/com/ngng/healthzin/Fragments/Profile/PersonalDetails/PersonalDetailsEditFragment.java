package com.ngng.healthzin.Fragments.Profile.PersonalDetails;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ngng.healthzin.Fragments.Profile.Intefaces.PersonalDetailsEditListener;
import com.ngng.healthzin.Model.User.UserInstance;
import com.ngng.healthzin.R;

/**
 * Created by nzisis on 17/01/17.
 */
public class PersonalDetailsEditFragment extends Fragment implements PersonalDetailsEditListener {
    private TextView tvNameHeader,tvSurnameHeader,tvAgeHeader,tvSexHeader;
    private EditText etName,etSurname,etAge;
    private RadioButton rbMale,rbFemale;

    private String sex;
    private int age;
    private SharedPreferences sharedPreferences;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal_details_edit,container,false);

        tvNameHeader = (TextView) view.findViewById(R.id.tvNameHeader);
        tvSurnameHeader = (TextView) view.findViewById(R.id.tvSurnameHeader);
        tvAgeHeader = (TextView) view.findViewById(R.id.tvAgeHeader);
        tvSexHeader = (TextView) view.findViewById(R.id.tvSexHeader);

        etName = (EditText) view.findViewById(R.id.etName);
        etSurname = (EditText) view.findViewById(R.id.etSurname);
        etAge = (EditText) view.findViewById(R.id.etAge);

        rbMale = (RadioButton) view.findViewById(R.id.rbMale);
        rbFemale = (RadioButton) view.findViewById(R.id.rbFemale);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();
    }

    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvNameHeader.setTypeface(typefaceCR);
        tvSexHeader.setTypeface(typefaceCR);
        tvSurnameHeader.setTypeface(typefaceCR);
        tvAgeHeader.setTypeface(typefaceCR);
        etName.setTypeface(typefaceCR);
        etAge.setTypeface(typefaceCR);
        etName.setTypeface(typefaceCR);
        rbFemale.setTypeface(typefaceCR);
        rbMale.setTypeface(typefaceCR);


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        sex = UserInstance.getInstance().getSex();
        age = UserInstance.getInstance().getAge();

        if(sex.equals(getString(R.string.male))) rbMale.setChecked(true);
        else if(sex.equals(getString(R.string.female))) rbFemale.setChecked(true);

        etName.setText(UserInstance.getInstance().getFirst_name() );
        etSurname.setText(UserInstance.getInstance().getLast_name());

        if(age!=-1) etAge.setText(age + "");
    }

    private void initListeners() {
        rbMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sex = getString(R.string.male);
            }
        });

        rbFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sex = getString(R.string.female);
            }
        });
    }

    @Override
    public void edit() {
        if(!etAge.getText().toString().trim().equals("")){
            UserInstance.getInstance().setAge(Integer.parseInt(etAge.getText().toString().trim()));
            sharedPreferences.edit().putInt("age", UserInstance.getInstance().getAge()).apply();
        }

        UserInstance.getInstance().setSex(sex);
        UserInstance.getInstance().setFirst_name(etName.getText().toString().trim());
        UserInstance.getInstance().setLast_name(etSurname.getText().toString().trim());

        sharedPreferences.edit().putString("first_name", UserInstance.getInstance().getFirst_name()).apply();
        sharedPreferences.edit().putString("last_name", UserInstance.getInstance().getLast_name()).apply();
        sharedPreferences.edit().putString("sex", UserInstance.getInstance().getSex()).apply();
    }
}
