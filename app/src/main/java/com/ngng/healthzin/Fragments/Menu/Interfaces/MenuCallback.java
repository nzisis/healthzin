package com.ngng.healthzin.Fragments.Menu.Interfaces;

/**
 * Created by nzisis on 11/24/16.
 */
public interface MenuCallback {
    void onItemClicked(int position);
    void profileClicked();
    void settingClicked();
}
