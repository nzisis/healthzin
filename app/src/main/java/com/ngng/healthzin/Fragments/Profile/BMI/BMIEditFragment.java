package com.ngng.healthzin.Fragments.Profile.BMI;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.ngng.healthzin.Fragments.Profile.Intefaces.BMIEditListener;
import com.ngng.healthzin.Model.User.UserInstance;
import com.ngng.healthzin.R;

import java.text.DecimalFormat;

/**
 * Created by nzisis on 18/01/17.
 */
public class BMIEditFragment extends Fragment implements BMIEditListener {
    private TextView tvWeightHeader, tvHeightHeader;
    private EditText etWeight, etHeight;

    private SharedPreferences sharedPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bmi_edit, container, false);

        tvHeightHeader = (TextView) view.findViewById(R.id.tvHeightHeader);
        tvWeightHeader = (TextView) view.findViewById(R.id.tvWeightHeader);

        etHeight = (EditText) view.findViewById(R.id.etHeight);
        etWeight = (EditText) view.findViewById(R.id.etWeight);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvHeightHeader.setTypeface(typefaceCR);
        tvWeightHeader.setTypeface(typefaceCR);
        etWeight.setTypeface(typefaceCR);
        etHeight.setTypeface(typefaceCR);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        float height = UserInstance.getInstance().getHeight();
        float weight = UserInstance.getInstance().getWeight();

        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        if (height != -1) etHeight.setText(decimalFormat.format(height) + "");
        if (weight != -1) etWeight.setText(weight + "");


    }

    @Override
    public void edit() {
        if (!etHeight.getText().toString().trim().equals("")) {
            UserInstance.getInstance().setHeight(Float.parseFloat(etHeight.getText().toString().trim()));
            sharedPreferences.edit().putFloat("height", UserInstance.getInstance().getHeight()).apply();
        }

        if (!etWeight.getText().toString().trim().equals("")) {
            UserInstance.getInstance().setWeight(Float.parseFloat(etWeight.getText().toString().trim()));
            sharedPreferences.edit().putFloat("weight", UserInstance.getInstance().getWeight()).apply();
        }


    }
}
