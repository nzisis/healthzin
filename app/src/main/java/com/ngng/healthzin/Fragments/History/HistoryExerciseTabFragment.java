package com.ngng.healthzin.Fragments.History;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
//import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ngng.healthzin.Adapters.History.HistoryTabAdapter;
import com.ngng.healthzin.Fragments.History.Interfaces.HistoryListener;
import com.ngng.healthzin.Model.History.ExerciseGroupItem;
import com.ngng.healthzin.Model.History.ExerciseHistory;
import com.ngng.healthzin.Model.History.MonthAndYear;
import com.ngng.healthzin.Model.Realm.Exercise;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.DividerItemDecoration;
import com.ngng.healthzin.Utils.TimeManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import io.realm.RealmResults;

/**
 * Created by nzisis on 23/12/16.
 */
public class HistoryExerciseTabFragment extends Fragment {

    private RecyclerView rvHistory;
    private LinearLayout llNoData;
    private TextView tvNoDataHeader, tvRecord;
    private FloatingActionButton fabAdd;
    private HashSet<MonthAndYear> monthAndYear;

    private RealmDatabase db;

    private HistoryTabAdapter adapter;

    private HistoryListener historyListener;
    private ImageView ivNoData;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_tab, container, false);

        llNoData = (LinearLayout) view.findViewById(R.id.llNoData);
        tvNoDataHeader = (TextView) view.findViewById(R.id.tvNoDataHeader);
        tvRecord = (TextView) view.findViewById(R.id.tvRecord);
        fabAdd = (FloatingActionButton) view.findViewById(R.id.fabRecord);
        ivNoData = (ImageView) view.findViewById(R.id.ivNoData);

        rvHistory = (RecyclerView) view.findViewById(R.id.rvHistory);
        rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvHistory.addItemDecoration(new DividerItemDecoration(getActivity().getResources().getDrawable(R.drawable.divider), false, false));


        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();
    }


    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvNoDataHeader.setTypeface(typefaceCR);
        tvRecord.setTypeface(typefaceCR);

        ivNoData.setImageResource(R.drawable.exercise_logo_circle_colorful);

        db = new RealmDatabase(getActivity());

        initExercise();
    }

    private void initNoData(boolean flag) {
        if (!flag) {
            llNoData.setVisibility(View.GONE);
            ivNoData.setVisibility(View.GONE);
        } else {
            llNoData.setVisibility(View.VISIBLE);
            tvNoDataHeader.setText(getString(R.string.no_data));
            tvRecord.setText(getString(R.string.record_exercise));
            ivNoData.setVisibility(View.VISIBLE);
        }
    }

    private void initNoDataFilterExercise(boolean flag){
        if (!flag) {
            llNoData.setVisibility(View.GONE);
        } else {
            llNoData.setVisibility(View.VISIBLE);
            tvNoDataHeader.setText(getString(R.string.filter_no_exercise));
            tvRecord.setText(getString(R.string.filter_go_back));
        }
    }


    private void initNoDataFilterDate(boolean flag){
        if (!flag) {
            llNoData.setVisibility(View.GONE);
        } else {
            llNoData.setVisibility(View.VISIBLE);
            tvNoDataHeader.setText(getString(R.string.filter_no_date));
            tvRecord.setText(getString(R.string.filter_go_back));
        }
    }

    private void initListeners() {
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historyListener.addExercise();
            }
        });

    }


    public void initExercise() {
        List<ExerciseHistory> exerciseHistory = new ArrayList<>();
        ArrayList<ExerciseGroupItem> exerciseGroupItems = new ArrayList<>();
        monthAndYear = new HashSet<>();

        RealmResults<Exercise> allExercise = db.getAllExerciseFromNewestToOldest();
        initNoData(allExercise.isEmpty());

        for (int j = 0; j < allExercise.size(); j++) {
            Exercise child = allExercise.get(j);
            String tokens[] = child.getDate().split("-");
            int year = Integer.parseInt(tokens[0]);
            String month = TimeManager.getMonthFromInt(Integer.parseInt(tokens[1]) - 1);
            MonthAndYear may = new MonthAndYear(month, year);

            if (!monthAndYear.contains(may)) {
                ExerciseGroupItem group = new ExerciseGroupItem(month, year);
                exerciseGroupItems.add(group);
                monthAndYear.add(may);
            }


            for (int i = 0; i < exerciseGroupItems.size(); i++) {
                ExerciseGroupItem item = exerciseGroupItems.get(i);
                if (item.getMonth() == month && item.getYear() == year) {
                    exerciseGroupItems.get(i).addChild(child);
                    break;
                }

            }


        }


        for (int i = 0; i < exerciseGroupItems.size(); i++) {
            ExerciseGroupItem item = exerciseGroupItems.get(i);
            item.setup();
            ExerciseHistory exerciseHistoryGroup = new ExerciseHistory(item.getMonth() + " " + item.getYear(), item.getDistict_childs());
            exerciseHistoryGroup.setInnerItems(item.getGroup_of_childs());
            exerciseHistory.add(exerciseHistoryGroup);

        }

        if(adapter!=null) adapter = null;
        adapter = new HistoryTabAdapter(exerciseHistory, getActivity(), 2);

        rvHistory.setAdapter(adapter);


    }

    private List<ExerciseHistory> initExercise(RealmResults<Exercise> allExercise) {
        List<ExerciseHistory> exerciseHistory = new ArrayList<>();
        ArrayList<ExerciseGroupItem> exerciseGroupItems = new ArrayList<>();
        monthAndYear = new HashSet<>();


        for (int j = 0; j < allExercise.size(); j++) {
            Exercise child = allExercise.get(j);
            String tokens[] = child.getDate().split("-");
            int year = Integer.parseInt(tokens[0]);
            String month = TimeManager.getMonthFromInt(Integer.parseInt(tokens[1]) - 1);
            MonthAndYear may = new MonthAndYear(month, year);

            if (!monthAndYear.contains(may)) {
                ExerciseGroupItem group = new ExerciseGroupItem(month, year);
                exerciseGroupItems.add(group);
                monthAndYear.add(may);
            }


            for (int i = 0; i < exerciseGroupItems.size(); i++) {
                ExerciseGroupItem item = exerciseGroupItems.get(i);
                if (item.getMonth() == month && item.getYear() == year) {
                    exerciseGroupItems.get(i).addChild(child);
                    break;
                }

            }


        }


        for (int i = 0; i < exerciseGroupItems.size(); i++) {
            ExerciseGroupItem item = exerciseGroupItems.get(i);
            item.setup();
            ExerciseHistory exerciseHistoryGroup = new ExerciseHistory(item.getMonth() + " " + item.getYear(), item.getDistict_childs());
            exerciseHistoryGroup.setInnerItems(item.getGroup_of_childs());
            exerciseHistory.add(exerciseHistoryGroup);

        }


        return exerciseHistory;
    }


    public void setHistoryListener(HistoryListener historyListener) {
        this.historyListener = historyListener;
    }

    public void filterExercise(String filter) {
        RealmResults<Exercise> exercises = db.getExerciseByName(filter);

        List<ExerciseHistory> nutritionHistory = initExercise(exercises);
        if (adapter != null) {
            adapter = null;
            adapter = new HistoryTabAdapter(nutritionHistory, getActivity(), 2);
            if (rvHistory != null) rvHistory.setAdapter(adapter);
        }

        initNoDataFilterExercise(nutritionHistory.isEmpty());

    }

    public void filterDate(String filter) {
        db = new RealmDatabase(getActivity());
        RealmResults<Exercise> exercises = db.getExerciseByDate(filter);

        List<ExerciseHistory> nutritionHistory = initExercise(exercises);
        if (adapter != null) {
            adapter = null;
            adapter = new HistoryTabAdapter(nutritionHistory, getActivity(), 2);
            if (rvHistory != null) rvHistory.setAdapter(adapter);
        }

        initNoDataFilterDate(nutritionHistory.isEmpty());

    }
}
