package com.ngng.healthzin.Fragments.Exercise.ExercisePlan;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.ngng.healthzin.Fragments.Exercise.Interfaces.ExerciseEventListener;
import com.ngng.healthzin.Fragments.Exercise.Interfaces.ExercisePlanListener;
import com.ngng.healthzin.Model.Realm.ExercisePlanEvent;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;

import java.util.Calendar;

/**
 * Created by nzisis on 11/26/16.
 */
public class EventDialogFragment extends DialogFragment {

  private TextView tvDate,tvTime;
  private ImageButton ibTime;
  private EditText etExercise;
  private Button bDone;

  private RadialTimePickerDialogFragment rtime;

  private String date,time,exercise;
  private int id;
  private RealmDatabase db;

  private Dialog dialog;
  private ExerciseEventListener exerciseEventListener;
  private ExercisePlanListener exercisePlanListener;


 public static EventDialogFragment newInstance(String date,String time,String exercise,int id){
     EventDialogFragment eventDialogFragment = new EventDialogFragment();

     Bundle args = new Bundle();
     args.putString("date",date);
     args.putString("time",time);
     args.putString("exercise",exercise);
     args.putInt("id", id);

     eventDialogFragment.setArguments(args);

     return eventDialogFragment;

 }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_event_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.app_gray)));


        date = getArguments().getString("date");
        time = getArguments().getString("time");
        exercise = getArguments().getString("exercise");
        id = getArguments().getInt("id");


        initUI();
        init();
        initListeners();

        return dialog;
    }

    private void initUI(){
        tvDate = (TextView) dialog.findViewById(R.id.tvDate);
        tvTime = (TextView) dialog.findViewById(R.id.tvTime);
        ibTime = (ImageButton) dialog.findViewById(R.id.ibTime);
        etExercise = (EditText) dialog.findViewById(R.id.etExercise);
        bDone = (Button) dialog.findViewById(R.id.bDone);
    }



    private void init(){
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvDate.setTypeface(typefaceCR);
        tvTime.setTypeface(typefaceCR);
        etExercise.setTypeface(typefaceCR);


        Calendar calendar = Calendar.getInstance();
        db = new RealmDatabase(getActivity());

        rtime = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(timeSetListener)
                .setDoneText("OK")
                .setCancelText(getString(R.string.cancel))
                .setStartTime(calendar.getTime().getHours(), calendar.getTime().getMinutes());



        if(time == ""){
            time = TimeManager.getCurrentTime();
        }

        tvDate.setText(date);
        tvTime.setText(time);
        etExercise.setText(exercise);
    }


    private RadialTimePickerDialogFragment.OnTimeSetListener timeSetListener = new RadialTimePickerDialogFragment.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
            String hours, mins;
            if (hourOfDay < 10)
                hours = "0" + hourOfDay;
            else
                hours = String.valueOf(hourOfDay);
            if (minute < 10)
                mins = "0" + minute;
            else
                mins = String.valueOf(minute);
            time = hours + ":" + mins;

            tvTime.setText(time);

        }
    };


    private void initListeners(){


        ibTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exercisePlanListener.showTime(rtime);
            }
        });

        bDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean update = true;
                if(id == -1) {
                    id = db.getExercisePlanEventNextKey();
                    update = false;
                }
                ExercisePlanEvent exerciseEvent = new ExercisePlanEvent(etExercise.getText().toString(),date,time);
                exerciseEvent.setId(id);

                db.insertOrUpdateExercisePlan(exerciseEvent);

                if(!update) exerciseEventListener.addEvent(date);
                dismiss();

            }
        });


    }

    public void setExerciseEventListener(ExerciseEventListener exerciseEventListener){
        this.exerciseEventListener = exerciseEventListener;
    }

    public void setExercisePlanListener(ExercisePlanListener exercisePlanListener){
        this.exercisePlanListener = exercisePlanListener ;
    }

}
