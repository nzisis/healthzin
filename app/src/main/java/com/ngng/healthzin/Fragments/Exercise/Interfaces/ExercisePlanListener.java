package com.ngng.healthzin.Fragments.Exercise.Interfaces;

import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

/**
 * Created by nzisis on 09/02/17.
 */
public interface ExercisePlanListener {
    void showTime(RadialTimePickerDialogFragment fragment);
}
