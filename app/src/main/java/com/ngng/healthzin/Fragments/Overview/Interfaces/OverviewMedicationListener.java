package com.ngng.healthzin.Fragments.Overview.Interfaces;

/**
 * Created by nzisis on 24/01/17.
 */
public interface OverviewMedicationListener {
    void moveToNext();
}
