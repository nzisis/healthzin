package com.ngng.healthzin.Fragments.Settings.Interfaces;

/**
 * Created by nzisis on 09/02/17.
 */
public interface SettingsListener {
    void showMedicationPlanHistory();
    void showExercisePlan();
}
