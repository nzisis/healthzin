package com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces;


/**
 * Created by nzisis on 08/02/17.
 */
public interface MedicationPlanHistoryListener {
    void newMedicationPlan();
}
