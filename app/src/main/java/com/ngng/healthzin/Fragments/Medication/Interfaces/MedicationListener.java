package com.ngng.healthzin.Fragments.Medication.Interfaces;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

/**
 * Created by nzisis on 29/11/16.
 */
public interface MedicationListener {
    void addedMedication();
    void startMedicationPlan();
    void showTimeDialog(RadialTimePickerDialogFragment fragment);
    void showDateDialog(CalendarDatePickerDialogFragment fragment);
}
