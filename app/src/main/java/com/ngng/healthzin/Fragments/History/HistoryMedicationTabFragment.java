package com.ngng.healthzin.Fragments.History;

import android.app.AlertDialog;
import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
//import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ngng.healthzin.Adapters.History.HistoryTabAdapter;
import com.ngng.healthzin.Fragments.History.Interfaces.HistoryListener;
import com.ngng.healthzin.Model.History.MedicationGroupItem;
import com.ngng.healthzin.Model.History.MedicationHistory;
import com.ngng.healthzin.Model.History.MonthAndYear;
import com.ngng.healthzin.Model.Realm.Medication;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.DividerItemDecoration;
import com.ngng.healthzin.Utils.TimeManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import io.realm.RealmResults;

/**
 * Created by nzisis on 23/12/16.
 */
public class HistoryMedicationTabFragment extends Fragment {

    private RecyclerView rvHistory;
    private LinearLayout llNoData;
    private TextView tvNoDataHeader, tvRecord;
    private FloatingActionButton fabAdd;
    private HashSet<MonthAndYear> monthAndYear;
    private ImageView ivNoData;

    private RealmDatabase db;


    private HistoryTabAdapter adapter;

    private HistoryListener historyListener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_tab, container, false);

        llNoData = (LinearLayout) view.findViewById(R.id.llNoData);
        tvNoDataHeader = (TextView) view.findViewById(R.id.tvNoDataHeader);
        tvRecord = (TextView) view.findViewById(R.id.tvRecord);
        fabAdd = (FloatingActionButton) view.findViewById(R.id.fabRecord);
        ivNoData = (ImageView) view.findViewById(R.id.ivNoData);

        rvHistory = (RecyclerView) view.findViewById(R.id.rvHistory);
        rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvHistory.addItemDecoration(new DividerItemDecoration(getActivity().getResources().getDrawable(R.drawable.divider), false, false));


        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();
    }


    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvNoDataHeader.setTypeface(typefaceCR);
        tvRecord.setTypeface(typefaceCR);

        ivNoData.setImageResource(R.drawable.medication_logo_circle_colorful);

        db = new RealmDatabase(getActivity());

        initMedication();
    }

    private void initNoData(boolean flag) {
        if (!flag) {
            llNoData.setVisibility(View.GONE);
            ivNoData.setVisibility(View.GONE);
        } else {
            llNoData.setVisibility(View.VISIBLE);
            tvNoDataHeader.setText(getString(R.string.no_data));
            tvRecord.setText(getString(R.string.record_medication));
            ivNoData.setVisibility(View.VISIBLE);
        }
    }

    private void initNoDataFilterMedicine(boolean flag) {
        if (!flag) {
            llNoData.setVisibility(View.GONE);
        } else {
            llNoData.setVisibility(View.VISIBLE);
            tvNoDataHeader.setText(getString(R.string.filter_no_medication));
            tvRecord.setText(getString(R.string.filter_go_back));
        }
    }

    private void initNoDataFilterDate(boolean flag) {
        if (!flag) {
            llNoData.setVisibility(View.GONE);
        } else {
            llNoData.setVisibility(View.VISIBLE);
            tvNoDataHeader.setText(getString(R.string.filter_no_date));
            tvRecord.setText(getString(R.string.filter_go_back));
        }
    }

    private void initListeners() {
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historyListener.addMedication();
            }
        });

    }


    public void initMedication() {
        List<MedicationHistory> medicationHistory = new ArrayList<>();
        ArrayList<MedicationGroupItem> medicationGroupItems = new ArrayList<>();
        monthAndYear = new HashSet<>();

        RealmResults<Medication> allExercise = db.getAllMedicationFromNewestToOldest();
        initNoData(allExercise.isEmpty());

        for (int j = 0; j < allExercise.size(); j++) {
            Medication child = allExercise.get(j);
            String tokens[] = child.getDate().split("-");
            int year = Integer.parseInt(tokens[0]);
            String month = TimeManager.getMonthFromInt(Integer.parseInt(tokens[1]) - 1);
            MonthAndYear may = new MonthAndYear(month, year);

            if (!monthAndYear.contains(may)) {
                MedicationGroupItem group = new MedicationGroupItem(month, year);
                medicationGroupItems.add(group);
                monthAndYear.add(may);
            }


            for (int i = 0; i < medicationGroupItems.size(); i++) {
                MedicationGroupItem item = medicationGroupItems.get(i);
                if (item.getMonth() == month && item.getYear() == year) {
                    medicationGroupItems.get(i).addChild(child);
                    break;
                }

            }


        }


        for (int i = 0; i < medicationGroupItems.size(); i++) {
            MedicationGroupItem item = medicationGroupItems.get(i);
            item.setup();
            MedicationHistory medicationHistoryGroup = new MedicationHistory(item.getMonth() + " " + item.getYear(), item.getDistict_childs());
            medicationHistoryGroup.setInnerItems(item.getGroup_of_childs());
            medicationHistory.add(medicationHistoryGroup);

        }


        if(adapter != null) adapter = null;
        adapter = new HistoryTabAdapter(medicationHistory, getActivity(), 3);

        rvHistory.setAdapter(adapter);
    }

    private List<MedicationHistory> initMedication(RealmResults<Medication> allExercise) {
        List<MedicationHistory> medicationHistory = new ArrayList<>();
        ArrayList<MedicationGroupItem> medicationGroupItems = new ArrayList<>();
        monthAndYear = new HashSet<>();


        for (int j = 0; j < allExercise.size(); j++) {
            Medication child = allExercise.get(j);
            String tokens[] = child.getDate().split("-");
            int year = Integer.parseInt(tokens[0]);
            String month = TimeManager.getMonthFromInt(Integer.parseInt(tokens[1]) - 1);
            MonthAndYear may = new MonthAndYear(month, year);

            if (!monthAndYear.contains(may)) {
                MedicationGroupItem group = new MedicationGroupItem(month, year);
                medicationGroupItems.add(group);
                monthAndYear.add(may);
            }


            for (int i = 0; i < medicationGroupItems.size(); i++) {
                MedicationGroupItem item = medicationGroupItems.get(i);
                if (item.getMonth() == month && item.getYear() == year) {
                    medicationGroupItems.get(i).addChild(child);
                    break;
                }

            }


        }


        for (int i = 0; i < medicationGroupItems.size(); i++) {
            MedicationGroupItem item = medicationGroupItems.get(i);
            item.setup();
            MedicationHistory medicationHistoryGroup = new MedicationHistory(item.getMonth() + " " + item.getYear(), item.getDistict_childs());
            medicationHistoryGroup.setInnerItems(item.getGroup_of_childs());
            medicationHistory.add(medicationHistoryGroup);

        }

        return medicationHistory;

    }

    public void setHistoryListener(HistoryListener historyListener) {
        this.historyListener = historyListener;
    }

    public void filterMedicine(String filter) {
        RealmResults<Medication> medications = db.getMedicationByMedicine(filter);

        List<MedicationHistory> medicationHistory = initMedication(medications);
        if (adapter != null) {
            adapter = null;
            adapter = new HistoryTabAdapter(medicationHistory, getActivity(), 3);
            if (rvHistory != null) rvHistory.setAdapter(adapter);
        }

        initNoDataFilterMedicine(medicationHistory.isEmpty());

    }

    public void filterDate(String filter) {
        RealmResults<Medication> medications = db.getMedicationByDate(filter);

        List<MedicationHistory> medicationHistory = initMedication(medications);
        if (adapter != null) {
            adapter = null;
            adapter = new HistoryTabAdapter(medicationHistory, getActivity(), 3);
            if (rvHistory != null) rvHistory.setAdapter(adapter);
        }

        initNoDataFilterDate(medicationHistory.isEmpty());

    }
}
