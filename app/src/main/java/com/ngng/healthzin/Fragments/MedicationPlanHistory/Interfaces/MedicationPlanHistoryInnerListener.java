package com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces;

/**
 * Created by nzisis on 12/02/17.
 */
public interface MedicationPlanHistoryInnerListener {
    void updateMedicationPlan();
}
