package com.ngng.healthzin.Fragments.Profile;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.makeramen.roundedimageview.RoundedImageView;
import com.ngng.healthzin.Activities.OverviewActivity;
import com.ngng.healthzin.Fragments.Menu.Interfaces.NavigationListener;
import com.ngng.healthzin.Fragments.Profile.BMI.BMIEditFragment;
import com.ngng.healthzin.Fragments.Profile.BMI.BMIPreviewFragment;
import com.ngng.healthzin.Fragments.Profile.Intefaces.BMIEditListener;
import com.ngng.healthzin.Fragments.Profile.Intefaces.PersonalDetailsEditListener;
import com.ngng.healthzin.Fragments.Profile.Intefaces.ProfileListener;
import com.ngng.healthzin.Fragments.Profile.PersonalDetails.PersonalDetailsEditFragment;
import com.ngng.healthzin.Fragments.Profile.PersonalDetails.PersonalDetailsPreviewFragment;
import com.ngng.healthzin.Model.User.UserInstance;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.ImageHandler;
import com.ngng.healthzin.Utils.SharedPreferenceManager;

import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * Created by nzisis on 17/01/17.
 */
public class ProfileFragment extends Fragment {

    private Toolbar toolbar;
    private RoundedImageView rivProfile;
    private TextView tvName, tvPersonalDetailsHeader, tvBMIHeader;
    private ImageSwitcher ivPersonalDetailsEdit, ivBMIEdit;

    private FragmentManager manager;
    private HashMap<String, Fragment> components;
    private String tag;
    private static final int SELECT_PICTURE = 100;
    private static final int MY_PERMISSIONS_REQUEST_READ_STORAGE = 101;


    private PersonalDetailsEditListener personalDetailsEditListener;
    private BMIEditListener bmiEditListener;
    private NavigationListener navigationListener;
    private ProfileListener profileListener;

    private boolean personalDetails, bmi;
    private SharedPreferences sharedPreferences;

    private ShowcaseView svProfile;
    private int showcase;

    private static final Field sChildFragmentManagerField;

    static {
        Field f = null;
        try {
            f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.e("EEEE", "Error getting mChildFragmentManager field", e);
        }
        sChildFragmentManagerField = f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.menu_drawer);

        ((OverviewActivity) getActivity()).setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigationListener.onNavigationClicked();
            }
        });

        toolbar.setTitle(getString(R.string.profile));

        rivProfile = (RoundedImageView) view.findViewById(R.id.rivProfile);

        tvName = (TextView) view.findViewById(R.id.tvName);
        tvPersonalDetailsHeader = (TextView) view.findViewById(R.id.tvPersonalDetailsHeader);
        tvBMIHeader = (TextView) view.findViewById(R.id.tvBMIHeader);

        ivPersonalDetailsEdit = (ImageSwitcher) view.findViewById(R.id.ivPersonalDetailsProcess);
        ivBMIEdit = (ImageSwitcher) view.findViewById(R.id.ivBMIProcess);


        return view;

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();

    }

    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvName.setTypeface(typefaceCR);
        tvBMIHeader.setTypeface(typefaceCR);
        tvPersonalDetailsHeader.setTypeface(typefaceCR);

        ivPersonalDetailsEdit.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView myView = new ImageView(getActivity().getApplicationContext());
                return myView;
            }
        });

        ivBMIEdit.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView myView = new ImageView(getActivity().getApplicationContext());
                return myView;
            }
        });


        Animation in = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_out_right);

        ivPersonalDetailsEdit.setInAnimation(in);
        ivPersonalDetailsEdit.setOutAnimation(out);
        ivBMIEdit.setInAnimation(in);
        ivBMIEdit.setOutAnimation(out);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String path = sharedPreferences.getString("profile_path", "");
        if (!path.equals("")) {
            Bitmap bitmap = ImageHandler.decodeSampledBitmapFromPath(path, 100, 100);
            if (bitmap != null)
                rivProfile.setImageBitmap(ImageHandler.rotatePicture(bitmap, path, getActivity()));
            else rivProfile.setImageResource(R.drawable.no_profile_icon);

        } else {
            rivProfile.setImageResource(R.drawable.no_profile_icon);
        }


        showcase = 0;

        if (!sharedPreferences.getBoolean("profileShowcase", false)) {

            ((OverviewActivity) getActivity()).setDisable_back_button(true);

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);


            Target viewTarget = new ViewTarget(R.id.rivProfile, getActivity());
            svProfile = new ShowcaseView.Builder(getActivity())
                    .withMaterialShowcase()
                    .blockAllTouches()
                    .setTarget(viewTarget)
                    .setContentTitle(getString(R.string.profile) + " (1/2)")
                    .setContentText(getString(R.string.instructions_profile_1))
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setOnClickListener(showCaseListener)
                    .build();

            svProfile.setButtonText(getString(R.string.instructions_got_it));
            svProfile.setButtonPosition(lps);
        }


        manager = getChildFragmentManager();
        components = new HashMap<>();


        tvName.setText(UserInstance.getInstance().getFirst_name() + " " + UserInstance.getInstance().getLast_name());


        tag = "PersonalDetailsPreviewFragment";
        PersonalDetailsPreviewFragment personalDetailsPreviewFragment = new PersonalDetailsPreviewFragment();
        components.put(tag, personalDetailsPreviewFragment);


        tag = "PersonalDetailsEditFragment";
        PersonalDetailsEditFragment personalDetailsEditFragment = new PersonalDetailsEditFragment();
        components.put(tag, personalDetailsEditFragment);


        tag = "BMIPreviewFragment";
        BMIPreviewFragment bmiPreviewFragment = new BMIPreviewFragment();
        components.put(tag, bmiPreviewFragment);

        tag = "BMIEditFragment";
        BMIEditFragment bmiEditFragment = new BMIEditFragment();
        components.put(tag, bmiEditFragment);

        personalDetailsEditListener = personalDetailsEditFragment;
        bmiEditListener = bmiEditFragment;

        //Check if the user has set his profile
        if (SharedPreferenceManager.isProfileSet(getActivity())) {

            Fragment fragment = components.get("PersonalDetailsPreviewFragment");
            manager.beginTransaction().replace(R.id.flPersonalDetails, fragment).commit();

            Fragment fragment1 = components.get("BMIPreviewFragment");
            manager.beginTransaction().replace(R.id.flBMI, fragment1).commit();

            personalDetails = false;
            bmi = false;

            ivBMIEdit.setImageResource(R.drawable.edit_icon);
            ivPersonalDetailsEdit.setImageResource(R.drawable.edit_icon);

        } else {
            Fragment fragment = components.get("PersonalDetailsEditFragment");
            manager.beginTransaction().replace(R.id.flPersonalDetails, fragment).commit();

            Fragment fragment1 = components.get("BMIEditFragment");
            manager.beginTransaction().replace(R.id.flBMI, fragment1).commit();

            personalDetails = true;
            bmi = true;

            ivBMIEdit.setImageResource(R.drawable.done_icon);
            ivPersonalDetailsEdit.setImageResource(R.drawable.done_icon);
        }


    }

    private void initListeners() {
        ivPersonalDetailsEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment firstFragment, secondFragment;

                if (personalDetails) {
                    personalDetailsEditListener.edit();

                    profileListener.updateMenu();

                    tvName.setText(UserInstance.getInstance().getFirst_name() + " " + UserInstance.getInstance().getLast_name());

                    firstFragment = components.get("PersonalDetailsEditFragment");
                    secondFragment = components.get("PersonalDetailsPreviewFragment");

                    ivPersonalDetailsEdit.setImageResource(R.drawable.edit_icon);

                } else {
                    firstFragment = components.get("PersonalDetailsPreviewFragment");
                    secondFragment = components.get("PersonalDetailsEditFragment");
                    ivPersonalDetailsEdit.setImageResource(R.drawable.done_icon);
                }

                FragmentTransaction fragmentTransaction = manager.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), fragmentTransaction, firstFragment, secondFragment, R.id.flPersonalDetails);
                fragmentTransactionExtended.addTransition(FragmentTransactionExtended.CUBE);
                fragmentTransactionExtended.commit();


                personalDetails = !personalDetails;
            }

        });

        ivBMIEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment firstFragment, secondFragment;
                if (bmi) {
                    bmiEditListener.edit();

                    firstFragment = components.get("BMIEditFragment");
                    secondFragment = components.get("BMIPreviewFragment");
                    ivBMIEdit.setImageResource(R.drawable.edit_icon);
                    if (!personalDetails)
                        ((PersonalDetailsPreviewFragment) components.get("PersonalDetailsPreviewFragment")).updateView();
                } else {

                    firstFragment = components.get("BMIPreviewFragment");
                    secondFragment = components.get("BMIEditFragment");
                    ivBMIEdit.setImageResource(R.drawable.done_icon);
                }

                FragmentTransaction fragmentTransaction = manager.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), fragmentTransaction, firstFragment, secondFragment, R.id.flBMI);
                fragmentTransactionExtended.addTransition(FragmentTransactionExtended.CUBE);
                fragmentTransactionExtended.commit();


                bmi = !bmi;
            }
        });


        rivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//User haven't granted permission
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED){


                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {

                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        builder.setMessage("Please grant permission to the app to access your storage in order to load a picture for your profile picture")
                                .setTitle("Permission");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                        MY_PERMISSIONS_REQUEST_READ_STORAGE);
                            }
                        });

                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST_READ_STORAGE);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }

                }else{
                    openImageChooser();
                }


            }
        });

    }


    public void setNavigationListener(NavigationListener navigationListener) {
        this.navigationListener = navigationListener;
    }

    public void setProfileListener(ProfileListener profileListener) {
        this.profileListener = profileListener;
    }

    public void openImageChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                // Get the url from data
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    // Get the path from the Uri
                    // String path = selectedImageUri.getPath();
                    String path = getRealPathFromURI_API19(getActivity(),selectedImageUri);
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    sharedPreferences.edit().putString("profile_path", path).apply();
                    Bitmap bitmap = ImageHandler.rotatePicture(ImageHandler.decodeSampledBitmapFromPath(path, 100, 100), path, getActivity());
                    if (bitmap != null) rivProfile.setImageBitmap(bitmap);
                    else rivProfile.setImageResource(R.drawable.no_profile_icon);

                    profileListener.updateMenu();
                }
            }
        }
    }

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }


    public String getRealPathFromURI_API19(Context context, Uri uri){
        String filePath = "";
        if (Build.VERSION.SDK_INT > 22){
            String wholeID = DocumentsContract.getDocumentId(uri);

            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];

            String[] column = { MediaStore.Images.Media.DATA };

            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{ id }, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }else{
            return getPathFromURI(uri);
        }


        return filePath;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e) {
                Log.e("EEEE", "Error setting mChildFragmentManager field", e);
            }
        }
    }

    private View.OnClickListener showCaseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (showcase) {
                case 0:
                    svProfile.setContentTitle("");
                    svProfile.setContentText("");
                    svProfile.setShowcase(new ViewTarget(ivPersonalDetailsEdit), true);
                    svProfile.setContentTitle(getString(R.string.profile) + " (2/2)");
                    svProfile.setContentText(getString(R.string.instructions_profile_2));
                    svProfile.setButtonText(getString(R.string.instructions_got_it));
                    break;
                case 1:
                    svProfile.hide();
                    ((OverviewActivity) getActivity()).setDisable_back_button(false);
                    sharedPreferences.edit().putBoolean("profileShowcase", true).apply();
                    break;
            }
            showcase++;
        }
    };

//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//            case MY_PERMISSIONS_REQUEST_READ_STORAGE: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay!
//                    openImageChooser();
//
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                }
//                return;
//            }
//
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
//    }
}
