package com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces;

/**
 * Created by nzisis on 09/02/17.
 */
public interface MedicationPlanHistoryUpdaterListener {
    void updateMedicationPlan(int position,boolean isChecked);
}
