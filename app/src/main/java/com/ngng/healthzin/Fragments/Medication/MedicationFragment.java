package com.ngng.healthzin.Fragments.Medication;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.makeramen.roundedimageview.RoundedImageView;
import com.ngng.healthzin.Activities.OverviewActivity;
import com.ngng.healthzin.Adapters.AutoCompleteTextview.AutoCompleteTextViewAdapter;
import com.ngng.healthzin.Fragments.Medication.Interfaces.MedicationDetailsListener;
import com.ngng.healthzin.Fragments.Medication.Interfaces.MedicationListener;
import com.ngng.healthzin.Fragments.Menu.Interfaces.NavigationListener;
import com.ngng.healthzin.Model.Realm.AutoCompleteMedication;
import com.ngng.healthzin.Model.Realm.Medication;
import com.ngng.healthzin.Model.Realm.MedicationPlanEvent;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.TimeManager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Stack;

import me.grantland.widget.AutofitTextView;

/**
 * Created by nzisis on 28/11/16.
 */
public class MedicationFragment extends Fragment {

    private Toolbar toolbar;
    private TextView tvMedicine, tvDose, tvReason, tvHeader;
    private AutofitTextView tvDate, tvTime;
    private AutoCompleteTextView actvMedicine;
    private EditText etDose, etReason;
    private RoundedImageView ivMedication;
    private ImageButton ibDate, ibTime;
    private Spinner spDose;
    private CardView cvMedication;

    private RelativeLayout rlMedicine, rlDose;


    private CalendarDatePickerDialogFragment cdate;
    private RadialTimePickerDialogFragment rtime;

    private String date, time, dose_meter;


    private FragmentManager manager;
    private HashMap<String, Fragment> components;
    private Stack<String> fragmentsStack;

    private String tag;
    private boolean show;

    private MedicationListener medicationListener;
    private NavigationListener navigationListener;


    private RealmDatabase db;

    private int flag;

    private AutoCompleteTextViewAdapter adapter;

    private View setupView, doneView;
    private ShowcaseView svMedication;
    private int showcase;
    private SharedPreferences sharedPreferences;

    private InputMethodManager inputMethodManagerMedicine, inputMethodManagerDose;

    private static final Field sChildFragmentManagerField;

    static {
        Field f = null;
        try {
            f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.e("EEEE", "Error getting mChildFragmentManager field", e);
        }
        sChildFragmentManagerField = f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_medication, container, false);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.menu_drawer);

        ((OverviewActivity) getActivity()).setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();
                navigationListener.onNavigationClicked();
            }
        });

        toolbar.setTitle(getString(R.string.medication));


        tvMedicine = (TextView) view.findViewById(R.id.tvMedicine);
        tvDose = (TextView) view.findViewById(R.id.tvDose);
        tvReason = (TextView) view.findViewById(R.id.tvReason);
        tvDate = (AutofitTextView) view.findViewById(R.id.tvDate);
        tvTime = (AutofitTextView) view.findViewById(R.id.tvTime);
        tvHeader = (TextView) view.findViewById(R.id.tvHeader);


        actvMedicine = (AutoCompleteTextView) view.findViewById(R.id.etMedicine);
        etDose = (EditText) view.findViewById(R.id.etDose);
        etReason = (EditText) view.findViewById(R.id.etReason);

        ivMedication = (RoundedImageView) view.findViewById(R.id.rivMedication);

        ibDate = (ImageButton) view.findViewById(R.id.ibDate);
        ibTime = (ImageButton) view.findViewById(R.id.ibTime);

        rlMedicine = (RelativeLayout) view.findViewById(R.id.rlMedicine);
        rlDose = (RelativeLayout) view.findViewById(R.id.rlDose);

        spDose = (Spinner) view.findViewById(R.id.spDose);

        actvMedicine.setVisibility(View.GONE);
        etDose.setVisibility(View.GONE);
        spDose.setVisibility(View.GONE);

        cvMedication = (CardView) view.findViewById(R.id.cvMedicine);

        setHasOptionsMenu(true);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();
    }


    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_medication, menu);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                MenuItem setup = menu.findItem(R.id.action_Setup);
                MenuItem done = menu.findItem(R.id.action_Done);

                setupView = getActivity().findViewById(setup.getItemId());
                doneView = getActivity().findViewById(done.getItemId());
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_Done:
                Log.e("Done", "Pressed");
                addMedication();
                break;

            case R.id.action_Setup:
                medicationListener.startMedicationPlan();
                break;


        }

        return false;
    }


    public void setMedicationListener(MedicationListener medicationListener) {
        this.medicationListener = medicationListener;
    }

    public void setNavigationListener(NavigationListener navigationListener) {
        this.navigationListener = navigationListener;
    }


    private void addMedication() {

        if (actvMedicine.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getString(R.string.enter_medicine), Toast.LENGTH_SHORT).show();
            return;
        }

        if (etDose.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getString(R.string.enter_dose), Toast.LENGTH_SHORT).show();
            return;
        }


        Medication medication = new Medication(actvMedicine.getText().toString(), date, Double.valueOf(etDose.getText().toString()), time, etReason.getText().toString(), dose_meter);
        medication.setId(db.getMedicationNextKey());

        AutoCompleteMedication autoCompleteMedication = new AutoCompleteMedication(actvMedicine.getText().toString().toUpperCase());

        db.insertMedication(medication);
        db.insertAutoCompleteMedication(autoCompleteMedication);
        db.getAllMedication();

        closeKeyboard();
        closeInnerFragment();

        medicationListener.addedMedication();

    }


    private void initListeners() {

        rlMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvMedicine.setVisibility(View.GONE);
                actvMedicine.setVisibility(View.VISIBLE);
                actvMedicine.requestFocus();
                inputMethodManagerMedicine.showSoftInput(actvMedicine, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        rlDose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvDose.setVisibility(View.GONE);
                etDose.setVisibility(View.VISIBLE);
                spDose.setVisibility(View.VISIBLE);
                etDose.requestFocus();
                inputMethodManagerDose.showSoftInput(etDose, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        ibDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicationListener.showDateDialog(cdate);
            }
        });


        ibTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicationListener.showTimeDialog(rtime);
            }
        });

        ivMedication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicationDetailsListener.showDetails();
            }
        });

        spDose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dose_meter = spDose.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void init() {
        Typeface typefaceCR = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Comfortaa-Regular.ttf");

        tvHeader.setTypeface(typefaceCR);
        tvMedicine.setTypeface(typefaceCR);
        tvDose.setTypeface(typefaceCR);
        tvReason.setTypeface(typefaceCR);
        tvDate.setTypeface(typefaceCR);
        tvTime.setTypeface(typefaceCR);
        actvMedicine.setTypeface(typefaceCR);
        etDose.setTypeface(typefaceCR);
        etReason.setTypeface(typefaceCR);

        inputMethodManagerMedicine = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManagerDose = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);


        adapter = new AutoCompleteTextViewAdapter(getActivity(), 3);
        actvMedicine.setAdapter(adapter);

        showcase = 0;

        if (!sharedPreferences.getBoolean("medicationShowcase", false)) {

            ((OverviewActivity) getActivity()).setDisable_back_button(true);

            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);


            Target viewTarget = new ViewTarget(R.id.rivMedication, getActivity());
            svMedication = new ShowcaseView.Builder(getActivity())
                    .withMaterialShowcase()
                    .blockAllTouches()
                    .setTarget(viewTarget)
                    .setContentTitle(getString(R.string.medication) + " (1/4)")
                    .setContentText(getString(R.string.instructions_medication_1))
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setOnClickListener(showCaseListener)
                    .build();

            svMedication.setButtonText(getString(R.string.instructions_got_it));
            svMedication.setButtonPosition(lps);
        }


        dose_meter = "ml";

        String categories[] = getResources().getStringArray(R.array.dose_meter);
        ArrayList<String> finalCategories = new ArrayList<>();
        for (int i = 0; i < categories.length; i++) {
            finalCategories.add(categories[i]);
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item, R.id.tvSpinnerCategories, finalCategories);
        spDose.setAdapter(arrayAdapter);


        db = new RealmDatabase(getActivity());

        manager = getChildFragmentManager();
        fragmentsStack = new Stack<>();
        components = new HashMap<>();

        show = false;

        tag = "MedicationDetailsFragment";
        MedicationDetailsFragment fragment = new MedicationDetailsFragment();
        components.put(tag, fragment);


        Calendar calendar = Calendar.getInstance();

        cdate = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(dateSetListener)
                .setDoneText("OK")
                .setCancelText(getString(R.string.cancel))
                .setPreselectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));


        rtime = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(timeSetListener)
                .setDoneText("OK")
                .setCancelText(getString(R.string.cancel))
                .setStartTime(calendar.getTime().getHours(), calendar.getTime().getMinutes());


        flag = getArguments().getInt("flag");

        if (flag != 0) {
            MedicationPlanEvent medicationPlanEvent = db.getMedicationPlanEventBasedOnID(flag);

            if (medicationPlanEvent != null) {
                tvMedicine.setVisibility(View.GONE);
                actvMedicine.setVisibility(View.VISIBLE);

                tvDose.setVisibility(View.GONE);
                etDose.setVisibility(View.VISIBLE);
                spDose.setVisibility(View.VISIBLE);

                etDose.setText(medicationPlanEvent.getDose() + "");
                actvMedicine.setText(medicationPlanEvent.getMedicine());

                int index = 0;
                for (int i = 0; i < spDose.getCount(); i++) {
                    if (spDose.getItemAtPosition(i).toString().equalsIgnoreCase(medicationPlanEvent.getDose_meter())) {
                        index = i;
                        break;
                    }
                }
                spDose.setSelection(index);
            }

        }

        time = TimeManager.getCurrentTime();
        date = TimeManager.getCurrentDate();

        tvDate.setText(getString(R.string.today));
        tvTime.setText(time);

    }


    private CalendarDatePickerDialogFragment.OnDateSetListener dateSetListener = new CalendarDatePickerDialogFragment.OnDateSetListener() {
        @Override
        public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
            String month, day;
            monthOfYear++;
            if (monthOfYear < 10)
                month = "0" + monthOfYear;
            else
                month = String.valueOf(monthOfYear);
            if (dayOfMonth < 10)
                day = "0" + dayOfMonth;
            else
                day = String.valueOf(dayOfMonth);
            date = year + "-" + month + "-" + day;


            if (TimeManager.isToday(date)) {
                tvDate.setText(getString(R.string.today));
            } else if (TimeManager.isYesterday(date)) {
                tvDate.setText(getString(R.string.yesterday));
            } else {
                tvDate.setText(date);
            }


        }
    };

    private RadialTimePickerDialogFragment.OnTimeSetListener timeSetListener = new RadialTimePickerDialogFragment.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
            String hours, mins;
            if (hourOfDay < 10)
                hours = "0" + hourOfDay;
            else
                hours = String.valueOf(hourOfDay);
            if (minute < 10)
                mins = "0" + minute;
            else
                mins = String.valueOf(minute);
            time = hours + ":" + mins;

            tvTime.setText(time);

        }
    };


    private MedicationDetailsListener medicationDetailsListener = new MedicationDetailsListener() {
        @Override
        public void showDetails() {
            if (!show) {
                fragmentsStack.push(tag);

                Fragment fragment = components.get(tag);
                FragmentTransaction fragmentTransaction = manager.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), fragmentTransaction, null, fragment, R.id.flMedication);
                fragmentTransactionExtended.addTransition(FragmentTransactionExtended.SLIDE_HORIZONTAL);
                fragmentTransactionExtended.commit();


                show = true;
            } else {
                Fragment fragment = components.get(tag);
                fragmentsStack.pop();
                manager.beginTransaction().remove(fragment).commit();

                show = false;

            }
        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        closeInnerFragment();
    }



    @Override
    public void onDetach() {
        super.onDetach();

        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e) {
                Log.e("EEEE", "Error setting mChildFragmentManager field", e);
            }
        }
    }

    private View.OnClickListener showCaseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (showcase) {
                case 0:
                    svMedication.setContentTitle("");
                    svMedication.setContentText("");
                    svMedication.setShowcase(new ViewTarget(cvMedication), true);
                    svMedication.setContentTitle(getString(R.string.medication) + " (2/4)");
                    svMedication.setContentText(getString(R.string.instructions_medication_2));
                    svMedication.setButtonText(getString(R.string.instructions_got_it));
                    break;
                case 1:
                    svMedication.setContentTitle("");
                    svMedication.setContentText("");
                    svMedication.setShowcase(new ViewTarget(setupView), true);
                    svMedication.setContentTitle(getString(R.string.medication) + " (3/4)");
                    svMedication.setContentText(getString(R.string.instructions_medication_3));
                    svMedication.setButtonText(getString(R.string.instructions_got_it));
                    break;
                case 2:
                    svMedication.setContentTitle("");
                    svMedication.setContentText("");
                    svMedication.setShowcase(new ViewTarget(doneView), true);
                    svMedication.setContentTitle(getString(R.string.medication) + " (4/4)");
                    svMedication.setContentText(getString(R.string.instructions_medication_4));
                    svMedication.setButtonText(getString(R.string.instructions_got_it));
                    break;
                case 3:
                    svMedication.hide();
                    ((OverviewActivity) getActivity()).setDisable_back_button(false);
                    sharedPreferences.edit().putBoolean("medicationShowcase", true).apply();
                    break;
            }
            showcase++;
        }
    };

    public void closeKeyboard(){
        if (inputMethodManagerMedicine.isActive())
            inputMethodManagerMedicine.hideSoftInputFromWindow(actvMedicine.getWindowToken(), 0);
        else if (inputMethodManagerDose.isActive())
            inputMethodManagerDose.hideSoftInputFromWindow(etDose.getWindowToken(), 0);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManagerDose.hideSoftInputFromWindow(etReason.getWindowToken(),0);
    }

    private void closeInnerFragment() {
        if (show) {
            Fragment fragment = components.get(tag);
            fragmentsStack.pop();
            manager.beginTransaction().remove(fragment).commit();

            show = false;
        }
    }

}
