package com.ngng.healthzin.Fragments.History.Interfaces;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;

/**
 * Created by nzisis on 22/12/16.
 */
public interface HistoryListener {
    void addNutrition();
    void addExercise();
    void addMedication();
    void showDateDialog(CalendarDatePickerDialogFragment fragment);
}
