package com.ngng.healthzin.Fragments.History;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ngng.healthzin.Activities.OverviewActivity;
import com.ngng.healthzin.Adapters.History.HistoryTabAdapter;
import com.ngng.healthzin.Fragments.History.Interfaces.HistoryListener;
import com.ngng.healthzin.Model.History.ExerciseGroupItem;
import com.ngng.healthzin.Model.History.ExerciseHistory;
import com.ngng.healthzin.Model.History.MedicationGroupItem;
import com.ngng.healthzin.Model.History.MedicationHistory;
import com.ngng.healthzin.Model.History.NutritionGroupItem;
import com.ngng.healthzin.Model.History.MonthAndYear;
import com.ngng.healthzin.Model.History.NutritionHistory;
import com.ngng.healthzin.Model.Realm.Exercise;
import com.ngng.healthzin.Model.Realm.Medication;
import com.ngng.healthzin.Model.Realm.Nutrition;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Utils.DividerItemDecoration;
import com.ngng.healthzin.Utils.TimeManager;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import io.realm.RealmResults;

/**
 * Created by nzisis on 19/12/16.
 */
public class HistoryTabFragment extends Fragment {
    private RecyclerView rvHistory;
    private LinearLayout llNoData;
    private TextView tvNoDataHeader, tvRecord;
    private FloatingActionButton fabAdd;
    private HashSet<MonthAndYear> monthAndYear;

    private RealmDatabase db;

    private int indicator;
   // private HistoryTabAdapter adapter;

    private HistoryListener historyListener;

    private AlertDialog dialog;

    public static HistoryTabFragment newInstance(int indicator) {

        HistoryTabFragment myFragment = new HistoryTabFragment();

        Bundle args = new Bundle();
        args.putInt("indicator", indicator);
        myFragment.setArguments(args);

        return myFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_tab, container, false);

        llNoData = (LinearLayout) view.findViewById(R.id.llNoData);
        tvNoDataHeader = (TextView) view.findViewById(R.id.tvNoDataHeader);
        tvRecord = (TextView) view.findViewById(R.id.tvRecord);
        fabAdd = (FloatingActionButton) view.findViewById(R.id.fabRecord);

        rvHistory = (RecyclerView) view.findViewById(R.id.rvHistory);
        rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvHistory.addItemDecoration(new DividerItemDecoration(getActivity().getResources().getDrawable(R.drawable.divider), false, false));

        //setHasOptionsMenu(true);

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        initListeners();
    }



    private void init() {
        monthAndYear = new HashSet<>();
        db = new RealmDatabase(getActivity());

        this.indicator = getArguments().getInt("indicator");
        switch (indicator) {
            case 1:
                initNutrition();
                break;
            case 2:
                initExercise();
                break;
            case 3:
                initMedication();
                break;

        }



    }

    private void initNoData(boolean flag) {
        if (!flag) {
            llNoData.setVisibility(View.GONE);
        } else {
            llNoData.setVisibility(View.VISIBLE);
            switch (indicator) {
                case 1:
                    tvNoDataHeader.setText("No nutrition is recorded");
                    tvRecord.setText("Add new Nutrition by pressing the plus button");
                    break;
                case 2:
                    tvNoDataHeader.setText("No exercise is recorded");
                    tvRecord.setText("Add new Exercixe by pressing the plus button");
                    break;
                case 3:
                    tvNoDataHeader.setText("No medication is recorded");
                    tvRecord.setText("Add new Medication by pressing the plus button");
                    break;
            }
        }
    }

    private void initListeners() {
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (indicator) {
                    case 1:
                        historyListener.addNutrition();
                        break;
                    case 2:
                        historyListener.addExercise();
                        break;
                    case 3:
                        historyListener.addMedication();
                        break;
                }
            }
        });

    }

    private void initNutrition() {

        List<NutritionHistory> nutritionHistory = new ArrayList<>();
        ArrayList<NutritionGroupItem> nutritionGroupItems = new ArrayList<>();


         RealmResults<Nutrition> allNutrition = db.getAllNutritionFromNewestToOldest();
         initNoData(allNutrition.isEmpty());

        for (int j = 0; j < allNutrition.size(); j++) {
            Nutrition child = allNutrition.get(j);
            String tokens[] = child.getDate().split("-");
            int year = Integer.parseInt(tokens[0]);
            String month = TimeManager.getMonthFromInt(Integer.parseInt(tokens[1]) - 1);
            MonthAndYear may = new MonthAndYear(month, year);

            if (!monthAndYear.contains(may)) {
                NutritionGroupItem group = new NutritionGroupItem(month, year);
                nutritionGroupItems.add(group);
                monthAndYear.add(may);
            }


            for (int i = 0; i < nutritionGroupItems.size(); i++) {
                NutritionGroupItem item = nutritionGroupItems.get(i);
                if (item.getMonth() == month && item.getYear() == year) {
                    nutritionGroupItems.get(i).addChild(child);
                    break;
                }

            }


        }


        for (int i = 0; i < nutritionGroupItems.size(); i++) {
            NutritionGroupItem item = nutritionGroupItems.get(i);
            item.setup();
            NutritionHistory nutritionHistoryGroup = new NutritionHistory(item.getMonth() + " " + item.getYear(), item.getDistict_childs());
            nutritionHistoryGroup.setInnerItems(item.getGroup_of_childs());
            nutritionHistory.add(nutritionHistoryGroup);

        }



       HistoryTabAdapter adapter = new HistoryTabAdapter(nutritionHistory, getActivity(),1);

        rvHistory.setAdapter(adapter);
    }


    private List<NutritionHistory> initNutrition(RealmResults<Nutrition> allNutrition, boolean flag) {

        List<NutritionHistory> nutritionHistory = new ArrayList<>();
        ArrayList<NutritionGroupItem> nutritionGroupItems = new ArrayList<>();


        // RealmResults<Nutrition> allNutrition = db.getAllNutritionFromNewestToOldest();
        if (flag) initNoData(allNutrition.isEmpty());

        for (int j = 0; j < allNutrition.size(); j++) {
            Nutrition child = allNutrition.get(j);
            String tokens[] = child.getDate().split("-");
            int year = Integer.parseInt(tokens[0]);
            String month = TimeManager.getMonthFromInt(Integer.parseInt(tokens[1]) - 1);
            MonthAndYear may = new MonthAndYear(month, year);

            if (!monthAndYear.contains(may)) {
                NutritionGroupItem group = new NutritionGroupItem(month, year);
                nutritionGroupItems.add(group);
                monthAndYear.add(may);
            }


            for (int i = 0; i < nutritionGroupItems.size(); i++) {
                NutritionGroupItem item = nutritionGroupItems.get(i);
                if (item.getMonth() == month && item.getYear() == year) {
                    nutritionGroupItems.get(i).addChild(child);
                    break;
                }

            }


        }


        for (int i = 0; i < nutritionGroupItems.size(); i++) {
            NutritionGroupItem item = nutritionGroupItems.get(i);
            item.setup();
            NutritionHistory nutritionHistoryGroup = new NutritionHistory(item.getMonth() + " " + item.getYear(), item.getDistict_childs());
            nutritionHistoryGroup.setInnerItems(item.getGroup_of_childs());
            nutritionHistory.add(nutritionHistoryGroup);

        }

        return nutritionHistory;

    }



    private void initExercise() {
        List<ExerciseHistory> exerciseHistory = new ArrayList<>();
        ArrayList<ExerciseGroupItem> exerciseGroupItems = new ArrayList<>();

         RealmResults<Exercise> allExercise = db.getAllExerciseFromNewestToOldest();
         initNoData(allExercise.isEmpty());

        for (int j = 0; j < allExercise.size(); j++) {
            Exercise child = allExercise.get(j);
            String tokens[] = child.getDate().split("-");
            int year = Integer.parseInt(tokens[0]);
            String month = TimeManager.getMonthFromInt(Integer.parseInt(tokens[1]) - 1);
            MonthAndYear may = new MonthAndYear(month, year);

            if (!monthAndYear.contains(may)) {
                ExerciseGroupItem group = new ExerciseGroupItem(month, year);
                exerciseGroupItems.add(group);
                monthAndYear.add(may);
            }


            for (int i = 0; i < exerciseGroupItems.size(); i++) {
                ExerciseGroupItem item = exerciseGroupItems.get(i);
                if (item.getMonth() == month && item.getYear() == year) {
                    exerciseGroupItems.get(i).addChild(child);
                    break;
                }

            }


        }


        for (int i = 0; i < exerciseGroupItems.size(); i++) {
            ExerciseGroupItem item = exerciseGroupItems.get(i);
            item.setup();
            ExerciseHistory exerciseHistoryGroup = new ExerciseHistory(item.getMonth() + " " + item.getYear(), item.getDistict_childs());
            exerciseHistoryGroup.setInnerItems(item.getGroup_of_childs());
            exerciseHistory.add(exerciseHistoryGroup);

        }

      HistoryTabAdapter  adapter = new HistoryTabAdapter(exerciseHistory, getActivity(), 2);

        rvHistory.setAdapter(adapter);


    }

    private List<ExerciseHistory> initExercise(RealmResults<Exercise> allExercise, boolean flag) {
        List<ExerciseHistory> exerciseHistory = new ArrayList<>();
        ArrayList<ExerciseGroupItem> exerciseGroupItems = new ArrayList<>();


        if (flag) initNoData(allExercise.isEmpty());

        for (int j = 0; j < allExercise.size(); j++) {
            Exercise child = allExercise.get(j);
            String tokens[] = child.getDate().split("-");
            int year = Integer.parseInt(tokens[0]);
            String month = TimeManager.getMonthFromInt(Integer.parseInt(tokens[1]) - 1);
            MonthAndYear may = new MonthAndYear(month, year);

            if (!monthAndYear.contains(may)) {
                ExerciseGroupItem group = new ExerciseGroupItem(month, year);
                exerciseGroupItems.add(group);
                monthAndYear.add(may);
            }


            for (int i = 0; i < exerciseGroupItems.size(); i++) {
                ExerciseGroupItem item = exerciseGroupItems.get(i);
                if (item.getMonth() == month && item.getYear() == year) {
                    exerciseGroupItems.get(i).addChild(child);
                    break;
                }

            }


        }


        for (int i = 0; i < exerciseGroupItems.size(); i++) {
            ExerciseGroupItem item = exerciseGroupItems.get(i);
            item.setup();
            ExerciseHistory exerciseHistoryGroup = new ExerciseHistory(item.getMonth() + " " + item.getYear(), item.getDistict_childs());
            exerciseHistoryGroup.setInnerItems(item.getGroup_of_childs());
            exerciseHistory.add(exerciseHistoryGroup);

        }


        return exerciseHistory;
    }

    private void initMedication() {
        List<MedicationHistory> medicationHistory = new ArrayList<>();
        ArrayList<MedicationGroupItem> medicationGroupItems = new ArrayList<>();

        RealmResults<Medication> allExercise = db.getAllMedicationFromNewestToOldest();
        initNoData(allExercise.isEmpty());

        for (int j = 0; j < allExercise.size(); j++) {
            Medication child = allExercise.get(j);
            String tokens[] = child.getDate().split("-");
            int year = Integer.parseInt(tokens[0]);
            String month = TimeManager.getMonthFromInt(Integer.parseInt(tokens[1]) - 1);
            MonthAndYear may = new MonthAndYear(month, year);

            if (!monthAndYear.contains(may)) {
                MedicationGroupItem group = new MedicationGroupItem(month, year);
                medicationGroupItems.add(group);
                monthAndYear.add(may);
            }


            for (int i = 0; i < medicationGroupItems.size(); i++) {
                MedicationGroupItem item = medicationGroupItems.get(i);
                if (item.getMonth() == month && item.getYear() == year) {
                    medicationGroupItems.get(i).addChild(child);
                    break;
                }

            }


        }


        for (int i = 0; i < medicationGroupItems.size(); i++) {
            MedicationGroupItem item = medicationGroupItems.get(i);
            item.setup();
            MedicationHistory medicationHistoryGroup = new MedicationHistory(item.getMonth() + " " + item.getYear(), item.getDistict_childs());
            medicationHistoryGroup.setInnerItems(item.getGroup_of_childs());
            medicationHistory.add(medicationHistoryGroup);

        }


      HistoryTabAdapter  adapter = new HistoryTabAdapter(medicationHistory, getActivity(), 3);

        rvHistory.setAdapter(adapter);
    }

    private List<MedicationHistory> initMedication(RealmResults<Medication> allExercise, boolean flag) {
        List<MedicationHistory> medicationHistory = new ArrayList<>();
        ArrayList<MedicationGroupItem> medicationGroupItems = new ArrayList<>();


        if (flag) initNoData(allExercise.isEmpty());

        for (int j = 0; j < allExercise.size(); j++) {
            Medication child = allExercise.get(j);
            String tokens[] = child.getDate().split("-");
            int year = Integer.parseInt(tokens[0]);
            String month = TimeManager.getMonthFromInt(Integer.parseInt(tokens[1]) - 1);
            MonthAndYear may = new MonthAndYear(month, year);

            if (!monthAndYear.contains(may)) {
                MedicationGroupItem group = new MedicationGroupItem(month, year);
                medicationGroupItems.add(group);
                monthAndYear.add(may);
            }


            for (int i = 0; i < medicationGroupItems.size(); i++) {
                MedicationGroupItem item = medicationGroupItems.get(i);
                if (item.getMonth() == month && item.getYear() == year) {
                    medicationGroupItems.get(i).addChild(child);
                    break;
                }

            }


        }


        for (int i = 0; i < medicationGroupItems.size(); i++) {
            MedicationGroupItem item = medicationGroupItems.get(i);
            item.setup();
            MedicationHistory medicationHistoryGroup = new MedicationHistory(item.getMonth() + " " + item.getYear(), item.getDistict_childs());
            medicationHistoryGroup.setInnerItems(item.getGroup_of_childs());
            medicationHistory.add(medicationHistoryGroup);

        }

        return medicationHistory;

    }

    public void setHistoryListener(HistoryListener historyListener) {
        this.historyListener = historyListener;
    }
//
//    public void filter(String filter, int indicator) {
//
//        switch (indicator) {
//            case 1:
//                RealmResults<Nutrition> nutritions = db.getNutritionByFood(filter);
//                List<NutritionHistory> nutritionHistory = initNutrition(nutritions, false);
//                if (adapter != null) adapter.notifyList(nutritionHistory);
//                else {
//                    adapter = new HistoryTabAdapter(nutritionHistory, getActivity(), 1);
//                    if (rvHistory != null) rvHistory.setAdapter(adapter);
//                }
//                break;
//            case 2:
//                RealmResults<Exercise> exercises = db.getExerciseByName(filter);
//                List<ExerciseHistory> exerciseHistory = initExercise(exercises, false);
//                adapter.notifyList(exerciseHistory);
//                if (adapter != null) adapter.notifyList(exerciseHistory);
//                else {
//                    adapter = new HistoryTabAdapter(exerciseHistory, getActivity(), 2);
//                    if (rvHistory != null) rvHistory.setAdapter(adapter);
//                }
//                break;
//            case 3:
//                RealmResults<Medication> medications = db.getMedicationByMedicine(filter);
//                List<MedicationHistory> medicationHistory = initMedication(medications, false);
//                adapter.notifyList(medicationHistory);
//                if (adapter != null) adapter.notifyList(medicationHistory);
//                else {
//                    adapter = new HistoryTabAdapter(medicationHistory, getActivity(), 3);
//                    if (rvHistory != null) rvHistory.setAdapter(adapter);
//                }
//                break;
//        }
//
//
//    }

}
