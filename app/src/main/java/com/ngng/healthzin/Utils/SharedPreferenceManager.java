package com.ngng.healthzin.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by nzisis on 18/01/17.
 */
public class SharedPreferenceManager {

    public static boolean isProfileSet(Context context){
        boolean flag = false;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

       if(!sharedPreferences.getString("first_name","").equals("")) flag = true;
       else if(!sharedPreferences.getString("last_name","").equals("")) flag = true;
       else if(sharedPreferences.getInt("age",-1) != -1) flag = true;
       else if(sharedPreferences.getFloat("weight", -1) != -1) flag = true;
       else if(sharedPreferences.getFloat("height",-1) != -1) flag = true;
       else if(!sharedPreferences.getString("sex","").equals("")) flag = true;

        return flag;
    }

}
