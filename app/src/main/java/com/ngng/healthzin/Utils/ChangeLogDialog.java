package com.ngng.healthzin.Utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;

import com.ngng.healthzin.R;

import it.gmariotti.changelibs.library.view.ChangeLogRecyclerView;

/**
 * Created by nzisis on 12/02/17.
 */
public class ChangeLogDialog extends DialogFragment {

    public ChangeLogDialog() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        ChangeLogRecyclerView chgList = (ChangeLogRecyclerView) layoutInflater.inflate(R.layout.change_log, null);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.version))
                .setView(chgList)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .create();

    }

}
