package com.ngng.healthzin.Utils;

import android.content.Context;

import com.ngng.healthzin.R;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by nzisis on 11/11/16.
 */
public class TimeManager {


    public static String getCurrentDate(){
        Calendar now = Calendar.getInstance();
        Date today = now.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = format.format(today);

        return formattedDate;
    }


    public static String getCurrentTime(){
        Calendar now = Calendar.getInstance();
        Date today = now.getTime();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        String formatedTime = format.format(today);

        return formatedTime;
    }

    public static boolean isToday(String date){
        Calendar today = Calendar.getInstance();

        Calendar item_date = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            item_date.setTime(format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return today.get(Calendar.YEAR) == item_date.get(Calendar.YEAR) &&
                today.get(Calendar.DAY_OF_YEAR) == item_date.get(Calendar.DAY_OF_YEAR);
    }

    public static boolean isYesterday(String date){
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);

        Calendar item_date = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            item_date.setTime(format.parse(date));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return yesterday.get(Calendar.YEAR) == item_date.get(Calendar.YEAR) &&
                yesterday.get(Calendar.DAY_OF_YEAR) == item_date.get(Calendar.DAY_OF_YEAR);
    }

    public static long daysDifference(String sdate1, String sdate2){

        long difference = 0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = format.parse(sdate1);
            Date date2 = format.parse(sdate2);
            difference = date2.getTime() - date1.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);

    }

    public static String getMonthFromInt(int num) {
        String month = "January";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }

    public static String getMonthAndDayFromDate(String sdate){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(sdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("MMM dd");

        return format.format(date);
    }

    public static String getTimeDifferenceFuture(String date,String time,Context context){

        String dateStop = date+" "+time;
        String dateStart = TimeManager.getCurrentDate() + " " +TimeManager.getCurrentTime();
        String timeDifference = "";

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            timeDifference = futureTimeDifferenceString(diff,context);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return timeDifference;

    }



    public static long getTimeDifferenceFutureInt(String date,String time){

        String dateStop = date+" "+time;
        String dateStart = TimeManager.getCurrentDate() + " " +TimeManager.getCurrentTime();


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Date d1 = null;
        Date d2 = null;

        long diff = -1;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
             diff = d2.getTime() - d1.getTime();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return diff;

    }


    public static String getTimeDifferencePast(String date,String time,Context context){

        String dateStart = date+" "+time;
        String dateStop = TimeManager.getCurrentDate() + " " +TimeManager.getCurrentTime();
        String timeDifference = "";

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();


           timeDifference = pastTimeDifferenceString(diff,context);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return timeDifference;

    }

    public static long getTimeDifferencePastInt(String date,String time){

        String dateStart = date+" "+time;
        String dateStop = TimeManager.getCurrentDate() + " " +TimeManager.getCurrentTime();
        long diff = -1;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
             diff = d2.getTime() - d1.getTime();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return diff;

    }

    public static String futureTimeDifferenceString(long diff,Context context){
        String timeDifference = "";

        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);


        if(diffDays!= 0){
            if(diffDays == 1) timeDifference = context.getString(R.string.tomorrow);
            else timeDifference = context.getString(R.string.in) +" " + diffDays + " "+context.getString(R.string.days);
        }else{
            if(diffHours!=0){
                timeDifference = context.getString(R.string.in) +" " + diffHours + " "+context.getString(R.string.hours);
            }else{
                timeDifference = context.getString(R.string.in) +" " + diffMinutes + " "+context.getString(R.string.mins);
            }
        }

        return timeDifference;
    }

    public static String pastTimeDifferenceString(long diff,Context context){
        String timeDifference = "";

        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);


        if(diffDays!= 0){
            if(diffDays == 1) timeDifference = context.getString(R.string.yesterday);
            else if(diffDays<=30) timeDifference =   diffDays + " " +context.getString(R.string.days_ago);
            else{
                long months = diffDays % 30;
                timeDifference = months + " "+context.getString(R.string.months_ago);
            }
        }else{
            if(diffHours!=0){
                timeDifference = diffHours + " "+context.getString(R.string.hours_ago);
            }else{
                if(diffMinutes == 0 ) timeDifference = context.getString(R.string.just_now);
                else timeDifference = diffMinutes + " "+context.getString(R.string.mins_ago);
            }
        }

        return timeDifference;
    }



}
