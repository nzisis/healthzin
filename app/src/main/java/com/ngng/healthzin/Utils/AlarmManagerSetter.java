package com.ngng.healthzin.Utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


import com.ngng.healthzin.Model.Realm.MedicationPlanEvent;
import com.ngng.healthzin.Services.BroadcastReceivers.StopAlarmReceiver;
import com.ngng.healthzin.Services.NotificationService;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by nzisis on 02/12/16.
 */
public class AlarmManagerSetter {

    public static void setAlarmManager(ArrayList<MedicationPlanEvent> events,Context context){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();

        for(int i=0; i<events.size(); i++){
            MedicationPlanEvent medicationPlan = events.get(i);
            int request_code = 100 * medicationPlan.getId();

            calendar.setTimeInMillis(System.currentTimeMillis());
            if(medicationPlan.getTime().compareTo(TimeManager.getCurrentTime())<=0) calendar.add(Calendar.DAY_OF_MONTH,1);

            String time_model[] = medicationPlan.getTime().split(":");
            calendar.set(Calendar.HOUR_OF_DAY,Integer.parseInt(time_model[0]));
            calendar.set(Calendar.MINUTE,Integer.parseInt(time_model[1]));


            int stop_request_code = request_code * 10;
            Intent startNotificationAlarm = new Intent(context, NotificationService.class);
            startNotificationAlarm.putExtra("Medicine", medicationPlan.getMedicine());
            startNotificationAlarm.putExtra("ID",medicationPlan.getId());
            startNotificationAlarm.putExtra("unique_id",request_code);
            PendingIntent pendingNotificationIntent = PendingIntent.getService(context, request_code, startNotificationAlarm, PendingIntent.FLAG_CANCEL_CURRENT);

            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, pendingNotificationIntent);

            Intent stopNotificationAlarm = new Intent(context, StopAlarmReceiver.class);
            Bundle bundle = new Bundle();
            bundle.putInt("Notification", request_code);
            bundle.putInt("MedicationPlanID",medicationPlan.getId());
            bundle.putInt("StopNotification", stop_request_code);
            stopNotificationAlarm.putExtras(bundle);
            PendingIntent pendingStopNotificationIntent = PendingIntent.getBroadcast(context,stop_request_code, stopNotificationAlarm, PendingIntent.FLAG_CANCEL_CURRENT);

            int interval = 1000 * 60 * 60 * 24 * medicationPlan.getTotal_days();
            calendar.setTimeInMillis(calendar.getTimeInMillis() + interval);
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingStopNotificationIntent);

        }

    }

    public static void stopAlarmManager(MedicationPlanEvent medicationPlan,Context context){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();

        int request_code = 100 * medicationPlan.getId();
        int stop_request_code = request_code * 10;

        Intent stopNotificationAlarm = new Intent(context, StopAlarmReceiver.class);
        Bundle bundle = new Bundle();
        bundle.putInt("Notification", request_code);
        bundle.putInt("MedicationPlanID",medicationPlan.getId());
        bundle.putInt("StopNotification", stop_request_code);
        stopNotificationAlarm.putExtras(bundle);
        PendingIntent pendingStopNotificationIntent = PendingIntent.getBroadcast(context,stop_request_code, stopNotificationAlarm, PendingIntent.FLAG_CANCEL_CURRENT);

        calendar.setTimeInMillis(calendar.getTimeInMillis());
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingStopNotificationIntent);
    }

}
