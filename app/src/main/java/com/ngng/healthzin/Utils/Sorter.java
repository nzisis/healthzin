package com.ngng.healthzin.Utils;

import com.ngng.healthzin.Model.Realm.MedicationPlanEvent;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Created by nzisis on 07/02/17.
 */
public class Sorter {

    public static LinkedHashMap<MedicationPlanEvent, Long> sortHashMapByValue(HashMap<MedicationPlanEvent, Long> unsortMap) {

        List<Map.Entry<MedicationPlanEvent, Long>> list = new LinkedList<Entry<MedicationPlanEvent, Long>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Entry<MedicationPlanEvent, Long>>() {
            @Override
            public int compare(Entry<MedicationPlanEvent, Long> o1,
                               Entry<MedicationPlanEvent, Long> o2) {

                return o1.getValue().compareTo(o2.getValue());
            }
        });

        // Maintaining insertion order with the help of LinkedList
        LinkedHashMap<MedicationPlanEvent, Long> sortedMap = new LinkedHashMap();
        for (Entry<MedicationPlanEvent, Long> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
}
