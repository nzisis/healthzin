package com.ngng.healthzin.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.IOException;

/**
 * Created by nzisis on 3/4/17.
 */
public class ImageHandler {

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public static Bitmap decodeSampledBitmapFromPath(String path,
                                                     int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }


    public static Bitmap rotatePicture(Bitmap bitmap, String path, Context context) {

        ExifInterface exifInterface = null;
        if (path != null) {
            try {
                exifInterface = new ExifInterface(path);
                int rotation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotationDegrees = -1;
                if (rotation == ExifInterface.ORIENTATION_ROTATE_90) {
                    rotationDegrees = 90;
                } else if (rotation == ExifInterface.ORIENTATION_ROTATE_180) {
                    rotationDegrees = 180;
                } else if (rotation == ExifInterface.ORIENTATION_ROTATE_270) {
                    rotationDegrees = 270;
                } else {
                    rotationDegrees = 0;
                }

                if (rotationDegrees != 0) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotationDegrees);
                    Bitmap rotatedImg = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    bitmap.recycle();
                    return rotatedImg;
                } else {
                    return bitmap;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        return null;
    }


    public static Bitmap getBitmapFromURI(Uri uri, Context context) {
        try {
            return MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Bitmap rotatePicture(Uri uri, Context context) {
        Bitmap bitmap = getBitmapFromURI(uri, context);

        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(uri.getPath());
            int rotation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int rotationDegrees = -1;
            if (rotation == ExifInterface.ORIENTATION_ROTATE_90) {
                rotationDegrees = 90;
            } else if (rotation == ExifInterface.ORIENTATION_ROTATE_180) {
                rotationDegrees = 180;
            } else if (rotation == ExifInterface.ORIENTATION_ROTATE_270) {
                rotationDegrees = 270;
            } else {
                rotationDegrees = 0;
            }

            if (rotationDegrees != 0) {
                Matrix matrix = new Matrix();
                matrix.postRotate(rotationDegrees);
                Bitmap rotatedImg = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bitmap.recycle();
                return rotatedImg;
            } else {
                return bitmap;
            }


        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

}
