package com.ngng.healthzin.Activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.View;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.ngng.healthzin.Fragments.Exercise.ExerciseFragment;
import com.ngng.healthzin.Fragments.Exercise.ExercisePlan.ExercisePlanFragment;
import com.ngng.healthzin.Fragments.Exercise.Interfaces.ExerciceListener;
import com.ngng.healthzin.Fragments.Exercise.Interfaces.ExercisePlanListener;
import com.ngng.healthzin.Fragments.History.HistoryFragment;
import com.ngng.healthzin.Fragments.History.Interfaces.HistoryListener;
import com.ngng.healthzin.Fragments.Medication.Interfaces.MedicationListener;
import com.ngng.healthzin.Fragments.Medication.Interfaces.MedicationPlanListener;
import com.ngng.healthzin.Fragments.Medication.MedicationFragment;
import com.ngng.healthzin.Fragments.Medication.MedicationPlan.MedicationPlanFragment;
import com.ngng.healthzin.Fragments.Menu.Interfaces.MenuCallback;
import com.ngng.healthzin.Fragments.Menu.Interfaces.NavigationListener;
import com.ngng.healthzin.Fragments.Menu.MenuFragment;
import com.ngng.healthzin.Fragments.Nutrition.Interfaces.NutritionListener;
import com.ngng.healthzin.Fragments.Nutrition.NutritionFragment;
import com.ngng.healthzin.Fragments.Overview.OverviewFragment;
import com.ngng.healthzin.Fragments.Profile.Intefaces.ProfileListener;
import com.ngng.healthzin.Fragments.Profile.ProfileFragment;
import com.ngng.healthzin.Model.User.UserInstance;
import com.ngng.healthzin.R;

import java.util.HashMap;
import java.util.Stack;


/**
 * Created by nzisis on 11/5/16.
 */
public class OverviewActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private FragmentManager manager;
    private Stack<String> fragmentsStack;
    private HashMap<String, Fragment> components;
    private String currentTag, previousTag;
    private int flag;
    private static final int MY_PERMISSIONS_REQUEST_READ_STORAGE = 101;

    private SharedPreferences sharedPreferences;

    private boolean disable_back_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);

        initUI();
        init();
    }


    private void initUI() {
        drawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.common_open_on_phone, R.string.common_open_on_phone) {
            @Override
            public void onDrawerOpened(View drawerView) {
                switch (currentTag) {
                    case "NutritionFragment":
                        ((NutritionFragment) components.get("NutritionFragment")).closeKeyboard();
                        break;
                    case "ExerciseFragment":
                        ((ExerciseFragment) components.get("ExerciseFragment")).closeKeyboard();
                        break;
                    case "MedicationFragment":
                        ((MedicationFragment) components.get("MedicationFragment")).closeKeyboard();
                        break;

                }
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    private void init() {


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(OverviewActivity.this);

        UserInstance.getInstance().setAge(sharedPreferences.getInt("age", -1));
        UserInstance.getInstance().setFirst_name(sharedPreferences.getString("first_name", ""));
        UserInstance.getInstance().setLast_name(sharedPreferences.getString("last_name", ""));
        UserInstance.getInstance().setSex(sharedPreferences.getString("sex", ""));
        UserInstance.getInstance().setHeight(sharedPreferences.getFloat("height", -1));
        UserInstance.getInstance().setWeight(sharedPreferences.getFloat("weight", -1));

        flag = 0;
        int position = 0;
        if (getIntent().getExtras() != null) {
            flag = getIntent().getExtras().getInt("flag");
            if (flag != 0) position = 2;
        }

        Bundle bundle = new Bundle();
        bundle.putInt("flag", flag);

        Bundle menu_bundle = new Bundle();
        menu_bundle.putInt("menu_position", position);


        manager = getFragmentManager();
        fragmentsStack = new Stack<>();
        components = new HashMap<>();
        previousTag = null;
        disable_back_button = false;

        String tag = "OverviewFragment";
        OverviewFragment overviewFragment = new OverviewFragment();
        overviewFragment.setNavigationListener(navigationListener);
        overviewFragment.setHistoryListener(historyListener);
        components.put(tag, overviewFragment);

        tag = "MedicationFragment";
        MedicationFragment medicationFragment = new MedicationFragment();
        medicationFragment.setMedicationListener(medicationListener);
        medicationFragment.setNavigationListener(navigationListener);
        medicationFragment.setArguments(bundle);
        components.put(tag, medicationFragment);


        tag = "MenuFragment";
        MenuFragment menuFragment = new MenuFragment();
        menuFragment.setMenuCallback(menuCallback);
        menuFragment.setArguments(menu_bundle);
        components.put(tag, menuFragment);

        presentMenu(tag);

        if (flag != 0) {
            presentFragment("MedicationFragment");
            notifyMenu(2);
        } else {
            presentFragment("OverviewFragment");
            notifyMenu(0);
        }


        tag = "NutritionFragment";
        NutritionFragment nutritionFragment = new NutritionFragment();
        nutritionFragment.setNutritionListener(nutritionListener);
        nutritionFragment.setNavigationListener(navigationListener);
        components.put(tag, nutritionFragment);

        tag = "ExerciseFragment";
        ExerciseFragment exerciseFragment = new ExerciseFragment();
        exerciseFragment.setExerciseListener(exerciceListener);
        exerciseFragment.setNavigationListener(navigationListener);
        components.put(tag, exerciseFragment);

        Bundle plan_bundle = new Bundle();
        plan_bundle.putBoolean("flow", true);

        tag = "ExercisePlanFragment";
        ExercisePlanFragment exercisePlanFragment = new ExercisePlanFragment();
        exercisePlanFragment.setExercisePlanListener(exercisePlanListener);
        exercisePlanFragment.setArguments(plan_bundle);
        components.put(tag, exercisePlanFragment);

        tag = "MedicationPlanFragment";
        MedicationPlanFragment medicationPlanFragment = new MedicationPlanFragment();
        medicationPlanFragment.setMedicationPlanListener(medicationPlanListener);
        medicationPlanFragment.setArguments(plan_bundle);
        components.put(tag, medicationPlanFragment);

        tag = "HistoryFragment";
        HistoryFragment historyFragment = new HistoryFragment();
        historyFragment.setHistoryListener(historyListener);
        historyFragment.setNavigationListener(navigationListener);
        components.put(tag, historyFragment);

        tag = "ProfileFragment";
        ProfileFragment profileFragment = new ProfileFragment();
        profileFragment.setNavigationListener(navigationListener);
        profileFragment.setProfileListener(profileListener);
        components.put(tag, profileFragment);


        Log.e("FLAG", flag + "");

    }


    public void presentFragment(String tag) {
        if (fragmentsStack.contains(tag)) {
            fragmentsStack.remove(tag);
        }

        fragmentsStack.push(tag);

        Fragment firstFragment, secondFragment;
        if (previousTag == null) firstFragment = null;
        else firstFragment = components.get(previousTag);

        secondFragment = components.get(tag);

        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(OverviewActivity.this, fragmentTransaction, firstFragment, secondFragment, R.id.flMainContent);
        fragmentTransactionExtended.addTransition(FragmentTransactionExtended.FADE);
        fragmentTransactionExtended.commit();

        previousTag = currentTag;
        currentTag = tag;

    }

    public void presentMenu(String tag) {
        Fragment fragment = components.get(tag);
        manager.beginTransaction().replace(R.id.flMenu, fragment).commit();
    }


    private MenuCallback menuCallback = new MenuCallback() {
        @Override
        public void onItemClicked(int position) {
            switch (position) {
                case 0:
                    presentFragment("OverviewFragment");
                    break;
                case 1:
                    presentFragment("NutritionFragment");
                    break;
                case 2:
                    presentFragment("MedicationFragment");
                    break;
                case 3:
                    presentFragment("ExerciseFragment");
                    break;
                case 4:
                    presentFragment("HistoryFragment");
                    break;


            }
            notifyMenu(position);
            openMenu();
        }

        @Override
        public void profileClicked() {
            presentFragment("ProfileFragment");
            notifyMenu(5);
            openMenu();
        }

        @Override
        public void settingClicked() {
            startActivity(new Intent(OverviewActivity.this, SettingsActivity.class));
        }
    };

    public void openMenu() {
        //Close the menu if it already opened otherwise open it
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    private void notifyMenu(int position) {
        MenuFragment menuFragment = ((MenuFragment) components.get("MenuFragment"));
        menuFragment.notify(position);
    }

    @Override
    public void onBackPressed() {

        String tag = "";

        if (!disable_back_button) {
            if (currentTag.equals("MedicationPlanFragment") || currentTag.equals("ExercisePlanFragment")) {
                if (!fragmentsStack.isEmpty() && fragmentsStack.size() > 1) {
                    fragmentsStack.pop();
                    //Then get the current first one without removing
                    tag = fragmentsStack.peek();
                    int position = -1;
                    switch (tag) {
                        case "ExerciseFragment":
                            position = 3;
                            break;
                        case "MedicationFragment":
                            position = 2;
                            break;
                    }

                    if (position != -1) {
                        notifyMenu(position);
                    }
                    presentFragment(tag);

                }
            } else if (currentTag.equals("OverviewFragment")) {
                finish();
            } else {
                tag = "OverviewFragment";
                notifyMenu(0);
                presentFragment(tag);
            }
        }


    }

    private NutritionListener nutritionListener = new NutritionListener() {
        @Override
        public void addedNutrition() {
            presentFragment("OverviewFragment");
            notifyMenu(0);
        }

        @Override
        public void showTimeDialog(RadialTimePickerDialogFragment fragment) {
            fragment.show(getSupportFragmentManager(), "TimeFragment");
        }

        @Override
        public void showDateDialog(CalendarDatePickerDialogFragment fragment) {
            fragment.show(getSupportFragmentManager(), "DateFragment");
        }
    };

    private ExerciceListener exerciceListener = new ExerciceListener() {
        @Override
        public void showWeeklyExercise() {
            presentFragment("ExercisePlanFragment");
        }

        @Override
        public void addedExercise() {
            presentFragment("OverviewFragment");
            notifyMenu(0);
        }

        @Override
        public void showTimeDialog(RadialTimePickerDialogFragment fragment) {
            fragment.show(getSupportFragmentManager(), "TimeFragment");
        }

        @Override
        public void showDateDialog(CalendarDatePickerDialogFragment fragment) {
            fragment.show(getSupportFragmentManager(), "DateFragment");
        }
    };

    private MedicationListener medicationListener = new MedicationListener() {
        @Override
        public void addedMedication() {
            presentFragment("OverviewFragment");
            notifyMenu(0);
        }

        @Override
        public void startMedicationPlan() {
            presentFragment("MedicationPlanFragment");
        }

        @Override
        public void showTimeDialog(RadialTimePickerDialogFragment fragment) {
            fragment.show(getSupportFragmentManager(), "TimeFragment");
        }

        @Override
        public void showDateDialog(CalendarDatePickerDialogFragment fragment) {
            fragment.show(getSupportFragmentManager(), "DateFragment");
        }
    };


    private MedicationPlanListener medicationPlanListener = new MedicationPlanListener() {
        @Override
        public void medicationPlanAdded() {
            presentFragment("OverviewFragment");
            notifyMenu(0);
        }

        @Override
        public void showTimeDialog(RadialTimePickerDialogFragment fragment) {
            fragment.show(getSupportFragmentManager(), "TimeFragment");
        }
    };


    private NavigationListener navigationListener = new NavigationListener() {
        @Override
        public void onNavigationClicked() {
            openMenu();
        }
    };

    private HistoryListener historyListener = new HistoryListener() {
        @Override
        public void addNutrition() {
            presentFragment("NutritionFragment");
            notifyMenu(1);
        }

        @Override
        public void addExercise() {
            presentFragment("ExerciseFragment");
            notifyMenu(3);
        }

        @Override
        public void addMedication() {
            presentFragment("MedicationFragment");
            notifyMenu(2);
        }

        @Override
        public void showDateDialog(CalendarDatePickerDialogFragment fragment) {
            fragment.show(getSupportFragmentManager(), "DateFragment");
        }
    };

    private ProfileListener profileListener = new ProfileListener() {
        @Override
        public void updateMenu() {
            MenuFragment menuFragment = ((MenuFragment) components.get("MenuFragment"));
            menuFragment.updateName();
        }
    };

    private ExercisePlanListener exercisePlanListener = new ExercisePlanListener() {
        @Override
        public void showTime(RadialTimePickerDialogFragment fragment) {
            fragment.show(getSupportFragmentManager(), "TimeFragment");
        }
    };


    public void setDisable_back_button(boolean disable_back_button) {
        this.disable_back_button = disable_back_button;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!

                    if(currentTag.equals("ProfileFragment")) ((ProfileFragment)components.get("ProfileFragment")).openImageChooser();


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
