package com.ngng.healthzin.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.ngng.healthzin.R;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by nzisis on 12/03/17.
 */
public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_TIME_OUT = 2000;
    private Handler mHandler;
    private Runnable myRunnable;
    private TextView tvHeader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        init();
    }

    private void init() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        Typeface typefaceCR = Typeface.createFromAsset(getAssets(), "fonts/Comfortaa-Regular.ttf");
        tvHeader.setTypeface(typefaceCR);

        mHandler = new Handler();
        myRunnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, OverviewActivity.class);
                startActivity(intent);
                finish();
            }
        };
    }


    @Override
    protected void onPause() {
// Remove callback on pause
        if (mHandler != null && myRunnable != null) {
            mHandler.removeCallbacks(myRunnable);
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
// Attach and start callback with delay on resume
        if (mHandler != null && myRunnable != null) {
            mHandler.postDelayed(myRunnable, SPLASH_TIME_OUT);
        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (mHandler != null && myRunnable != null) {
            mHandler.removeCallbacks(myRunnable);
        }
        super.onBackPressed();
    }
}
