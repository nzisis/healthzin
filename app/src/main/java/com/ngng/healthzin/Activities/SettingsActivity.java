package com.ngng.healthzin.Activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.PersistableBundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.ngng.healthzin.Fragments.Exercise.ExercisePlan.ExercisePlanFragment;
import com.ngng.healthzin.Fragments.Exercise.Interfaces.ExercisePlanListener;
import com.ngng.healthzin.Fragments.Medication.Interfaces.MedicationPlanListener;
import com.ngng.healthzin.Fragments.Medication.MedicationPlan.MedicationPlanFragment;
import com.ngng.healthzin.Fragments.MedicationPlanHistory.Interfaces.MedicationPlanHistoryListener;
import com.ngng.healthzin.Fragments.MedicationPlanHistory.MedicationPlanHistoryFragment;
import com.ngng.healthzin.Fragments.Settings.Interfaces.MedicationPlanSettingsListener;
import com.ngng.healthzin.Fragments.Settings.Interfaces.SettingsListener;
import com.ngng.healthzin.Fragments.Settings.SettingsFragment;
import com.ngng.healthzin.Model.Realm.MedicationPlanEvent;
import com.ngng.healthzin.R;

import java.util.HashMap;
import java.util.Stack;

/**
 * Created by nzisis on 30/01/17.
 */
public class SettingsActivity extends AppCompatActivity {
    private FragmentManager manager;
    private Stack<String> fragmentsStack;
    private HashMap<String, Fragment> components;
    private String currentTag;
    private Toolbar toolbar;
    private SettingsFragment settingsFragment;
    private boolean disable_back_button;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initUI();
        init();
    }

    private void initUI() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.navigation_back_arrow);
        toolbar.setTitle(getString(R.string.settings));
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void init() {
        manager = getFragmentManager();
        fragmentsStack = new Stack<>();
        components = new HashMap<>();
        disable_back_button = false;

        Bundle bundle = new Bundle();
        bundle.putBoolean("flow", false);

        String tag = "SettingsFragment";
        settingsFragment = new SettingsFragment();
        settingsFragment.setSettingsListener(settingsListener);
        components.put(tag, settingsFragment);

        presentFragment(tag);

        tag = "MedicationPlanFragment";
        MedicationPlanFragment medicationPlanFragment = new MedicationPlanFragment();
        medicationPlanFragment.setMedicationPlanListener(medicationPlanListener);
        medicationPlanFragment.setMedicationPlanSettingsListener(medicationPlanSettingsListener);
        medicationPlanFragment.setArguments(bundle);
        components.put(tag, medicationPlanFragment);

        tag = "ExercisePlanFragment";
        ExercisePlanFragment exercisePlanFragment = new ExercisePlanFragment();
        exercisePlanFragment.setExercisePlanListener(exercisePlanListener);
        exercisePlanFragment.setArguments(bundle);
        components.put(tag, exercisePlanFragment);

        tag = "MedicationPlanHistoryFragment";
        MedicationPlanHistoryFragment medicationPlanHistoryFragment = new MedicationPlanHistoryFragment();
        medicationPlanHistoryFragment.setMedicationPlanHistoryListener(medicationPlanHistoryListener);
        components.put(tag, medicationPlanHistoryFragment);

    }


    public void presentFragment(String tag) {
        if (fragmentsStack.contains(tag)) {
            fragmentsStack.remove(tag);
        }

        fragmentsStack.push(tag);

        currentTag = tag;
        Fragment fragment = components.get(tag);
        manager.beginTransaction().replace(R.id.content_frame, fragment).commit();

    }

    private MedicationPlanHistoryListener medicationPlanHistoryListener = new MedicationPlanHistoryListener() {

        @Override
        public void newMedicationPlan() {
            presentFragment("MedicationPlanFragment");
        }
    };

    private SettingsListener settingsListener = new SettingsListener() {
        @Override
        public void showMedicationPlanHistory() {
            presentFragment("MedicationPlanHistoryFragment");
        }

        @Override
        public void showExercisePlan() {
            presentFragment("ExercisePlanFragment");
        }
    };

    private MedicationPlanListener medicationPlanListener = new MedicationPlanListener() {
        @Override
        public void medicationPlanAdded() {
            onBackPressed();
        }

        @Override
        public void showTimeDialog(RadialTimePickerDialogFragment fragment) {
            fragment.show(getSupportFragmentManager(), "TimeFragment");
        }
    };

    private ExercisePlanListener exercisePlanListener = new ExercisePlanListener() {
        @Override
        public void showTime(RadialTimePickerDialogFragment fragment) {
            fragment.show(getSupportFragmentManager(), "TimeFragment");

        }
    };

    private MedicationPlanSettingsListener medicationPlanSettingsListener = new MedicationPlanSettingsListener() {
        @Override
        public void setup() {
         if(toolbar == null){
             toolbar = (Toolbar) findViewById(R.id.toolbar);

             toolbar.setNavigationIcon(R.drawable.navigation_back_arrow);
             toolbar.setTitle(getString(R.string.settings));
             toolbar.setTitleTextColor(getResources().getColor(R.color.white));


         }

            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            getSupportActionBar().hide();
        }
    };

    public void setDisable_back_button(boolean disable_back_button) {
        this.disable_back_button = disable_back_button;
    }


    @Override
    public void onBackPressed() {

        if(!disable_back_button){
            if (!fragmentsStack.isEmpty() && fragmentsStack.size() > 1) {
                //Remove the first element of the stack
                fragmentsStack.pop();
                //Then get the current first one without removing
                String tag = fragmentsStack.peek();
                if(tag.equals("SettingsFragment")) getSupportActionBar().show();

                presentFragment(tag);
            } else {
                if (currentTag.equals("SettingsFragment")) {
                    finish();
                    super.onBackPressed();
                } else {
                    presentFragment("SettingsFragment");
                }

            }
        }




    }


}
