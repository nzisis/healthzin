package com.ngng.healthzin.Services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ngng.healthzin.Activities.OverviewActivity;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.R;
import com.ngng.healthzin.Services.BroadcastReceivers.NotificationDoneReceiver;

/**
 * Created by nzisis on 02/12/16.
 */
public class NotificationService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public NotificationService(String name) {
        super(name);
    }

    public NotificationService() {
        super("NotificationWorkPlace");
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        String medicine = intent.getExtras().getString("Medicine");
        int id = intent.getExtras().getInt("ID");
        int unique_id = intent.getExtras().getInt("unique_id");
        //Every time the notification runs update the remaining days
        RealmDatabase db = new RealmDatabase(this);
        int days_left = db.updateMedicationPlanRemainingDays(id);


        String title=medicine;
        if(days_left!=-1){
            if(days_left == 0) title += " ("+getString(R.string.last_day)+")";
            else title += " (" +days_left+" "+getString(R.string.days_left)+")";
        }

        Log.d("Notification id ",unique_id+"");
        //on notification click  , launch recurrent transactions
        Intent contentIntent = new Intent(this, OverviewActivity.class);
        contentIntent.putExtra("flag",id);
        PendingIntent pIntent = PendingIntent.getActivity(this, unique_id, contentIntent, 0);


        //done action button
        Intent doneIntent = new Intent(this, NotificationDoneReceiver.class);
        doneIntent.putExtra("id", id);
        doneIntent.putExtra("notification_id",unique_id);
        PendingIntent donePendingIntent = PendingIntent.getBroadcast(this, (unique_id * 16), doneIntent, 0);

        //notification builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(R.drawable.logo_white);
        } else {
            builder.setSmallIcon(R.drawable.logo);
        }

        //create the notification
        Notification n = builder
                //set title
                .setContentTitle(title)

                .setPriority(NotificationCompat.PRIORITY_MAX)

                .setWhen(0)
                        //set content
                .setContentText(getString(R.string.time_for_medicine) + " " + medicine)
                        //set intent to launch on content click
                .setContentIntent(pIntent)
                        //cancel notification on click
                .setAutoCancel(true)
                        //add the actions
                .addAction(R.drawable.ic_done, getString(R.string.took_it), donePendingIntent)

                .build();

        //Sound and vibrate
         n.defaults |= Notification.DEFAULT_VIBRATE;
         n.defaults |= Notification.DEFAULT_SOUND;

        //notification manager
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        //create the notification
        notificationManager.notify(unique_id, n);


    }
}
