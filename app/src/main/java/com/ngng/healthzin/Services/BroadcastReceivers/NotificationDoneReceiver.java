package com.ngng.healthzin.Services.BroadcastReceivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ngng.healthzin.Activities.OverviewActivity;
import com.ngng.healthzin.Model.Realm.Medication;
import com.ngng.healthzin.Model.Realm.MedicationPlanEvent;
import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.Utils.TimeManager;

/**
 * Created by nzisis on 12/5/16.
 */
public class NotificationDoneReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int id = intent.getExtras().getInt("id");
        int notification_id = intent.getExtras().getInt("notification_id");

        RealmDatabase db = new RealmDatabase(context);
        MedicationPlanEvent medicationPlanEvent = db.getMedicationPlanEventBasedOnID(id);

        if(medicationPlanEvent!=null){
            Medication medication = new Medication(medicationPlanEvent.getMedicine(), TimeManager.getCurrentDate(),medicationPlanEvent.getDose(),TimeManager.getCurrentTime(),"",medicationPlanEvent.getDose_meter());
            medication.setId(db.getMedicationNextKey());

            db.insertMedication(medication);
        }


        //notification manager
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        notificationManager.cancel(notification_id);

        Intent overviewIntent = new Intent(context.getApplicationContext(), OverviewActivity.class);
        overviewIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(overviewIntent);


    }
}
