package com.ngng.healthzin.Services.BroadcastReceivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ngng.healthzin.Model.Realm.RealmDatabase;
import com.ngng.healthzin.Services.NotificationService;

/**
 * Created by nzisis on 02/12/16.
 */
public class StopAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        int notification = intent.getExtras().getInt("Notification");
        int stopNotification = intent.getExtras().getInt("StopNotification");
        int medication_plan_id = intent.getExtras().getInt("MedicationPlanID");

        Intent startNotificationAlarm = new Intent(context,NotificationService.class);
        PendingIntent pendingLocationIntent = PendingIntent.getService(context, notification, startNotificationAlarm, PendingIntent.FLAG_CANCEL_CURRENT);

        Intent stopNotificationAlarm = new Intent(context,StopAlarmReceiver.class);
        PendingIntent pendingStopLocationIntent = PendingIntent.getBroadcast(context,stopNotification,stopNotificationAlarm,PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager =(AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingLocationIntent);
        alarmManager.cancel(pendingStopLocationIntent);


        RealmDatabase db = new RealmDatabase(context);
        db.updateAMedicationPlanActive(false,medication_plan_id);

    }
}
