package com.ngng.healthzin.Model.Menu;

/**
 * Created by nzisis on 11/24/16.
 */
public class MenuItem {
    private String title;
    private int resource;
    private int color_resource;
    private boolean selected;

    public MenuItem(String title,int resource,int color_resource,boolean selected){
        this.title = title;
        this.resource = resource;
        this.color_resource = color_resource;
        this.selected = selected;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setResource(int resource){
        this.resource = resource;
    }

    public void setSelected(boolean selected){
        this.selected = selected;
    }

    public void setColor_resource(int color_resource){
        this.color_resource = color_resource;
    }

    public String getTitle(){
        return title;
    }

    public boolean getSelected(){
        return selected;
    }

    public int getResource(){
        return resource;
    }

    public int getColor_resource(){
        return color_resource;
    }
}
