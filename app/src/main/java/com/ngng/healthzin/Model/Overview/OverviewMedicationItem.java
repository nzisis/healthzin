package com.ngng.healthzin.Model.Overview;

import java.io.Serializable;

/**
 * Created by nzisis on 24/01/17.
 */
public class OverviewMedicationItem implements Serializable {

    private String header;
    private String time;
    private String time_ago;
    private String medication;
    private boolean next;

    public OverviewMedicationItem(String header,String time,String medication,boolean next){
        this.header = header;
        this.time = time;
        this.medication = medication;
        this.next = next;
    }

    public void setHeader(String header){
        this.header = header;
    }

    public void setTime(String time){
        this.time = time;
    }

    public void setTime_ago(String  time_ago){
        this.time_ago = time_ago;
    }


    public void setMedication(String medication){
        this.medication=  medication;
    }

    public void setNext(boolean next){
        this.next = next;
    }


    public String getHeader(){
        return header;
    }

    public String getTime(){
        return time;
    }

    public String getTime_ago(){
        return time_ago;
    }

    public String getMedication(){
        return medication;
    }

    public boolean getNext(){
        return next;
    }

}
