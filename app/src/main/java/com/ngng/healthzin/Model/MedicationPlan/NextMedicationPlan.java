package com.ngng.healthzin.Model.MedicationPlan;

/**
 * Created by nzisis on 22/01/17.
 */
public class NextMedicationPlan {
    private int position;
    private String value;

    public NextMedicationPlan(int position,String value){
        this.value = value;
        this.position = position;
    }
    public int getPosition(){
        return position;
    }

    public String getValue(){
        return value;
    }
}
