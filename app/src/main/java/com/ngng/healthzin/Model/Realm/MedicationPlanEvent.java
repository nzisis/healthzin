package com.ngng.healthzin.Model.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nzisis on 1/11/17.
 */
public class MedicationPlanEvent extends RealmObject {
    private String medicine;
    private String date;
    private double dose;
    private int total_days;
    private String time;
    private int days_left;
    private boolean active;
    private String dose_meter;


    @PrimaryKey
    private int id;

    public MedicationPlanEvent() {

    }

    public MedicationPlanEvent(String medicine, String startedDate, int total_days, String time, int days_left,double dose,String dose_meter) {
        this.medicine = medicine;
        this.date = startedDate;
        this.total_days = total_days;
        this.days_left = days_left;
        this.dose = dose;
        this.time = time;
        this.dose_meter = dose_meter;
        active = true;
    }

    public void setDose_meter(String dose_meter){
        this.dose_meter = dose_meter;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public void setDate(String startedDate) {
        this.date = startedDate;
    }

    public void setDose(double dose){
        this.dose = dose;
    }

    public void setTotal_days(int total_days) {
        this.total_days = total_days;
    }

    public void setTime(String time){
        this.time = time;
    }

    public void setDays_left(int days_left) {
        this.days_left = days_left;
    }

    public void setId(int id) {
        this.id = id;
    }


    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDose_meter(){
        return dose_meter;
    }

    public String getMedicine() {
        return medicine;
    }

    public String getDate() {
        return date;
    }

    public int getTotal_days() {
        return total_days;
    }

    public String getTime(){
        return time;
    }


    public int getDays_left() {
        return days_left;
    }


    public double getDose(){
        return dose;
    }

    public int getId() {
        return id;
    }


    public boolean getActive() {
        return active;
    }
    
}
