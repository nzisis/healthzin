package com.ngng.healthzin.Model.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nzisis on 1/3/17.
 */
public class ExercisePlanEvent extends RealmObject {
    private String exercise;
    private String date;
    private String time;

    @PrimaryKey
    private int id;

    public ExercisePlanEvent() {
        exercise = "";
        time = "";
    }

    public ExercisePlanEvent(String exercise, String date, String time) {
        this.exercise = exercise;
        this.date = date;
        this.time = time;
    }

    public void setId(int id){
        this.id = id;
    }


    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getExercise() {
        return exercise;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public int getId(){
        return id;
    }

}
