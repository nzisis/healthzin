package com.ngng.healthzin.Model.Realm;

import io.realm.RealmObject;

/**
 * Created by nzisis on 14/02/17.
 */
public class AutoCompleteExercise extends RealmObject {
    private String exercice;

    public AutoCompleteExercise(String exercice){
        this.exercice = exercice;
    }

    public AutoCompleteExercise(){

    }

    public void setExercice(String exercice){
        this.exercice = exercice;
    }

    public String getExercice(){
        return exercice;
    }
}
