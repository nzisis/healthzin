package com.ngng.healthzin.Model.Realm;

import io.realm.RealmObject;

/**
 * Created by nzisis on 14/02/17.
 */
public class AutoCompleteMedication extends RealmObject {
    private String medicine;

    public AutoCompleteMedication(String medicine){
        this.medicine = medicine;
    }

    public AutoCompleteMedication(){

    }

    public void setMedicine(String medicine){
        this.medicine = medicine;
    }

    public String getMedicine(){
        return medicine;
    }
}
