package com.ngng.healthzin.Model.Realm;

import io.realm.RealmObject;

/**
 * Created by nzisis on 14/02/17.
 */
public class AutoCompleteNutrition extends RealmObject {
    private String food;

    public AutoCompleteNutrition(String food){
        this.food = food;
    }

    public AutoCompleteNutrition(){

    }

    public void setFood(String food){
        this.food = food;
    }

    public String getFood(){
        return food;
    }
}
