package com.ngng.healthzin.Model.Realm;

import android.content.Context;
import android.util.Log;

import com.ngng.healthzin.Model.MedicationPlan.NextMedicationPlan;
import com.ngng.healthzin.Utils.Sorter;
import com.ngng.healthzin.Utils.TimeManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by nzisis on 19/12/16.
 */
public class RealmDatabase {

    private Realm realm;


    public RealmDatabase(Context context) {
        Realm.init(context);
        realm = Realm.getDefaultInstance();
    }


    public void insertAutoCompleteExercise(final AutoCompleteExercise exercise){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(exercise);
            }
        });
    }

    public void insertAutoCompleteNutrition(final AutoCompleteNutrition autoCompleteNutrition){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(autoCompleteNutrition);
            }
        });
    }

    public void insertAutoCompleteMedication(final AutoCompleteMedication autoCompleteMedication){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(autoCompleteMedication);
            }
        });
    }

    public void insertNutrition(final Nutrition nutrition) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(nutrition);
            }
        });

    }

    public void insertMedication(final Medication medication) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(medication);
            }
        });
    }

    public void insertExercise(final Exercise exercise) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(exercise);
            }
        });
    }


    public void insertOrUpdateMedicationPlan(final MedicationPlanEvent medicationPlanEvent) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(medicationPlanEvent);
            }
        });
    }

    public void insertOrUpdateExercisePlan(final ExercisePlanEvent exercisePlanEvent) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(exercisePlanEvent);
            }
        });
    }


    public void getAllNutritions() {

        RealmResults<Nutrition> items = realm.where(Nutrition.class).findAll();


        if (!items.isEmpty()) {
            for (int i = 0; i < items.size(); i++) {
                Nutrition nutrition = items.get(i);
                Log.e("MealTime", nutrition.getTime());
                Log.e("MealDate", nutrition.getDate());
                Log.e("Meal", nutrition.getFood());
                Log.e("MealQuantity", nutrition.getQuantity() + "");
                Log.e("MealCurrentMEal", nutrition.getCurrent_meal() + "");
                Log.e("MealID", nutrition.getNutritionID() + "");
            }
        }

    }

    public void getAllExercise() {

        RealmResults<Exercise> items = realm.where(Exercise.class).findAll();


        if (!items.isEmpty()) {
            for (int i = 0; i < items.size(); i++) {
                Exercise exercise = items.get(i);
                Log.e("ExerciseTime", exercise.getTime());
                Log.e("ExerciseDate", exercise.getDate());
                Log.e("Exercise", exercise.getExercise());
                Log.e("WorkTime", exercise.getWorkTime() + "");
                Log.e("Period of day", exercise.getPeriodOfDay());
                Log.e("ExerciseID", exercise.getId() + "");
            }
        }

    }

    public void getAllMedication() {

        RealmResults<Medication> items = realm.where(Medication.class).findAll();


        if (!items.isEmpty()) {
            for (int i = 0; i < items.size(); i++) {
                Medication medication = items.get(i);
                Log.e("Time", medication.getTime());
                Log.e("Date", medication.getDate());
                Log.e("Medication", medication.getMedicine());
                Log.e("Dose", medication.getDose() + "");
                Log.e("Reason", medication.getCause());
                Log.e("ID", medication.getId() + "");
            }
        }

    }

    public int getNutritionNextKey() {
        if (realm.where(Nutrition.class).findAll().isEmpty()) return 1;


        return realm.where(Nutrition.class).max("id").intValue() + 1;
    }

    public int getExerciseNextKey() {
        if (realm.where(Exercise.class).findAll().isEmpty()) return 1;


        return realm.where(Exercise.class).max("id").intValue() + 1;
    }

    public int getMedicationNextKey() {
        if (realm.where(Medication.class).findAll().isEmpty()) return 1;


        return realm.where(Medication.class).max("id").intValue() + 1;
    }

    public int getExercisePlanEventNextKey() {
        if (realm.where(ExercisePlanEvent.class).findAll().isEmpty()) return 1;


        return realm.where(ExercisePlanEvent.class).max("id").intValue() + 1;
    }

    public int getMedicationPlanEventNextKey() {
        if (realm.where(MedicationPlanEvent.class).findAll().isEmpty()) return 1;


        return realm.where(MedicationPlanEvent.class).max("id").intValue() + 1;
    }


    public Nutrition getTheMostRecentNutrition() {
        Nutrition nutrition = null;

        RealmResults<Nutrition> items = realm.where(Nutrition.class).findAll();
        if (!items.isEmpty()) {
            String[] fields = new String[3];
            Sort[] sorted = new Sort[3];
            fields[0] = "date";
            fields[1] = "time";
            fields[2] = "id";

            sorted[0] = Sort.DESCENDING;
            sorted[1] = Sort.DESCENDING;
            sorted[2] = Sort.DESCENDING;


            RealmResults<Nutrition> sortedItems = items.sort(fields, sorted);
            nutrition = sortedItems.get(0);

        }

        return nutrition;

    }

    public Exercise getTheMostRecentExercise() {
        Exercise exercise = null;

        RealmResults<Exercise> items = realm.where(Exercise.class).findAll();
        if (!items.isEmpty()) {
            String[] fields = new String[3];
            Sort[] sorted = new Sort[3];
            fields[0] = "date";
            fields[1] = "time";
            fields[2] = "id";


            sorted[0] = Sort.DESCENDING;
            sorted[1] = Sort.DESCENDING;
            sorted[2] = Sort.DESCENDING;


            RealmResults<Exercise> sortedItems = items.sort(fields, sorted);
            exercise = sortedItems.get(0);

        }

        return exercise;
    }

    public Medication getTheMostRecentMedication() {
        Medication medication = null;

        RealmResults<Medication> items = realm.where(Medication.class).findAll();
        if (!items.isEmpty()) {
            String[] fields = new String[3];
            Sort[] sorted = new Sort[3];
            fields[0] = "date";
            fields[1] = "time";
            fields[2] = "id";

            sorted[0] = Sort.DESCENDING;
            sorted[1] = Sort.DESCENDING;
            sorted[2] = Sort.DESCENDING;


            RealmResults<Medication> sortedItems = items.sort(fields, sorted);
            medication = sortedItems.get(0);

        }

        return medication;

    }

    public RealmResults<Nutrition> getAllNutritionFromNewestToOldest() {

        RealmResults<Nutrition> items = realm.where(Nutrition.class).findAll();
        String[] fields = new String[2];
        Sort[] sorted = new Sort[2];
        fields[0] = "date";
        fields[1] = "id";

        sorted[0] = Sort.DESCENDING;
        sorted[1] = Sort.ASCENDING;

        RealmResults<Nutrition> sortedItems = items.sort(fields, sorted);


        return sortedItems;
    }

    public RealmResults<Nutrition> getNutritionByFood(String food) {
        RealmResults<Nutrition> items = realm.where(Nutrition.class).equalTo("food", food).findAll();

        if (!items.isEmpty()) {
            String[] fields = new String[2];
            Sort[] sorted = new Sort[2];
            fields[0] = "date";
            fields[1] = "id";

            sorted[0] = Sort.DESCENDING;
            sorted[1] = Sort.ASCENDING;

            RealmResults<Nutrition> sortedItems = items.sort(fields, sorted);

            return sortedItems;
        }


        return items;
    }

    public RealmResults<Nutrition> getNutritionByDate(String date) {
        RealmResults<Nutrition> items = realm.where(Nutrition.class).equalTo("date", date).findAll();
        return items;
    }

    public RealmResults<Exercise> getAllExerciseFromNewestToOldest() {

        RealmResults<Exercise> items = realm.where(Exercise.class).findAll();
        String[] fields = new String[2];
        Sort[] sorted = new Sort[2];
        fields[0] = "date";
        fields[1] = "id";

        sorted[0] = Sort.DESCENDING;
        sorted[1] = Sort.ASCENDING;

        RealmResults<Exercise> sortedItems = items.sort(fields, sorted);


        return sortedItems;
    }

    public RealmResults<Exercise> getExerciseByName(String exercise) {
        RealmResults<Exercise> items = realm.where(Exercise.class).equalTo("exercise", exercise).findAll();
        String[] fields = new String[2];
        Sort[] sorted = new Sort[2];
        fields[0] = "date";
        fields[1] = "id";

        sorted[0] = Sort.DESCENDING;
        sorted[1] = Sort.ASCENDING;

        RealmResults<Exercise> sortedItems = items.sort(fields, sorted);


        return sortedItems;
    }

    public RealmResults<Exercise> getExerciseByDate(String date) {
        RealmResults<Exercise> items = realm.where(Exercise.class).equalTo("date", date).findAll();
        return items;
    }

    public RealmResults<Medication> getAllMedicationFromNewestToOldest() {

        RealmResults<Medication> items = realm.where(Medication.class).findAll();
        String[] fields = new String[2];
        Sort[] sorted = new Sort[2];
        fields[0] = "date";
        fields[1] = "id";

        sorted[0] = Sort.DESCENDING;
        sorted[1] = Sort.ASCENDING;

        RealmResults<Medication> sortedItems = items.sort(fields, sorted);


        return sortedItems;
    }

    public RealmResults<Medication> getMedicationByMedicine(String medicine) {
        RealmResults<Medication> items = realm.where(Medication.class).equalTo("medicine", medicine).findAll();
        String[] fields = new String[2];
        Sort[] sorted = new Sort[2];
        fields[0] = "date";
        fields[1] = "id";

        sorted[0] = Sort.DESCENDING;
        sorted[1] = Sort.ASCENDING;

        RealmResults<Medication> sortedItems = items.sort(fields, sorted);

        return sortedItems;
    }

    public RealmResults<Medication> getMedicationByDate(String date) {
        RealmResults<Medication> items = realm.where(Medication.class).equalTo("date", date).findAll();
        return items;
    }

    public int getCurrentMealNumber(String date) {
        int currentNumberOfMeal = 1;

        RealmResults<Nutrition> results = realm.where(Nutrition.class).equalTo("date", date).findAll();

        if (!results.isEmpty()) {
            currentNumberOfMeal = results.size() + 1;
        }

        return currentNumberOfMeal;
    }

    public RealmResults<ExercisePlanEvent> searchEvent(String date) {
        RealmResults<ExercisePlanEvent> results = realm.where(ExercisePlanEvent.class).equalTo("date", date).findAll();

        return results;
    }

    public ArrayList<String> getAllExercisePlanEvents() {
        RealmResults<ExercisePlanEvent> results = realm.where(ExercisePlanEvent.class).findAll();
        ArrayList<String> events = new ArrayList<>();

        if (!results.isEmpty()) {
            for (int i = 0; i < results.size(); i++) {
                events.add(results.get(i).getDate());
            }
        }

        return events;
    }


    public ExercisePlanEvent getNextExercisePlanEvent(String date, String time) {
        ExercisePlanEvent exercisePlanEvent = null;
        RealmResults<ExercisePlanEvent> results = realm.where(ExercisePlanEvent.class).findAll();

        if (!results.isEmpty()) {
            String[] fields = new String[2];
            Sort[] sorted = new Sort[2];
            fields[0] = "date";
            fields[1] = "time";

            sorted[0] = Sort.ASCENDING;
            sorted[1] = Sort.ASCENDING;


            RealmResults<ExercisePlanEvent> sortedItems = results.sort(fields, sorted);
            for (int i = 0; i < sortedItems.size(); i++) {
                ExercisePlanEvent item = sortedItems.get(i);
                if (item.getDate().compareTo(date) > 0) {
                    return item;
                } else if (item.getDate().compareTo(date) == 0 && item.getTime().compareTo(time) >= 0) {
                    return item;
                }
            }
        }

        return exercisePlanEvent;

    }


    public MedicationPlanEvent getMedicationPlanEventBasedOnID(int id) {
        MedicationPlanEvent medicationPlanEvent = null;

        RealmResults<MedicationPlanEvent> results = realm.where(MedicationPlanEvent.class).equalTo("id", id).findAll();
        if (!results.isEmpty()) {
            medicationPlanEvent = results.get(0);
        }

        return medicationPlanEvent;

    }


    public LinkedHashMap<MedicationPlanEvent, Long> getAllActiveMedicationPlanEventsOrdered() {
        LinkedHashMap<MedicationPlanEvent, Long> medicationPlanEvents = new LinkedHashMap<>();
        HashMap<MedicationPlanEvent, Long> unsortedMedicationPlanEvents = new HashMap<>();

        RealmResults<MedicationPlanEvent> results = realm.where(MedicationPlanEvent.class).equalTo("active", true).findAll();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        if (!results.isEmpty()) {

            for (int i = 0; i < results.size(); i++) {
                Calendar next_date = Calendar.getInstance();
                MedicationPlanEvent medicationPlanEvent = results.get(i);

                if (medicationPlanEvent.getTime().compareTo(TimeManager.getCurrentTime()) < 0) {
                    next_date.add(Calendar.DAY_OF_MONTH, 1);
                }

                String date = format.format(next_date.getTime());
                long diff = TimeManager.getTimeDifferenceFutureInt(date,medicationPlanEvent.getTime());

                unsortedMedicationPlanEvents.put(medicationPlanEvent,diff);
            }

            medicationPlanEvents = Sorter.sortHashMapByValue(unsortedMedicationPlanEvents);
        }

        return medicationPlanEvents;
    }


    public LinkedHashMap<MedicationPlanEvent, Long> getAllNoActiveMedicationPlanEventsOrdered() {
        LinkedHashMap<MedicationPlanEvent, Long> medicationPlanEvents = new LinkedHashMap<>();
        HashMap<MedicationPlanEvent, Long> unsortedMedicationPlanEvents = new HashMap<>();

        RealmResults<MedicationPlanEvent> results = realm.where(MedicationPlanEvent.class).equalTo("active", false).findAll();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        if (!results.isEmpty()) {

            for (int i = 0; i < results.size(); i++) {
                Calendar previous_date = Calendar.getInstance();
                MedicationPlanEvent medicationPlanEvent = results.get(i);


                String date_tokens[] = medicationPlanEvent.getDate().split("-");
                previous_date.set(Calendar.YEAR,Integer.parseInt(date_tokens[0]));
                previous_date.set(Calendar.MONTH,Integer.parseInt(date_tokens[1]));
                previous_date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date_tokens[2]));



                int days_passed = medicationPlanEvent.getTotal_days() - medicationPlanEvent.getDays_left();
                previous_date.add(Calendar.DAY_OF_MONTH,days_passed);

                String date = format.format(previous_date.getTime());

                long diff = TimeManager.getTimeDifferenceFutureInt(date,medicationPlanEvent.getTime());

                unsortedMedicationPlanEvents.put(medicationPlanEvent,diff);
            }

            medicationPlanEvents = Sorter.sortHashMapByValue(unsortedMedicationPlanEvents);
        }

        return medicationPlanEvents;
    }



    public Medication getNextMedicationPlanEvent() {
        Medication medication = null;
        RealmResults<MedicationPlanEvent> results = realm.where(MedicationPlanEvent.class).equalTo("active", true).findAll();

        if (!results.isEmpty()) {
            String[] fields = new String[2];
            Sort[] sorted = new Sort[2];
            fields[0] = "date";
            fields[1] = "time";

            sorted[0] = Sort.ASCENDING;
            sorted[1] = Sort.ASCENDING;


            RealmResults<MedicationPlanEvent> sortedItems = results.sort(fields, sorted);
            ArrayList<MedicationPlanEvent> possible_next_medication_plan_events = new ArrayList<>();

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Calendar next_date = Calendar.getInstance();

            boolean today = true;

            while (possible_next_medication_plan_events.isEmpty()) {
                //Find the next date
                for (int i = 0; i < sortedItems.size(); i++) {
                    MedicationPlanEvent possibleMedicationPlanEvent = sortedItems.get(i);
                    String medication_date_started = possibleMedicationPlanEvent.getDate();

                    Calendar item_date = Calendar.getInstance();

                    try {
                        item_date.setTime(format.parse(medication_date_started));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    //Find how many days passed from the beginning of the medication plan
                    long diff = next_date.getTime().getTime() - item_date.getTime().getTime();
                    long diffDays = diff / (24 * 60 * 60 * 1000);

                    //Check if the days passed are equal or smaller than the total days of the medication plan
                    if (diffDays <= possibleMedicationPlanEvent.getTotal_days()) {
                        if (today) {
                            //String[] different_times = possibleMedicationPlanEvent.getDifferent_times().split("-");
//                            for (int j = 0; j < different_times.length; j++) {
//                                if (different_times[j].compareTo(TimeManager.getCurrentTime()) >= 0) {
//                                    possible_next_medication_plan_events.add(possibleMedicationPlanEvent);
//                                    break;
//                                }
//                            }
                            if (possibleMedicationPlanEvent.getTime().compareTo(TimeManager.getCurrentTime()) > 0) {
                                possible_next_medication_plan_events.add(possibleMedicationPlanEvent);
                            }

                        } else {
                            possible_next_medication_plan_events.add(possibleMedicationPlanEvent);
                        }

                    }

                }

                if (possible_next_medication_plan_events.isEmpty()) {
                    next_date.add(Calendar.DAY_OF_MONTH, 1);
                    today = false;
                }


            }

            ArrayList<NextMedicationPlan> nextMedicationPlans = new ArrayList<>();

//            for (int i = 0; i < possible_next_medication_plan_events.size(); i++) {
//                MedicationPlanEvent possibleMedicationPlanEvent = possible_next_medication_plan_events.get(i);
//                String[] different_times = possibleMedicationPlanEvent.getDifferent_times().split("-");
//                for (int j = 0; j < different_times.length; j++) {
//                    if (today) {
//                        if (different_times[j].compareTo(TimeManager.getCurrentTime()) >= 0) {
//                            nextMedicationPlans.add(new NextMedicationPlan(i, different_times[j]));
//                        }
//                    } else {
//                        nextMedicationPlans.add(new NextMedicationPlan(i, different_times[j]));
//                    }
//
//                }
//            }


//            medicationPlanEvent = possible_next_medication_plan_events.get(nextMedicationPlans.get(0).getPosition());
//            String min_value = nextMedicationPlans.get(0).getValue();
//
//
//            for (int k = 1; k < nextMedicationPlans.size(); k++) {
//                NextMedicationPlan item = nextMedicationPlans.get(k);
//                if (item.getValue().compareTo(min_value) < 0) {
//                    min_value = item.getValue();
//                    medicationPlanEvent = possible_next_medication_plan_events.get(item.getPosition());
//                }
//            }

            MedicationPlanEvent nextMedicationPlanEvent = possible_next_medication_plan_events.get(0);

            medication = new Medication();
            medication.setDate(format.format(next_date.getTime()));
            medication.setTime(nextMedicationPlanEvent.getTime());
            medication.setMedicine(nextMedicationPlanEvent.getMedicine());
            medication.setDose(nextMedicationPlanEvent.getDose());

        }

        return medication;

    }


    public int updateMedicationPlanRemainingDays(int id) {
        RealmResults<MedicationPlanEvent> results = realm.where(MedicationPlanEvent.class).equalTo("id", id).findAll();
        int current_remaining_days = -1;

        if (!results.isEmpty()) {
            MedicationPlanEvent medicationPlanEvent = results.get(0);
            realm.beginTransaction();
            current_remaining_days = medicationPlanEvent.getTotal_days() - ((int) TimeManager.daysDifference(medicationPlanEvent.getDate(), TimeManager.getCurrentDate()));
            medicationPlanEvent.setDays_left(current_remaining_days);
            realm.commitTransaction();
        }

        return current_remaining_days;
    }


    public void updateAMedicationPlanActive(boolean active, int id) {
        RealmResults<MedicationPlanEvent> results = realm.where(MedicationPlanEvent.class).equalTo("id", id).findAll();

        if (!results.isEmpty()) {
            MedicationPlanEvent medicationPlanEvent = results.get(0);
            realm.beginTransaction();
            medicationPlanEvent.setActive(active);
            realm.commitTransaction();
        }

    }

    public void updateMedicationPlanDate(String date,int id){
        RealmResults<MedicationPlanEvent> results = realm.where(MedicationPlanEvent.class).equalTo("id", id).findAll();

        if (!results.isEmpty()) {
            MedicationPlanEvent medicationPlanEvent = results.get(0);
            realm.beginTransaction();
            medicationPlanEvent.setDate(date);
            medicationPlanEvent.setDays_left(medicationPlanEvent.getTotal_days());
            realm.commitTransaction();
        }
    }

    public ArrayList<String> getAutoCompleteNutritionBasedOnFirst2Characters(String startswith){
        ArrayList<String> foods = new ArrayList<>();

        RealmResults<AutoCompleteNutrition> results = realm.where(AutoCompleteNutrition.class).beginsWith("food", startswith).findAll();

        if(!results.isEmpty()){
            for(int i=0; i<results.size(); i++){
                foods.add(results.get(i).getFood());
            }
        }

        return foods;
    }

    public ArrayList<String> getAutoCompleteExerciseBasedOnFirst2Characters(String startswith){
        ArrayList<String> exercises = new ArrayList<>();

        RealmResults<AutoCompleteExercise> results = realm.where(AutoCompleteExercise.class).beginsWith("exercice",startswith).findAll();

        if(!results.isEmpty()){
            for(int i=0; i<results.size(); i++){
                exercises.add(results.get(i).getExercice());
            }
        }

        return exercises;
    }

    public ArrayList<String> getAutoCompleteMedicationBasedOnFirst2Characters(String startswith){
        ArrayList<String> medicines = new ArrayList<>();

        RealmResults<AutoCompleteMedication> results = realm.where(AutoCompleteMedication.class).beginsWith("medicine",startswith).findAll();

        if(!results.isEmpty()){
            for(int i=0; i<results.size(); i++){
                medicines.add(results.get(i).getMedicine());
            }
        }

        return medicines;
    }

}
