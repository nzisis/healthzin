package com.ngng.healthzin.Model.History;

//import com.ngng.healthzin.Model.Database.Nutrition;
import com.ngng.healthzin.Model.Realm.Nutrition;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nzisis on 12/12/16.
 */
public class NutritionHistory extends ExpandableGroup<Nutrition> {

    public ArrayList<ArrayList<Nutrition>> nutritionItems;

    public NutritionHistory(String title, List<Nutrition> items) {
        super(title, items);
    }


    public void setInnerItems( ArrayList<ArrayList<Nutrition>> nutritionItems){
        this.nutritionItems = nutritionItems;
    }

    public  ArrayList<ArrayList<Nutrition>> getInnerItems(){
        return nutritionItems;
    }

}
