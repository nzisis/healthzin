package com.ngng.healthzin.Model.History;

import com.ngng.healthzin.Model.Realm.Exercise;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nzisis on 20/12/16.
 */
public class ExerciseHistory extends ExpandableGroup<Exercise> {

    public ArrayList<ArrayList<Exercise>> nutritionItems;

    public ExerciseHistory(String title, List<Exercise> items) {
        super(title, items);
    }


    public void setInnerItems( ArrayList<ArrayList<Exercise>> nutritionItems){
        this.nutritionItems = nutritionItems;
    }

    public  ArrayList<ArrayList<Exercise>> getInnerItems(){
        return nutritionItems;
    }

}
