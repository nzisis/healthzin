package com.ngng.healthzin.Model.History;

import com.ngng.healthzin.Model.Realm.Medication;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nzisis on 22/12/16.
 */
public class MedicationHistory extends ExpandableGroup<Medication> {

    public ArrayList<ArrayList<Medication>> nutritionItems;

    public MedicationHistory(String title, List<Medication> items) {
        super(title, items);
    }


    public void setInnerItems( ArrayList<ArrayList<Medication>> nutritionItems){
        this.nutritionItems = nutritionItems;
    }

    public  ArrayList<ArrayList<Medication>> getInnerItems(){
        return nutritionItems;
    }

}
