package com.ngng.healthzin.Model.Realm;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nzisis on 19/12/16.
 */
public class Nutrition extends RealmObject implements Parcelable {
    private String food;
    private double quantity;
    private String time;
    private String date;
    private int current_meal;

    @PrimaryKey
    private int id;

    public Nutrition(){

    }

    protected Nutrition(Parcel in) {
        Bundle bundle = in.readBundle();
        food = bundle.getString("food");
        time = bundle.getString("time");
        date = bundle.getString("date");
        quantity = bundle.getDouble("quantity");
        current_meal = bundle.getInt("current");
        id = bundle.getInt("id");
    }

    public Nutrition(String food,double quantity, String time,String date, int current_meal){
        this.food = food;
        this.quantity = quantity;
        this.time = time;
        this.date = date;
        this.current_meal = current_meal;
    }

    public void setNutritionID(int id){
        this.id = id;
    }

    public void setFood(String food){
        this.food = food;
    }

    public void setQuantity(double quantity){
        this.quantity = quantity;
    }

    public void setTime(String time){
        this.time = time;
    }

    public void setDate(String date){
        this.date = date;
    }

    public void setCurrent_meal(int current_meal){
        this.current_meal = current_meal;
    }

    public String getFood(){
        return food;
    }

    public double getQuantity(){
        return quantity;
    }

    public String getTime(){
        return time;
    }

    public String getDate(){
        return date;
    }

    public int getCurrent_meal(){
        return current_meal;
    }

    public int getNutritionID(){
        return id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Bundle bundle = new Bundle();
        bundle.putString("food",food);
        bundle.putString("date",date);
        bundle.putString("time",time);
        bundle.putDouble("quantity", quantity);
        bundle.putInt("current", current_meal);
        bundle.putInt("id", id);

        dest.writeBundle(bundle);
    }


    public static final Parcelable.Creator<Nutrition> CREATOR = new Parcelable.Creator<Nutrition>() {
        @Override
        public Nutrition createFromParcel(Parcel in) {
            return new Nutrition(in);
        }

        @Override
        public Nutrition[] newArray(int size) {
            return new Nutrition[size];
        }
    };
}
