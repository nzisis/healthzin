package com.ngng.healthzin.Model.Realm;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nzisis on 20/12/16.
 */
public class Medication extends RealmObject implements Parcelable {
    private String medicine;
    private String date;
    private double dose;
    private String cause;
    private String time;
    private String dose_meter;

    @PrimaryKey
    private int id;

    public Medication() {

    }

    protected Medication(Parcel in) {
        Bundle bundle = in.readBundle();
        medicine = bundle.getString("medicine");
        time = bundle.getString("time");
        date = bundle.getString("date");
        dose = bundle.getDouble("dose");
        cause = bundle.getString("reason");
        dose_meter = bundle.getString("dose_meter");
        id = bundle.getInt("id");
    }

    public Medication(String medicine, String date, double dose, String time, String cause,String dose_meter) {

        this.medicine = medicine;
        this.date = date;
        this.dose = dose;
        this.time = time;
        this.cause = cause;
        this.dose_meter = dose_meter;

    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDose_meter(String dose_meter){
        this.dose_meter = dose_meter;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDose(double dose) {
        this.dose = dose;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDose_meter(){
        return dose_meter;
    }

    public String getMedicine() {
        return medicine;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public double getDose() {
        return dose;
    }

    public String getCause() {
        return cause;
    }

    public int getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Bundle bundle = new Bundle();
        bundle.putString("medicine", medicine);
        bundle.putString("date", date);
        bundle.putString("time", time);
        bundle.putDouble("dose", dose);
        bundle.putString("reason", cause);
        bundle.putInt("id", id);
        bundle.putString("dose_meter",dose_meter);

        dest.writeBundle(bundle);
    }


    public static final Parcelable.Creator<Medication> CREATOR = new Parcelable.Creator<Medication>() {
        @Override
        public Medication createFromParcel(Parcel in) {
            return new Medication(in);
        }

        @Override
        public Medication[] newArray(int size) {
            return new Medication[size];
        }
    };
}
