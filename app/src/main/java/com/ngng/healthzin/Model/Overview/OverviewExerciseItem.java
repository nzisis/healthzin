package com.ngng.healthzin.Model.Overview;

import java.io.Serializable;

/**
 * Created by nzisis on 21/01/17.
 */
public class OverviewExerciseItem implements Serializable {
    private String header;
    private String time;
    private String time_ago;
    private String exercise;
    private boolean next;

    public OverviewExerciseItem(String header,String time,String exercise,boolean next){
        this.header = header;
        this.time = time;
        this.exercise = exercise;
        this.next = next;
    }

    public void setHeader(String header){
        this.header = header;
    }

    public void setTime(String time){
        this.time = time;
    }

    public void setTime_ago(String  time_ago){
        this.time_ago = time_ago;
    }


    public void setExercise(String exercise){
        this.exercise=  exercise;
    }

    public void setNext(boolean next){
        this.next = next;
    }


    public String getHeader(){
        return header;
    }

    public String getTime(){
        return time;
    }

    public String getTime_ago(){
        return time_ago;
    }

    public String getExercise(){
        return exercise;
    }

    public boolean getNext(){
        return next;
    }


}
