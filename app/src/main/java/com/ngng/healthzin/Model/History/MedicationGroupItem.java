package com.ngng.healthzin.Model.History;

import com.ngng.healthzin.Model.Realm.Medication;

import java.util.ArrayList;

/**
 * Created by nzisis on 22/12/16.
 */
public class MedicationGroupItem {
    private String month;
    private int year;
    private ArrayList<Medication> childItems;
    private ArrayList<Medication> distict_childs;
    private ArrayList<ArrayList<Medication>> group_of_childs;

    public MedicationGroupItem(String month, int year) {
        this.month = month;
        this.year = year;
        childItems = new ArrayList<>();
    }

    public void addChild(Medication childItem) {
        childItems.add(childItem);
    }

    public String getMonth() {
        return month;
    }

    public ArrayList<Medication> getChildItems() {
        return childItems;
    }


    public int getYear() {
        return year;
    }

    public ArrayList<Medication> getDistict_childs() {
        return distict_childs;
    }

    public ArrayList<ArrayList<Medication>> getGroup_of_childs() {
        return group_of_childs;
    }

    public void setup() {

        distict_childs = new ArrayList<>();
        group_of_childs = new ArrayList<>();
        int counter = 0;

        for (int i = 0; i < childItems.size(); i++) {

            if (distict_childs.isEmpty()) {
                distict_childs.add(childItems.get(i));
                ArrayList<Medication> medicationItems = new ArrayList<>();
                medicationItems.add(childItems.get(i));
                group_of_childs.add(medicationItems);
                continue;
            }

            boolean flag = false;
            for (int j = 0; j < distict_childs.size(); j++) {

                if (distict_childs.get(j).getDate().equals(childItems.get(i).getDate())) {
                    group_of_childs.get(counter).add(childItems.get(i));
                    flag = true;
                    break;
                }
            }

            if (!flag) {
                counter++;
                distict_childs.add(childItems.get(i));
                ArrayList<Medication> medicationItems = new ArrayList<>();
                medicationItems.add(childItems.get(i));
                group_of_childs.add(medicationItems);
            }

        }
    }

}
