package com.ngng.healthzin.Model.User;

/**
 * Created by nzisis on 17/01/17.
 */
public class UserInstance {

    private String first_name;
    private String last_name;
    private String sex;
    private int age;
    private float weight;
    private float height;

    private static UserInstance instance;

    public static synchronized UserInstance getInstance(){
        if(instance == null){
            instance = new UserInstance();
        }

        return instance;
    }


    public void setFirst_name(String first_name){
        this.first_name = first_name;
    }

    public void setLast_name(String last_name){
        this.last_name = last_name;
    }
    
    public void setSex(String sex){this.sex = sex;}

    public void setAge(int age){
        this.age = age;
    }

    public void setWeight(float weight){
        this.weight = weight;
    }

    public void setHeight(float height){
        this.height = height;
    }

    public String getFirst_name(){
        return first_name;
    }

    public String getLast_name(){
        return last_name;
    }

    public String getSex(){
        return sex;
    }

    public int getAge(){
        return age;
    }

    public float getWeight(){
        return weight;
    }

    public float getHeight(){
        return height;
    }


}
