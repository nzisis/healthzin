package com.ngng.healthzin.Model.MedicationPlan;

/**
 * Created by nzisis on 29/11/16.
 */
public class MedicationPlanItem {
    private String header;
    private String time;

    public MedicationPlanItem(String header,String time){
        this.header = header;
        this.time = time;
    }

    public void setTime(String time){
        this.time = time;
    }

    public String getHeader(){
        return header;
    }

    public String getTime(){
        return time;
    }


}
